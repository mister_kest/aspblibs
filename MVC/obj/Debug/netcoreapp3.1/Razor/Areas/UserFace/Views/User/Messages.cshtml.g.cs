#pragma checksum "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "49d569289fc6a72df236ab9fe70fc508a780b971"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(ASpbLibs.User.Areas_UserFace_Views_User_Messages), @"mvc.1.0.view", @"/Areas/UserFace/Views/User/Messages.cshtml")]
namespace ASpbLibs.User
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\_ViewImports.cshtml"
using ASpbLibs;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"49d569289fc6a72df236ab9fe70fc508a780b971", @"/Areas/UserFace/Views/User/Messages.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0af805a46f4d7079167245b995af9e8baffb9016", @"/Areas/UserFace/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0af805a46f4d7079167245b995af9e8baffb9016", @"/Areas/UserFace/Views/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"23ac09be4bcfaa7f9829a01d1a134874eaae1f3b", @"/Areas/UserFace/Views/User/_ViewImports.cshtml")]
    public class Areas_UserFace_Views_User_Messages : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<ApplicationDb.Entities.Message>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
  
    ViewData["Title"] = "Личные сообщения";
    Layout = "_UserMessagesLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<div>\r\n");
#nullable restore
#line 10 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
     if (Model.Count() == 0)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <p>Нет сообщений</p>\r\n");
#nullable restore
#line 13 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
    }
    else
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
         foreach (ApplicationDb.Entities.Message message in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <p>");
#nullable restore
#line 18 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
          Write(message.Subject);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n");
#nullable restore
#line 19 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "A:\Wrk\ASpbLibs\MVC\Areas\UserFace\Views\User\Messages.cshtml"
         
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<ApplicationDb.Entities.Message>> Html { get; private set; }
    }
}
#pragma warning restore 1591
