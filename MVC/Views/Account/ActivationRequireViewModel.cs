﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationMVC.Views.Account
{
    
    public class ActivationRequireViewModel
    {
   
        [Display(Name = "Код активации")] 
        [Required(ErrorMessage = "Не указан код активации")]
        public string ActivationKey { get; set; }

        public string Email { get; set; }
    }
}
