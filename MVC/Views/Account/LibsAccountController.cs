﻿using ApplicationDb.Entities;
using ApplicationMVC.Views.Account;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.ControllersMVC
{
    public class LibsAccountController: Controller
    {
        private readonly APIAuthorization _authorization;
        private readonly ILogger<LibsAccountController> _logger;
       

        public LibsAccountController(
                ILogger<LibsAccountController> logger, 
                APIAuthorization authorization )
        {
            _authorization = authorization;
            _logger = logger;           
            _logger.LogInformation("Create");
        }


        /// <summary>
        /// Авторизация в качестве супервайзера
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsSuperviser()
        {
            _authorization.Signin("Superviser@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        }


        /// <summary>
        /// Авторизация в качестве комплектатора
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsComplectator()
        {
            _authorization.Signin("Complectator@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        }


        /// <summary>
        /// Авторизация в качестве ресепшен
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsReception()
        {
            _authorization.Signin("Reception@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        }


        /// <summary>
        /// Авторизация в качестве куратора
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsCurator()
        {
            _authorization.Signin("Curator@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        }


        /// <summary>
        /// Авторизация в качестве читателя
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsReader()
        {
            _authorization.Signin("Reader@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        } 
    }
}
