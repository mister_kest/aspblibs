﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationMVC.Views.Account
{
    public class RestoreModel
    {
        [BindProperty]
        public string ErrorMessage { get; set; }

        [BindProperty]
        [Display(Name = "Электронный адрес")]
        [DataType(
            DataType.EmailAddress,
            ErrorMessage = "Электронный адрес задан некорректно"
        )]
        [Required(ErrorMessage = "Не указан электронный адрес")]
        public string Email { get; set; }

        [BindProperty]
        public string EmailValidationState { get; set; }
 
    }
}
