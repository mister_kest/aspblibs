﻿using System;
using System.Linq;
using ApplicationCommon.CommonServices;
using ASpbLibs.Core.Services;
using ApplicationDb;
using ApplicationDb.Entities;
using ApplicationMVC.Views.Account;
using CoreApp.AppActive;
using CoreApp.AppAPI;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Collections.Generic;


namespace ASpbLibs.Application.ControllersMVC
{
    public class AccountController : Controller
    {
        private readonly PageLayoutService _layout;
   
        private readonly APIAuthorization _authorization;
        private readonly NotificationsService _notifications;
        private readonly ApplicationDbContext _db;
        private readonly EmailService _email;
        private readonly ILogger<AccountController> _logger;
        private readonly AuthorizationOptions _options;

        public AccountController(
            PageLayoutService layout,
            EmailService email,
            ApplicationDbContext db,
            AuthorizationOptions options,
            ILogger<AccountController> logger,
            APIAuthorization authorization,
       
            NotificationsService user )
        {
            _layout = layout;
      
            _authorization = authorization;
            _notifications = user;
            _db = db;
            _email = email;
            _options = options;
            _logger = logger;

            _logger.LogInformation("Create");
        }

        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyEmail(string email)
        {
            if (!_authorization.HasUserWithEmail(email))
            {
                return Json($"Электронный адрес {email} уже зарегистрирован");
            }

            return Json(true);
        }


        /// <summary>
        /// Авторизация в качестве администратора системы
        /// </summary>
        /// <returns></returns>
        public IActionResult LoginAsAdmin()
        {
            _authorization.Signin("Admin@eckumoc.com", "eckumoc@gmail.com");
            
            return Redirect("/Home/News");
           
        }

        public IActionResult LoginAsUser()
        {
            _authorization.Signin("User@eckumoc.com", "eckumoc@gmail.com");
            return Redirect("/Home/News");
        }


      

        /// <summary>
        /// Переход на страницу активации учетной записи
        /// </summary>
        /// <returns></returns>
        public IActionResult ActivationRequire()
        {
            ApplicationDb.Entities.User user = _authorization.Verify();
            if(user != null)
            {
                if(_authorization.IsActivated() == false)
                {
                    return View(new ActivationRequireViewModel()
                    {
                        Email = user.Account.Email
                    });
                }
                else
                {
                    return RedirectToAction("News", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public IActionResult ActivationRequire([Bind("ActivationKey,Email")]ActivationRequireViewModel model)
        {
            ApplicationDb.Entities.User user = _authorization.Verify();
            if (user != null)
            {
                if (_db.Accounts.Find(user.Account.ID).ActivationKey == model.ActivationKey)
                {
                    _db.Accounts.Find(user.Account.ID).Activated = DateTime.Now;
                    _db.SaveChanges();
                }
                return View("ActivationComplete");
            }
            else
            {
                return View(model);
            }
            
        }


        /// <summary>
        /// Отправка сообщения с кодом активации на электронную почту
        /// </summary>
        /// <returns></returns>
        public IActionResult SendActivationKey()
        {            
            ApplicationDb.Entities.User user = _authorization.Verify();
            if( user != null)
            {
                string baseUrl = GetBaseUrl();
                string activationKey = _authorization.GenerateActivationKey(_options.KeyLength);
                _db.Accounts.Find(user.AccountID).ActivationKey = activationKey;
                _db.SaveChanges();
                _email.SendEmail(user.Account.Email,
                    "Активация учетной записи",
                    $"<p>Ваш код активации: <b>{activationKey}</b></p>"+
                    $"Для активации учетной записи перейдите по ссылки: <a href=\"{baseUrl}/Account/Activate/{activationKey}\">{baseUrl}/Account/Activate/{activationKey}</a>"
                );
                _notifications.AddNotification(new NotificationMessage() {
                    Title = $"Код активации отправлен на {user.Account.Email}"
                });
            }
            else
            {
                _notifications.AddNotification(new NotificationMessage()
                {
                    Type = "Error",
                    Title = "Не удалось отправить код активации"
                });
            }
            return RedirectToAction("ActivationRequire");
        }

 
         

        /// <summary>
        /// Выход пользователя из системы
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {            
            _authorization.Signout();
            return RedirectToAction("HasReaderCard","Wizzard");
        }


        /// <summary>
        /// Переход на страницу восстановления доступа к учетной записи
        /// </summary>
        /// <returns></returns>
        public IActionResult Restore()
        {
            return View(new RestoreModel());
        }


        /// <summary>
        /// Запрос на восстановления доступа к учетной записи
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Restore([Bind("Email")] RestoreModel model)
        {
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    switch (state.Key)
                    {
                        case "Email": model.EmailValidationState = GetValidationState(state.Key); break;                      
                    }
                }
                return View(model);
            }
            try
            {
                try
                {
                    _authorization.RestorePasswordByEmail(model.Email);
                    return View("RestoreSuccess", model);
                }
                catch(Exception ex)
                {
                    model.ErrorMessage = ex.Message;
                    return View("Restore", model);
                }
                

                
            }
            catch (Exception ex)
            {
                model.ErrorMessage = "Авторизация не выполнена. " + ex.Message;
                return View(model);
            }
        }


        /// <summary>
        /// Переход на страницу регистрации пользователя
        /// </summary>
        /// <returns></returns>
        public IActionResult Registration()
        {
            return View(new RegistrationModel()
            {
                SurName = "Батов",
                FirstName = "Константин",
                LastName = "Александрович",
                Birthday = DateTime.Parse("26.08.1989"),
                Tel = "7-904-334-1124"
            });
        }

        
        /// <summary>
        /// Активация учетной записи
        /// </summary>
        /// <param name="id">ключ активации</param>
        /// <returns></returns>
        public IActionResult Activate([FromRoute] string id )
        {
            string activationKey = id;

            ApplicationDb.Entities.User user = _authorization.Verify();
            if(user != null)
            {
                if(_db.Accounts.Find(user.Account.ID).ActivationKey == id)
                {
                    _db.Accounts.Find(user.Account.ID).Activated = DateTime.Now;
                    _db.SaveChanges();
                }
                return View("ActivationComplete");
            }
            else
            {
                return View();
            }
        }


        /// <summary>
        /// Запрос регитсрации учетной записи пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Registration([Bind("Email,Password,Confirmation,FirstName,LastName,SurName,Birthday,Tel")]RegistrationModel model)
        {
            _logger.LogInformation("OnPostAsync");

            if (model.Birthday.Year < 1920)
            {
                ModelState.AddModelError("Birthday", "Дата рождения указана неверно");
                return View(model);
            }

            if (model.Confirmation != model.Password)
            {
                ModelState.AddModelError("Confirmation", "Подтверждение пароля отчается от самого пароля");
                return View(model);
            }

            if (_authorization.HasUserWithEmail(model.Email) == true)
            {
                ModelState.AddModelError("Email", "Данный электронный адрес уже зарегистрирован");
                return View(model);
            }

            if (_authorization.HasUserWithTel(model.Tel) == true)
            {
                ModelState.AddModelError("Tel", "Данный номер телефона уже зарегистрирован");
                return View(model);
            }

            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    switch (state.Key)
                    {
                        case "SurName": model.SurNameValidationState = GetValidationState(state.Key); break;
                        case "Last": model.LastNameValidationState = GetValidationState(state.Key); break;
                        case "First": model.FirstNameValidationState = GetValidationState(state.Key); break;
                        case "Birthday": model.BirthdayValidationState = GetValidationState(state.Key); break;
                        case "Email": model.EmailValidationState = GetValidationState(state.Key); break;
                        case "Password": model.PasswordValidationState = GetValidationState(state.Key); break;
                        case "Confirmation": model.ConfirmationValidationState = GetValidationState(state.Key); break;
                        case "Tel": model.TelValidationState = GetValidationState(state.Key); break;

                    }
                }
                return View(model);
            }

            try
            {


                string EncodedResponse = Request.Form["g-Recaptcha-Response"];
                bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "true") ? true : false;
                if (IsCaptchaValid)
                {
                    _authorization.Signup(model.Email, model.Password, model.Confirmation, model.FirstName, model.LastName, model.SurName, model.Birthday, model.Tel);

                    string baseUrl = GetBaseUrl();
                    string activationKey = _authorization.GenerateActivationKey(_options.KeyLength);
                    _authorization.GetUserByEmail(model.Email).Account.ActivationKey = activationKey;
                    _db.SaveChanges();
                    _email.SendEmail(model.Email, 
                        "Активация учетной записи", 
                        $"Для активации учетной записи перейдите по ссылки: <a href=\"{baseUrl}/Account/Activate/{activationKey}\">{baseUrl}/Account/Activate/{activationKey}</a>"
                    );
                    return View($"RegistrationComplete", model);
                }
                else
                {
                    TempData["recaptcha"] = "Подтвердите что Вы не робот";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                
                model.ErrorMessage = ex.Message;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    model.ErrorMessage += " " + ex.Message;
                }
                
                return View(model);
            }
        }


        /// <summary>
        /// Получение URL-адреса приложения
        /// </summary>
        /// <returns></returns>
        private string GetBaseUrl()
        {
            string url = _options.ApplicationUrl+HttpContext.Request.Path.ToString();
            int commaIndex = url.IndexOf(":");
            int serverNameBeginIndex = commaIndex + 3;
            int increase = url.Substring(serverNameBeginIndex).IndexOf("/");
            return url.Substring(0, serverNameBeginIndex + increase);            
        }


        /// <summary>
        /// Переход на страницу информирования об успешной регистрации
        /// </summary>
        /// <returns></returns>
        public IActionResult RegistrationComplete()
        {
            return View();
        }


        /// <summary>
        /// Получение статуся проверки свойства модели
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public string GetValidationState(string property)
        {
            string state = ModelState[property] == null ? "valid" :
                ModelState[property].ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid == true ? "valid" : "invalid";
            return state;
        }


        /// <summary>
        /// Переход на страницу авторизации пользователя
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {
            return View(new LoginModel());
        }


        /// <summary>
        /// Запрос авторизации пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Login([Bind("Email,Password")] LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    switch (state.Key)
                    {
                        case "Email": model.EmailValidationState = GetValidationState(state.Key); break;
                        case "Password": model.PasswordValidationState = GetValidationState(state.Key); break;
                    }
                }

                try
                {
                    string EncodedResponse = Request.Form["g-Recaptcha-Response"];
                    bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "true") ? true : false;
                    if (IsCaptchaValid)
                    {
                        ApplicationDb.Entities.User user = _authorization.Signin(model.Email, model.Password);
                        _notifications.AddNotification(new NotificationMessage()
                        {
                            Title = "Выполнен авторизованный вход в систему"
                        });
                        return Redirect("/Home/News");
                    }
                    else
                    {
                        TempData["recaptcha"] = "Подтвердите что Вы не робот";
                        return View(model);
                    }

                }
                catch (Exception ex)
                {
                    model.ErrorMessage = "Авторизация не выполнена. " + ex.Message;
                    return View(model);
                }

            }
            else
            {
                return View(model);
            }
        }
    }
}
