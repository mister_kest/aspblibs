﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ApplicationDb;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ASpbLibs.Application.ControllersMVC
{

    /// <summary>
    /// Необходимо уметь получать бинарные данные из ХД
    /// Сохранять в ХД
    /// Читать бинарные данные( отображать изображения )
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ResourceController : ControllerBase
    {
        private readonly ApplicationDbContext _db;


        /// <summary>
        /// Конструктор 
        /// </summary>
        /// <param name="db"> контекст базы данных </param>
        public ResourceController(ApplicationDbContext db) : base()
        {
            _db = db;
        }

  
        /// <summary>
        /// Вывод списка загруженных ресурсов
        /// </summary>       
        public async Task<Dictionary<string, object>> List()
        {
            Dictionary<string, object> resp = new Dictionary<string, object>();
            resp["data"] = await (from r in _db.Resources select r.ID).ToListAsync();
            resp["status"] = "success";
            return resp;
        }


        /// <summary>
        /// Используется вывода изображения
        /// </summary>
        /// <param name="id"> идентификатор ресурса</param>
        /// <returns></returns>
        [Route("/api/Resource/Use/{id:int}")]
        public async Task Use([FromRoute] int id)
        {
            Dictionary<string, object> resp = new Dictionary<string, object>();
            Resource resource = _db.Resources.Find(id);
            Response.ContentType = resource.Mime;
            byte[] data = resource.Data;
            await Response.Body.WriteAsync(data, 0, data.Length);
            resp["status"] = "success";
        }


        //string PhotoUrl = $"https://localhost:44347/Market/Products/PhotoImage/" + productId;
        [HttpPost()]
        public async Task<Dictionary<string, object>> Create()
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            string resourceName = Request.Headers["Resource-Name"];
            string mimeType = Request.Headers["Mime-Type"];
            Resource resource = new Resource()
            {
                Name = resourceName,
                Mime = mimeType,
                Created = DateTime.Now
            };
            long? length = this.HttpContext.Request.ContentLength;
            if (length != null)
            {
                resource.Data = new byte[(long)length];
                await this.HttpContext.Request.Body.ReadAsync(resource.Data, 0, (int)length);
            }
            _db.Add(resource);
            await _db.SaveChangesAsync();

            response["statuc"] = "success";
            response["id"] = resource.ID;
            response["url"] = $"/api/{GetType().Name.Replace("Controller", "")}/Use/{resource.ID}";
            return response;
        }
    }
}
