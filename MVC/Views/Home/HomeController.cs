﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreApp.AppAPI;
using ASpbLibs.Application.Abstraction.BookView;
using ApplicationDb.Entities;
using ASpbLibs.Application.Abstraction.SearchView;
using System.Collections.Generic;

namespace ASpbLibs.Application.ControllersMVC
{
    public class HomeController : SearchViewController<News>
    {
        private readonly ApplicationDbContext _context; 

        public HomeController(
                ApplicationDbContext context,
                APIAuthorization authorization ) : base( authorization )
        {       
            _context = context;
        }


        public override IQueryable<News> GetAll(SearchViewModel<News> model)
        {
            return _context.News.Where(n=> model.SearchQuery=="" || n.Title.ToLower().IndexOf(model.SearchQuery.ToLower())!=-1).OrderByDescending(n => n.Time);
        }


        public override string GetViewName()
        {
            return "News";
        }



        

              
        /// <summary>
        /// Переход на страницу просмотра
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> News()
        {
            User user = _authorization.Verify();
            if( user != null && user.Role.Code == "Admin")
            {
                return RedirectToAction("Search","News",new { 
                    area="AdminFace"
                });
            }
            return await SetPage(1);
        }



        /// <summary>
        /// Метод получения списка ключевых слов
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public override List<string> GetKeywords( string query )
        {
            HashSet<string> keywords = new HashSet<string>();
            List<string> titles =
                (from p in _context.News where p.Title.ToLower().IndexOf(query) != -1 
                    select p.Title ).Take(10).ToList();
            foreach(string title in titles)
            {
                foreach(string word in new List<string>(title.ToLower().Split(' ', '.', ',', '!')))
                {
                    if(String.IsNullOrEmpty(word.Trim())==false)
                    {
                        if(word.ToLower().StartsWith(query.ToLower()))
                        {
                            keywords.Add(word);
                        }
                        else
                        {
                            keywords.Add(query.Trim() + " " + word);
                        }
                    }
                    
                }
            }
            return new List<string>(keywords);
        }
    }
}
