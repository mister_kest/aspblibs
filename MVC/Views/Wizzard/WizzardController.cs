﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Wizzard
{
    public class WizzardController: Controller
    {
        public IActionResult HasReaderCard()
        {
            return View();
        }

        public IActionResult IHasReaderCard()
        {
            return RedirectToAction("Login", "Account");
        }

        public IActionResult IHasNotReaderCard()
        {
            return View();
        }

        public IActionResult IGoToReaderRoom()
        {
            return View();
        }
    }
}
