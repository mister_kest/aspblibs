﻿using ASpbLibs.Core.Services.Libs;
using ASpbLibs.Views.Categories;
using LibsDb.LibsEntities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ASpbLibs.Application.ControllersMVC
{
    public class CategoriesController: Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly CategoriesService _categories;

        public CategoriesController(CategoriesService categories,ApplicationDbContext context)
        {
            _context = context;
            _categories = categories;
        }

        public IActionResult Index()
        {           
            return View(_categories.GetCategoryHier());
        }


        /// <summary>
        /// Раскрывает/закрывает заданный узел
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IActionResult Expand( string key )
        {
            CategoryHier pnode = _categories.GetCategoryHier().Find(key);
            pnode.Expanded = pnode.Expanded ? false : true;
            return PartialView("_Categories",pnode);
        }

        public IActionResult Check(string key)
        {
            CategoryHier pnode = _categories.GetCategoryHier().Find(key);
            pnode.Checked = pnode.Checked ? false : true;
            pnode.DoRecursive((p)=> {
                p.Checked = pnode.Checked;
            });
            return PartialView("_Categories", pnode);
        }

    }
}
