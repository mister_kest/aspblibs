﻿using ASpbLibs.Common.CommonTypes;

namespace ASpbLibs.Views.Shared
{
    public class TabViewModel: PartialComponent
    {
        public string Title { get; set; }
        public string Icon { get; set; }
    }
}