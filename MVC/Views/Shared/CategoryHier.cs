﻿
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Application.Abstraction.TreeView;
using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Categories
{
    public class CategoryHier  
    {
        public Category Item { get; set; }
        public Dictionary<string, CategoryHier> Children { get; set; }
        public CategoryHier Parent { get; set; }

        public bool Checked { get; set; } = true;
        public bool Expanded { get; set; } = true;
        public bool Editable { get; set; } = false;

        public CategoryHier(CategoryHier parent, Category item)
        {
            Item = item;
            Children = new Dictionary<string, CategoryHier>();
        }


        public void DoRecursive( Action<CategoryHier> todo )
        {
            todo(this);
            foreach (var pair in Children)
            {
                pair.Value.DoRecursive(todo);
            }
        }

        public CategoryHier GetRoot()
        {
            CategoryHier p = this;
            while( p.Parent!=null)
            {
                p = p.Parent;
            }
            return p;
        }

        public CategoryHier GetChildByIndex( int index )
        {
            return Children.Values.ToArray()[index];
        }

        public CategoryHier Find( string path )
        {            
            CategoryHier pnode = this.GetRoot();
            if( path == "1")
            {
                return pnode;
            }
            string[] splices = path.Substring(2).Split("-");
            foreach (string splice in splices)
            {
                int index = int.Parse(splice);
                pnode = pnode.GetChildByIndex(index);
            }
            return pnode;
        }


        public int GetIndex()
        {
            return Parent.GetChildren().Values.ToList().IndexOf(this);
        }

        public string GetPath()
        {
            if (Parent!=null)
            {
                return Parent.GetPath() + "-" + this.GetIndex();
            }
            else
            {
                return "1";
            }            
        }

        public Category GetItem()
        {
            return Item;
        }


        public void SetParent(CategoryHier parent)
        {

            if (Parent != null && Parent.GetChildren().ContainsValue(this))
            {
                Parent.GetChildren().Remove(this.GetName());
            }
            Parent = parent;
            Parent.GetChildren()[GetName()] = this;
        }

        public void toggleExpanded()
        {
            Expanded = Expanded == true ? false : true;
        }

        public string GetName()
        {
            return Item.Name;
        }


        public Dictionary<string, CategoryHier> GetChildren()
        {
            return Children;
        }
    }
}
