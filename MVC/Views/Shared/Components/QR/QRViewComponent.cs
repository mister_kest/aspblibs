﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Shared.Components.QR
{
    public class QRViewComponent: ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(  )
        {
            return View(new List<string> { "1","2"});
        }

       
        public async Task<IViewComponentResult> Remove()
        {
        
            return View(new List<string> { "1"  });
        }

    }
}
