﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Shared
{
    public class PaginationViewComponent: ViewComponent
    {
             
        public int CurrentPage { get; set; } = 1;
        public int TotalPages { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        
        public int PrevPage()
        {
            return CurrentPage - 1;
        }
        public int NextPage()
        {
            return CurrentPage + 1;
        }

        public async Task<IViewComponentResult> InvokeAsync(int CurrentPage, int TotalPages, int PageSize)
        {
            this.CurrentPage = CurrentPage;
            this.TotalPages = TotalPages;
            this.PageSize = PageSize;

            return View(this);
        }
    }
}
