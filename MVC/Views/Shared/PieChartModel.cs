﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Shared
{
    public class PieChartModel
    {
        public string Title { get; set; } = "Pie Chart";

        public string Width { get; set; } = "640px";
        public string Height { get; set; } = "480px";


        /**
         * 
         * [
                    ['Task', 'Hours per Day'],
                    ['Work', 11],
                    ['Eat', 2],
                    ['Commute', 2],
                    ['Watch TV', 2],
                    ['Sleep', 7]
                ] */
        public Dictionary<string, float> KeyValues { get; set; } = new Dictionary<string, float>() {
            { "Work", 11 },          
            { "Eat", 2 },
            { "Commute", 2 },
            { "WatchTV", 2 },
            { "Sleep", 7 }
        };

        public string GetArrayToDataTableInJson()
        {
            string json = "[['Task', 'Hours per Day'],";
            foreach (var pair in KeyValues)
            {
                json += "['"+pair.Key + "',"+pair.Value+"],";
            }
            if (json.EndsWith(","))
            {
                json = json.Substring(0, json.Length - 1);
            }
            json += "]";
            return json;
        }
    }
}
