﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Shared
{
    public class TabsViewModel
    {
        public TabViewModel Active { get; set; }
        public List<TabViewModel> Tabs { get; set; } = new List<TabViewModel>();
    }
}
