﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb.LibsEntities;

namespace ASpbLibs.Views
{
    public class LiterDescriptionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LiterDescriptionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LiterDescriptions
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.LiterDescriptions.Include(l => l.Category).Include(l => l.Image);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: LiterDescriptions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var literDescription = await _context.LiterDescriptions
                .Include(l => l.Category)
                .Include(l => l.Image)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (literDescription == null)
            {
                return NotFound();
            }

            return View(literDescription);
        }

        // GET: LiterDescriptions/Create
        public IActionResult Create()
        {
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name");
            ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime");
            return View();
        }

        // POST: LiterDescriptions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Overview,Year,SelectCount,ImageID,CategoryID")] LiterDescription literDescription)
        {
            if (ModelState.IsValid)
            {
                _context.Add(literDescription);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", literDescription.CategoryID);
            ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", literDescription.ImageID);
            return View(literDescription);
        }

        // GET: LiterDescriptions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var literDescription = await _context.LiterDescriptions.FindAsync(id);
            if (literDescription == null)
            {
                return NotFound();
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", literDescription.CategoryID);
            ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", literDescription.ImageID);
            return View(literDescription);
        }

        // POST: LiterDescriptions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Overview,Year,SelectCount,ImageID,CategoryID")] LiterDescription literDescription)
        {
            if (id != literDescription.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(literDescription);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiterDescriptionExists(literDescription.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "ID", "Name", literDescription.CategoryID);
            ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", literDescription.ImageID);
            return View(literDescription);
        }

        // GET: LiterDescriptions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var literDescription = await _context.LiterDescriptions
                .Include(l => l.Category)
                .Include(l => l.Image)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (literDescription == null)
            {
                return NotFound();
            }

            return View(literDescription);
        }

        // POST: LiterDescriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var literDescription = await _context.LiterDescriptions.FindAsync(id);
            _context.LiterDescriptions.Remove(literDescription);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiterDescriptionExists(int id)
        {
            return _context.LiterDescriptions.Any(e => e.ID == id);
        }
    }
}
