﻿using ApplicationMVC.Views.About;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;

namespace ASpbLibs.Application.ControllersMVC
{
    /// <summary>
    /// Осуществляет навигацию по страницам справочной информации
    /// </summary>
    public class AboutController : Controller
    {
        private readonly ILogger<AboutController> _logger;
        private readonly ApplicationPartManager _partManager;


        public AboutController(
                ILogger<AboutController> logger,     
                ApplicationPartManager partManager)
        {
            _logger = logger;      
            _partManager = partManager;
            _logger.LogInformation("Create");
        }


        /// <summary>
        /// Переход к страницы проектной документации
        /// </summary>
        /// <returns></returns>
        public IActionResult ProjectDocumentation()
        {
            var viewModel = new ProjectDocumentationViewModel();
            var controllerFeature = new ControllerFeature();
            _partManager.PopulateFeature(controllerFeature);
            viewModel.Controllers = controllerFeature.Controllers.ToList();

            var tagHelperFeature = new TagHelperFeature();
            _partManager.PopulateFeature(tagHelperFeature);
            viewModel.TagHelpers = tagHelperFeature.TagHelpers.ToList();

            var viewComponentFeature = new ViewComponentFeature();
            _partManager.PopulateFeature(viewComponentFeature);
            viewModel.ViewComponents = viewComponentFeature.ViewComponents.ToList();

            return View(viewModel);
        }


        /// <summary>
        /// Переход к страницы описания разделения ответственности между пользователями
        /// </summary>
        /// <returns></returns>
        public IActionResult SeparationOfResponsibilities()
        {
            return View();
        }


        /// <summary>
        /// Переход к страницы содержащей политику конфиденциальности
        /// </summary>
        /// <returns></returns>
        public IActionResult PrivacyPolicy()
        {       
            return View();
        }


        /// <summary>
        /// Переход к страницы содержащей контактную информацию
        /// </summary>
        /// <returns></returns>
        public IActionResult ContactInformation()
        {
            return View();
        }
 
    }
}
