﻿using Microsoft.AspNetCore.Razor.TagHelpers;

using System.Collections.Generic;
using System.Reflection;

namespace ApplicationMVC.Views.About
{
    
    public class ProjectDocumentationViewModel
    {
        public ProjectDocumentationViewModel()
        {
        }
        
        public List<TypeInfo> ViewComponents { get; internal set; }
        public List<TypeInfo> TagHelpers { get; internal set; }
        public List<TypeInfo> Controllers { get; internal set; }
    }
}