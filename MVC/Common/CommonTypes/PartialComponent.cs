﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Common.CommonTypes
{
    public class PartialComponent
    {
        public string View { get; set; }
        public object Model { get; set; }
    }
}
