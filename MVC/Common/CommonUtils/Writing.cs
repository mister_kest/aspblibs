﻿using Microsoft.Extensions.Configuration;

using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ApplicationCommon.CommonUtils
{
    /// <summary>
    /// Реализует методы вывода
    /// </summary>
    public class Writing
    {
        public static void ToConsoleJson(object target)
        {
            Console.WriteLine(JObject.FromObject(target).ToString());
        }


        public static void ToConsole(string title, IDictionary<object, object> configuration)
        {
            ToConsole($"\n{title}");
            foreach (var pair in configuration)
            {
                ToConsole($"\t{pair.Key}={pair.Value}");
            }
            ToConsole($"\n");
        }

        public static void ToConsole(string title, IDictionary<string, object> configuration)
        {
            ToConsole($"\n{title}");
            foreach (var pair in configuration)
            {
                ToConsole($"\t{pair.Key}={pair.Value}");
            }
            ToConsole($"\n");

        }

        public static void ToConsole(string title, IConfiguration configuration)
        {
            ToConsole($"\n{title}");
            IEnumerator<KeyValuePair<string, string>> enumerator = configuration.AsEnumerable().GetEnumerator();
            while (enumerator.MoveNext())
            {
                ToConsole($"\t{enumerator.Current.Key}={enumerator.Current.Value}");
            }
            ToConsole($"\n");

        }



        public static void ToConsole(string message)
        {
            Console.WriteLine(message);
            Debug.WriteLine(message);
        }

        public static void ToConsole(string title, string[] messages)
        {
            ToConsole($"\n{title}");
            foreach (string message in messages)
            {
                ToConsole($"\t {message}");
            }
        }
    }
}
