﻿namespace ApplicationCommon.CommonUtils
{
    /// <summary>
    /// Перечисление стилей записи идентификаторов
    /// </summary>
    public enum NamingStyles
    {
        Capital, Kebab, Snake, Camel
    }
}
