﻿@Component({
    selector: 'frame',
    template:  
    `     
    <div class="card" style="width: 100%; border-width: 2px black;">
      <div class="card-header text-white bg-primary">


          <ng-content select=".frame-header"></ng-content>

      </div>
      <div class="card-body">
        <ng-content select="*:not(.frame-header):not(.frame-footer)"></ng-content>
      </div>
      <div style="display: flex; flex-direction: row; justify-content: flex-end;">
      </div>
    </div>

    <div style="width: 100%; height: 100%; display: flex; flex-direction: row;" >
        <div style="height: 100%;" >
            left
        </div>
        <div style="width: 100%; height: 100%; display: -webkit-flex; display: flex; flex-direction: column; overflow-y: auto;">
            <ng-transclude></ng-transclude>
        </div>
        <div style="height: 100%;">
            right
        </div>
    </div>
    `
})
class FrameComponent
{

}




@Component({
    template: `
        <div class="card text-center">
          <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">

              <li class="nav-item" *ngFor="let tab of tabs">
                <a class="nav-link active" (click)="set(tab)">{{ tab.label }}</a>
              </li>

            </ul>
          </div>
          <div class="card-body">
            <h5 class="card-title">
app-tabs
              <!-- <div *ngIf="loginForm.enabled">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">{{ loginForm.status }}</span>
                </div>
              </div> -->

              Login
            </h5>
            <div class="card-text">
              <ng-container *ngComponentOutlet="component"></ng-container>
                <!-- <app-login-form #loginForm></app-login-form> -->
            </div>
            <!-- <a href="#" class="btn btn-primary"(click)="loginForm.login('','')">login</a> -->
          </div>
        </div>

    `,
    selector: 'app-tabs'
})
export class TabsComponent {
    ngOnInit(): void {
        this.component = this.tabs[0].component;
    }

    //@Input()
    tabs: Array<{
        label: string,
        component: any
    }> = [
            {
                label: 'pagination',
                component: null//PaginationComponent
            },
            {
                label: 'table',
                component: null//TableComponent
            }

        ];

    component = null//PaginationComponent;

    set(tab) {
        this.component = tab.component;
    }
}
