﻿function $context() {
    if (!window['app']) {
        return window['app'] = {
            modules:    [],
            components: [],
            directives: [],
            services:   [],
            controllers:[]
        };
    } else {
        return window['app'];
    }
}
