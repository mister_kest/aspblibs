﻿function Controller(options) {
    console.log('Component');
    return function specification(constructor: Function) {
        $context().controllers.push({
            options:        options,
            constructor:    constructor
        });
    };
}