﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.TreeView
{
    public interface ICheckboxTree<T> : ITreeNode<T>
    {
        public bool Checked { get; set; }
    }
}
