﻿
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Application.Abstraction.TreeView;
using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Categories
{
    public class TreeNode : MvcComponent, ITreeNode<Category>
    {
        private readonly Category _item;
        private readonly Dictionary<string, ITreeNode<Category>> _children;
        private ITreeNode<Category> _parent;
 
        public bool Expanded = true;
        public bool Editable = false;

        public TreeNode(CategoryHier parent, Category item)
        {
            _item = item;
            _children = new Dictionary<string, ITreeNode<Category>>();
        }


        public Category GetItem()
        {
            return _item;
        }



        public void SetParent(ITreeNode<Category> parent)
        {
            if (_parent != null && _parent.GetChildren().ContainsValue(this))
            {
                _parent.GetChildren().Remove(this.GetName());
            }
            _parent = parent;
            _parent.GetChildren()[GetName()] = this;
        }


        public void toggleExpanded()
        {

            Expanded = Expanded == true ? false : true;
        }


        public void toggleSelected()
        {
            Selected = Selected == true ? false : true;
        }





        public string GetName()
        {
            return _item.Name;
        }


        public Dictionary<string, ITreeNode<Category>> GetChildren()
        {
            return _children;
        }
    }
}
