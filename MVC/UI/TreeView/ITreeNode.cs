﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.TreeView
{
    public interface ITreeNode<T>
    {
        T GetItem();
        void SetParent(ITreeNode<T> parent);
        Dictionary<string, ITreeNode<T>> GetChildren();
    }
}
