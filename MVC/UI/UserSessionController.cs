﻿using ApplicationDb.Entities;
using ASpbLibs.Core.Services.Domain.Reflection;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction
{

    /// <summary>
    /// Констроллер работающий с моделью сеанса
    /// </summary>
    /// <typeparam name="TModel"> тип модели сеанса </typeparam>
    public abstract class UserSessionController<TModel>: Controller
    {
        protected readonly APIAuthorization _authorization;
        private readonly IHttpContextAccessor _accessor;


        /// <summary>
        /// Конструктор контроллера модели сеанса
        /// </summary>
        /// <param name="authorization"> сервис авторизации </param>
        public UserSessionController( APIAuthorization authorization )
        {            
            _authorization = authorization;
            
        }


        /// <summary>
        /// Переход на страницу просмотра
        /// </summary>
        /// <returns></returns>
        public virtual async Task<IActionResult> Index()
        {
            return View(GetModel());
        }


        /// <summary>
        /// Метод инициаллизации модели
        /// </summary>
        /// <param name="model"></param>
        public abstract void InitModel(TModel model);


        /// <summary>
        /// Создание новой модели сеанса
        /// </summary>
        /// <returns></returns>
        public TModel NewModel()
        {
            string key = GetType().FullName + "::" + typeof(TModel).FullName;
            ConcurrentDictionary<string, object> session = _authorization.Session();
            TModel order = ReflectionService.CreateWithDefaultConstructor<TModel>(typeof(TModel));
            session[key] = order;
            InitModel(order);
            return (TModel)session[key];
        }


        /// <summary>
        /// Получение модели сеанса, если модели не существует выполняется её инициаллизация
        /// </summary>
        /// <returns></returns>
        public TModel GetModel()
        {
            string key = GetType().FullName + "::" + typeof(TModel).FullName;

            bool IsSigned = _authorization.IsSignin();
            User user = _authorization.Verify();
            ConcurrentDictionary<string, object> session = _authorization.Session();
            if( session == null )
            {
                _authorization.Signout();
                throw new Exception("Authentication failed");
            }
            else
            {
                if (session.ContainsKey(key) == false)
                {
                    TModel order =
                        ReflectionService.CreateWithDefaultConstructor<TModel>(typeof(TModel));
                    session[key] = order;
                    InitModel(order);
                }
                return (TModel)session[key];

            }
        }


        /// <summary>
        /// Получение модели сеанса из другого контроллера
        /// </summary>
        /// <typeparam name="T"> тип модели сеанса </typeparam>
        /// <param name="key"> ключ доступа </param>
        /// <returns></returns>
        public T GetAnotherModel<T>(string key)
        {                       
            ConcurrentDictionary<string, object> session = _authorization.Session();
            if (session.ContainsKey(key) == false)
            {
                T order =
                    ReflectionService.CreateWithDefaultConstructor<T>(typeof(T));
                session[key] = order;                
            }
            return (T)session[key];
        }


        /// <summary>
        /// Получение модели сеанса из другого контроллера
        /// </summary>
        /// <param name="controllerType"> тип контроллера </param>
        /// <returns></returns>
        public TModel GetModel(Type controllerType)
        {
            string key = controllerType.FullName + "::" + typeof(TModel).FullName;
            ConcurrentDictionary<string, object> session = _authorization.Session();
            if( session == null)
            {
                _accessor.HttpContext.Response.Redirect("");
                throw new Exception("User not authorizated at system");
            }
            else
            {
                if (session.ContainsKey(key) == false)
                {
                    TModel order =
                        ReflectionService.CreateWithDefaultConstructor<TModel>(typeof(TModel));
                    session[key] = order;
                    InitModel(order);
                }
                return (TModel)session[key];
            }
        }
    }
}
