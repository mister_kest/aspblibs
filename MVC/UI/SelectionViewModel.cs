﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.SearchView
{
    public class SelectionViewModel<T>
    {
        public T Item { get; set; }
        public bool _Selected { get; set; }
        public bool Selected 
        { 
            get; 
            set; 
        }

        public SelectionViewModel(T item)
        {
            Item = item;
        }
    }
}
