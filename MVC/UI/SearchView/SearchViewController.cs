﻿using ASpbLibs.Application.Abstraction.BookView;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace ASpbLibs.Application.Abstraction.SearchView
{

    /// <summary>
    /// Контроллер поиска и постраничного просмотра записей в базе данных.
    /// Для реализации исп. наследование.
    /// После инициаллизации необходимо установить в поле EntityName имя сущности
    /// </summary>
    public abstract class SearchViewController<T> : UserSessionController<SearchViewModel<T>>
    {

        /// <summary>
        /// Получение всех возможных элементов
        /// </summary>
        /// <returns></returns>
        public abstract IQueryable<T> GetAll(SearchViewModel<T> model);


        /// <summary>
        /// Получение наименования представления
        /// </summary>
        /// <returns></returns>
        public abstract string GetViewName();


        /// <summary>
        /// Получение наименования представления
        /// </summary>
        /// <returns></returns>
        public abstract List<string> GetKeywords(string query);


        /// <summary>
        /// Констутруктор
        /// </summary>
        /// <param name="crud"> Сервис поддерживающий постраничную выборку и операции чтения записи</param>
        /// <param name="authorization"> Сервис авторизации </param>
        public SearchViewController(APIAuthorization authorization) : base(authorization)
        {
        }


        public async Task<IActionResult> Search(string query)
        {
            var model = GetModel();
            model.SearchQuery = query==null? "": query;

            return await SetPage(1);
        }


        /// <summary>
        /// Обработка запроса ключевый слов
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IActionResult OnInput( string query )
        {
            return Json(this.GetKeywords(query));
        }


        /// <summary>
        /// Инициаллизация модели
        /// </summary>
        /// <param name="model"></param>
        public override void InitModel(SearchViewModel<T> model)
        {
            model.SearchQuery = "";
           
            foreach(var p in Request.RouteValues)
            {
                if (p.Key == "area")
                {
                    model.Area = p.Value.ToString();
                }
                if (p.Key == "controller")
                {
                    model.Controller = p.Value.ToString();
                }
            }
            model.UpdatePages(GetAll(model).Count());
        }
 
        /// <summary>
        /// Переход на страницу с заданным номером
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetPage(int page)
        {
            var model = GetModel();
            if (page <= 0 || page > model.TotalPages)
            {
                return NotFound();
            }
            else
            {
                model.CurrentPage = page;
                model.Results = await GetAll(model).Skip((page - 1) * model.PageSize).Take(model.PageSize).ToListAsync();
                model.UpdatePages(GetAll(model).Count());
                string view = GetViewName();
                return View(view, model);
            }
        }


        /// <summary>
        /// Переход на страницу с заданным номером
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetPageSize(int size)
        {
            var model = GetModel();
            if (size <= 0)
            {
                return NotFound();
            }
            else
            {
                model.PageSize = size;
                model.CurrentPage = 1;
                model.UpdatePages(GetAll(model).Count());
                return await SetPage(1);
            }
        }
    }
}
