﻿using ASpbLibs.Application.Abstraction.BookView;

namespace ASpbLibs.Application.Abstraction.SearchView
{
    public class SearchViewModel<T>: BookViewModel<T>
    {
        public string Area { get; set; }
        public string Controller { get; set; }        
        public string SearchQuery { get; set; }    

        public string GetEndpointUrl()
        {
            if (string.IsNullOrEmpty(Area))
            {
                return "/" + Controller;
            }
            else
            {
                return "/"+Area+"/" + Controller;
            }
        }
    }
}
