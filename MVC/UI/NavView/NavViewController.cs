﻿using ASpbLibs.Application.Abstraction;

using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.NavView
{
    public abstract class NavViewController<T> : UserSessionController<T> where T: NavViewModel
    {
        public NavViewController(APIAuthorization authorization) : base(authorization)
        {
        }

        

        public IActionResult NavigateTo( string key )
        {
            NavViewModel model = GetModel();
            model.SetActive(key);
            return View("Index",model);
        }
    }
}
