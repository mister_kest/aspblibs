﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.NavView
{
    public class NavViewModel
    {
        public string Active { get; set; }
        public ConcurrentDictionary<string, object> Nav { get; set; } = new ConcurrentDictionary<string, object>();

        public void SetActive(string key)
        {
            Active = key;
        }
    }
}
