﻿
using ASpbLibs.Core.Services.Domain.Reflection;

using ApplicationDb.Entities;
 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ASpbLibs.Core.Services.CRUD
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityRepository<T>
    {
        private DbContext _context;
        private dynamic _dbset;
        private string _name;
         
        public EntityRepository( DbContext context, dynamic dbset, string name)
        {
            _context = context;
            _dbset = dbset;
            _name = name;             
        }


        public void SetDataSet(dynamic _dbset)
        {
            this._dbset = _dbset;
        }


        

        /// <summary>
        /// Создание новой записи
        /// </summary>
        /// <param name="record"></param>
        public void Create(T record)
        {            
            _dbset.Add(record);
            _context.SaveChanges();             
        }

        public int Count()
        {
            return ((IQueryable<T>)_dbset).Count();
        }


        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            _dbset.Remove(_dbset.Find(id));
            _context.SaveChanges();
        }


        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="item"></param>
        public void Remove(T item)
        {
            _dbset.Remove(item);
            _context.SaveChanges();
        }


        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Find(int id)
        {
            return _dbset.Find(id);
        }

        /// <summary>
        /// Проверка наличия записи с уникальным свойством
        /// </summary>
        /// <param name="propertyName">имя свойства</param>
        /// <param name="propertyValue">значение свойства</param>
        /// <returns></returns>
        public bool IsUniq<TEntity>( string propertyName, object propertyValue)
        {
            ReflectionService reflection = new ReflectionService();            
            return ((IQueryable<TEntity>)_dbset).Where(p => reflection.GetValue(p, propertyName).ToString() == propertyValue.ToString()).Count() == 0;
        }


        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T> FindRange(int x1, int x2 )
        {
            ReflectionService reflection = new ReflectionService();
            return (from i in ((IQueryable<T>)_dbset)
             where (int)reflection.GetValue(i, "ID") >= x1
                 && (int)reflection.GetValue(i, "ID") < x2
             select i).ToList();
        }


        public void Update(dynamic item)
        {
            object target = _dbset.Find(item.ID);
            ReflectionService reflection = new ReflectionService();
            reflection.Copy(item,target );            
            _context.SaveChanges();
        }


        public System.Collections.Generic.List<T> List()
        {            
            return ((IQueryable<T>)_dbset).ToList();
        }



        public System.Collections.Generic.List<T> Page( int number, int size )
        {
            return ((IQueryable<T>)_dbset).Skip(size*number).Take(size).ToList();
        }



        public IEnumerable<INavigation> GetNavigationProperties()
        {                    
            IEntityType entity = (from navs in _context.Model.GetEntityTypes() where navs.Name == typeof(T).FullName select navs).SingleOrDefault();
            return entity.GetNavigations();          
        }

    }
}
