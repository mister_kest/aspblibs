﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction
{
    public class MvcComponentAttribute : Attribute, IControllerModelConvention
    {
         
        private readonly string _description;

        public MvcComponentAttribute(string description)
        {
            _description = description;
        }

        public void Apply(ControllerModel controllerModel)
        {
            controllerModel.Properties["description"] = _description;
        }
         
    }
}
