﻿

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ASpbLibs.Application.Abstraction
{
    //DataTypes:
    //CreditCard: отображает номер кредитной карты
    //Currency: отображает текст в виде валюты
    //Date: отображает только дату, без времени
    //DateTime: отображает дату и время
    //Time: отображает только время
    //Duration: отображает число - некоторую продолжительность
    //EmailAddress: отображает электронный адрес
    //Password: отображает символы с использованием маски
    //PhoneNumber: отображает номер телефона
    //PostalCode: отображает почтовый индекс
    //ImageUrl: представляет путь к изображению
    //Url: отображает строку Url
    //MultilineText: отображает многострочный текст(элемент textarea)
    //Text: отображает однострочный текст

    //UIHints:
    //Boolean
    //Хелперы редактирования создают флажок(checkbox) для булевых значений.Для значений типа bool? (nullable) 
    //создается элемент select с параметрами True, False и Not Set
    //Хелперы отображения генерируют те же элементы html, что и хелперы редактирования, только с атрибутом disabled
    //Collection
    //Используется соответствующий шаблон для рендеринга каждого элемента коллекции.Причем элементы могут быть разных типов.
    //Decimal
    //Хелперы редактирования создают однострочное текстовое поле - элемент input
    //EmailAddress
    //Хелперы редактирования создают однострочное текстовое поле.
    //Хелперы отображения генерируют элемент ссылка, где атрибут href имеет значение mailto:url
    //HiddenInput
    //Создается скрытое поле - элемент hidden input
    //Html
    //Хелперы редактирования создают однострочное текстовое поле.
    //Хелперы отображения просто показывают текст
    //MultilineText
    //Хелперы редактирования создают многострочное текстовое поле (элемент textarea)
    //Object
    //Хелперы изучают свойства объекта и выбирают наиболее подходящие для него шаблоны.
    //Password
    //Хелперы редактирования создают текстовое поле для ввода символов с использованием маски
    //Хелперы отображения показывают пароль как есть, без использования маски
    //String
    //Хелперы редактирования создают однострочное текстовое поле
    //Url
    //Хелперы редактирования создают текстовое поле
    //Хелперы отображения создают элемент ссылки для данного Url
    public class MvcComponent
    {
        public string id;
        public MvcComponent parent;
        public Dictionary<string,MvcComponent> children = new Dictionary<string, MvcComponent>();

        public string location;
        public string index;

        public bool Selected = false;
        public bool Visible = true;

        public MvcComponent( )
        {
            this.id = this.GetType().Name;                   
        }


        public string GetPath()
        {
            if( this.parent != null)
            {
                return this.parent.GetPath() + "-" + new List<MvcComponent>(this.parent.children.Values).IndexOf(this);
            }
            else
            {
                return "1";
            }
        }

        public MvcComponent Find( string id )
        {
            string[] ids = id.Substring(id.IndexOf("-")+1).Split("-");      
            MvcComponent p = this;
            foreach (string next in ids)
            {
                p = p.children.Values.ToArray()[int.Parse(next)-1];
            }                        
            return p;
        }


        public void Init()
        {
            Console.WriteLine(this + " init");
        }


        public void Destroy()
        {
            Console.WriteLine(this + " init");
        }


        public string Request(string action, object args)
        {
            return $"$request({JObject.FromObject(new { url = this.location, action= action, args = args, id= this.index }) });";
        }





        public void Add(MvcComponent tochild)
        {
            if (tochild.location == null)
            {
                tochild.location = location;
            }
            this.children[tochild.id] = tochild;
            tochild.index = this.index + "-" + this.children.Count;
            tochild.parent = this;
        }


        /// <summary>
        /// Наименование представление Partial
        /// </summary>
        /// <returns></returns>
        public string GetView()
        {
            return "_"+this.GetType().Name;
        }
    }
}
