﻿using System.Collections.Generic;

namespace ASpbLibs.Core.Services.Converter.Models
{
    /// <summary>
    /// Коллекция сетевых сервисов
    /// </summary>
    public class MyApplicationModel: Dictionary<string, MyControllerModel>
    {
    }
}
