﻿using System.Collections.Generic;

namespace ASpbLibs.Core.Services.Converter.Models
{
    /// <summary>
    /// Модель параметров вызова удаленной процедуры
    /// </summary>
    public class MyActionModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public Dictionary<string, MyParameterModel> Parameters { get; set; }
         
    }
}