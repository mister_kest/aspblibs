﻿using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.BookView
{

    /// <summary>
    /// Контроллер постраничного просмотра записей в базе данных.
    /// Для реализации исп. наследование.
    /// После инициаллизации необходимо установить в поле EntityName имя сущности
    /// </summary>
    public abstract class BookViewController<T>: UserSessionController<BookViewModel<T>> 
    {

        /// <summary>
        /// Получение всех возможных элементов
        /// </summary>
        /// <returns></returns>
        public abstract IQueryable<T> GetAll();


        /// <summary>
        /// Получение наименования представления
        /// </summary>
        /// <returns></returns>
        public abstract string GetViewName();


        /// <summary>
        /// Констутруктор
        /// </summary>
        /// <param name="crud"> Сервис поддерживающий постраничную выборку и операции чтения записи</param>
        /// <param name="authorization"> Сервис авторизации </param>
        public BookViewController( APIAuthorization authorization) : base(authorization)
        {
        }


        /// <summary>
        /// Инициаллизация модели
        /// </summary>
        /// <param name="model"></param>
        public override void InitModel(BookViewModel<T> model)
        {
            model.UpdatePages(GetAll().Count());
            
        }


        /// <summary>
        /// Переход на страницу просмотра
        /// </summary>
        /// <returns></returns>
        public async override Task<IActionResult> Index()
        {
            return await SetPage(1);
        }


        /// <summary>
        /// Переход на страницу с заданным номером
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetPage(int page)
        {
            var model = GetModel();
            if (page <= 0 || page>model.TotalPages)
            {
                return NotFound();
            }
            else
            {
                model.CurrentPage = page;
                model.Results = await GetAll().Skip((page - 1) * model.PageSize).Take(model.PageSize).ToListAsync();
                return View(GetViewName(), model);
            }
        }


        /// <summary>
        /// Переход на страницу с заданным номером
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetPageSize(int size)
        {
            var model = GetModel();
            if (size <= 0 )
            {
                return NotFound();
            }
            else
            {
                model.PageSize = size;
                model.CurrentPage = 1;
                model.UpdatePages(GetAll().Count());
                return await SetPage(1);
            }
        }
    }
}
