﻿using ASpbLibs.Application.Abstraction.SearchView;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Application.Abstraction.BookView
{
    /// <summary>
    /// Модель представления постраничного просмотра
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BookViewModel<T>: PageNavViewModel
    {
        public List<T> Results { get; set; }

    }
}
