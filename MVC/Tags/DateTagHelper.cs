﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Tags
{
    public class DateTagHelper : TagHelper
    {
        public DateTime Value { get; set; }

 


        public override void Process(
                TagHelperContext context, 
                    TagHelperOutput output)
        {
            output.TagName = "text";                 
            output.Content.SetHtmlContent($"{GetDateString(Value)}");
        }

        private string GetDateString(System.DateTime date)
        {
            string str = "";
            str += date.Day < 10 ? $"0{date.Day}" : date.Day.ToString();
            str += "."+(date.Month < 10 ? $"0{date.Month}" : date.Month.ToString());
            str += "." + date.Year;
            return str;
        }

        
    } 
}
