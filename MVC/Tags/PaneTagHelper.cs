﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Tags
{
    public class PaneTagHelper: TagHelper
    {
        public override void Process(
            TagHelperContext context,
                TagHelperOutput output)
        {
            output.TagName = "div";
            output.Attributes.SetAttribute("style", "overflow-y: auto; height: 100%;border-radius: 5px; box-shadow: 0px 1px 1px 2px rgba(0, 0, 0, 0.3), 3px 2px 2px 3px rgba(0, 0, 0, 0.24), -2px 2px 6px -2px rgba(0, 0, 0, 0.22); padding: 10px; padding-top: 20px; padding-bottom: 20px;");
        }
    }
}
