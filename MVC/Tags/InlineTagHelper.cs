﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Views.Tags
{
    public class InlineTagHelper : TagHelper
    {
        public override void Process(
                TagHelperContext context, 
                    TagHelperOutput output)
        {
            output.TagName = "div";
            output.Attributes.SetAttribute("align", "left");
            output.Attributes.SetAttribute("style", "display: flex; flex-direction: row: flex-wrap: nowrap; width: 100%; justify-items: flex-start; align-items: flex-start;");         
        }
        
    } 
}
