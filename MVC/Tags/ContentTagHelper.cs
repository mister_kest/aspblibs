﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Tags
{
    public class ContentTagHelper: TagHelper
    {
        public override void Process(
            TagHelperContext context,
                TagHelperOutput output)
        {
            output.TagName = "div";
            output.Attributes.SetAttribute("style","padding: 10px;");
        }
    }
}
