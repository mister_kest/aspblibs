    function request(url){ 
        console.log(url);
        const xhr = new XMLHttpRequest();
        xhr.open('get',url,false);
        xhr.send();
        return xhr.responseText;
    }
    
    function replaceAll( s, s1,s2 ){
        while(s.indexOf(s1)!=-1){
            s=s.toString().replace(s1,s2);
        }
        return s;
    }
    function getColors( file ){
        try{
          var text = request( file );
          text = replaceAll( text, '\t', ' ' );
          text = replaceAll( text, '  ', ' ' );
          var strings = text.split(  '\n' );
          
          var colors = new Array();
          for ( var i=0; i<strings.length; i++ ){
            var arr = strings[i].split(  ' ' );
            colors.push( arr[1] );
          }
          return colors;
        } catch ( e ){
          document.write( e );
          throw e;
        }
    }
      
      function testColors(){
        try{
          var files = [
            'colors/basic-colors.js',
            'colors/dark-colors.js',
            'colors/hsl-colors.js',
            'colors/hsv-colors.js',
            'colors/hue-colors.js',
            'colors/light-colors.js'
          ];
          for ( var i=0; i<files.length; i++ ){
            var colors = getColors(  files[ i ]  );
            document.write( '<h1>' + files[ i ] + '<h1/>');
            for ( var j=0; j<colors.length; j++ ){
              var color = colors[j];
              document.write( '<button style="background-color: ' + color + ';" onclick="alert(this.style.backgroundColor)">' + color + '</button>' );
            }
            document.write( colors.toHTML() + '<br/><br/>');
          }
        } catch ( e ){
          document.write( e );
        }
      }
      
      
      