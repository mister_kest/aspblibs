﻿/**
 * Обновление частиного представления методом get 
 * @param {any} id идентификатор контейнера
 * @param {any} query строка запроса 
 */
function UpdatePartialView(id, query) {
    try {
        
        const element = document.getElementById(id);
        /*
        if (!element)
            throw new Error('Невозможно обновить частичное представление. ' +
                'Не найден элемент с идентификатором: ' + id);*/

        const req = new XMLHttpRequest();
        req.open('get', query, true);
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                element.innerHTML = req.responseText;
                console.log(req.responseText);
            }
        };
        req.send();        
    } catch (e) {
        alert(e);
    }
}

///
function ToggleLeftSide() {    
    try {
        UpdatePartialView("id-left-side", '/UserFace/PageLayout/OnClickHome');
    } catch (e) {
        alert(e);
    }    
}