function Controller(options) {
    console.log('Component');
    return function specification(constructor) {
        $context().controllers.push({
            options: options,
            constructor: constructor
        });
    };
}
//# sourceMappingURL=controller.js.map