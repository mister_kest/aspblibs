var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
let FrameComponent = class FrameComponent {
};
FrameComponent = __decorate([
    Component({
        selector: 'frame',
        template: `     
    <div class="card" style="width: 100%; border-width: 2px black;">
      <div class="card-header text-white bg-primary">


          <ng-content select=".frame-header"></ng-content>

      </div>
      <div class="card-body">
        <ng-content select="*:not(.frame-header):not(.frame-footer)"></ng-content>
      </div>
      <div style="display: flex; flex-direction: row; justify-content: flex-end;">
      </div>
    </div>

    <div style="width: 100%; height: 100%; display: flex; flex-direction: row;" >
        <div style="height: 100%;" >
            left
        </div>
        <div style="width: 100%; height: 100%; display: -webkit-flex; display: flex; flex-direction: column; overflow-y: auto;">
            <ng-transclude></ng-transclude>
        </div>
        <div style="height: 100%;">
            right
        </div>
    </div>
    `
    })
], FrameComponent);
let TabsComponent = class TabsComponent {
    constructor() {
        this.tabs = [
            {
                label: 'pagination',
                component: null
            },
            {
                label: 'table',
                component: null
            }
        ];
        this.component = null;
    }
    ngOnInit() {
        this.component = this.tabs[0].component;
    }
    set(tab) {
        this.component = tab.component;
    }
};
TabsComponent = __decorate([
    Component({
        template: `
        <div class="card text-center">
          <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">

              <li class="nav-item" *ngFor="let tab of tabs">
                <a class="nav-link active" (click)="set(tab)">{{ tab.label }}</a>
              </li>

            </ul>
          </div>
          <div class="card-body">
            <h5 class="card-title">
app-tabs
              <!-- <div *ngIf="loginForm.enabled">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">{{ loginForm.status }}</span>
                </div>
              </div> -->

              Login
            </h5>
            <div class="card-text">
              <ng-container *ngComponentOutlet="component"></ng-container>
                <!-- <app-login-form #loginForm></app-login-form> -->
            </div>
            <!-- <a href="#" class="btn btn-primary"(click)="loginForm.login('','')">login</a> -->
          </div>
        </div>

    `,
        selector: 'app-tabs'
    })
], TabsComponent);
export { TabsComponent };
//# sourceMappingURL=frame.js.map