﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Расширение класса DbContext
/// </summary>
public static class DbContextExtensions
{
    /// <summary>
    /// Получение свойств навигации 
    /// </summary>    
    /// <param name="singleRecord"> обьект </param>
    /// <returns></returns>
    public static IEnumerable<INavigation> GetNavigationProperties(this DbContext _context, object singleRecord )
    {        
        IEntityType entity = (from navs in _context.Model.GetEntityTypes() where navs.Name == singleRecord.GetType().FullName select navs).SingleOrDefault();
        return entity.GetNavigations();
    }
}

