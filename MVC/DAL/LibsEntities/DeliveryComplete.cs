﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL.LibsEntities
{
    public class DeliveryComplete: ApplicationDb.Types.FactsTable
    {
        public int ID { get; set; }

        public int DeliveryOrderID { get; set; }
        public virtual LibsDb.LibsEntities.DeliveryOrder DeliveryOrder { get; set; }


        public int ComplectatorID { get; set; }
        public virtual LibsDb.LibsEntities.Complectator Complectator { get; set; }
    }
}
