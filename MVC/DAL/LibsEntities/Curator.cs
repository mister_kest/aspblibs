﻿using ApplicationDb.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibsDb.LibsEntities
{
    public class Curator
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Куратор")]
        public int UserID { get; set; }
        public virtual User User { get; set; }


        [DisplayName("ЦБС")]
        public int LibID { get; set; }
        public virtual Lib Lib { get; set; }
    }
}
