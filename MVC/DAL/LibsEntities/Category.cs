﻿using ApplicationDb.Types;

using System.ComponentModel;

namespace LibsDb.LibsEntities
{
    [DisplayName("Категория")]
    public class Category: HierDictionaryTable<Category>
    {

        public int ID { get; set; }

        public Category() { }

        public Category(int id, string name, Category parent) {
           
            Name = name;
            Parent = parent;
        }

    }
}
