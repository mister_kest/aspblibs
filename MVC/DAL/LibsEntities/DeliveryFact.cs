﻿using ApplicationDb.Entities;
using ApplicationDb.Types;

using ASpbLibs.DAL.LibsEntities;

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{


    [DisplayName("Сведения о выдаче литературы")]
    public class DeliveryFact: FactsTable
    {
        [Key]
        public int ID { get; set; }

        public int? ReturnFactID { get; set; }
        public virtual ReturnFact ReturnFact { get; set; }


        [DisplayName("Литература")]
        public int LiteratureID { get; set; }
        public virtual Liter Literature { get; set; }


        [DisplayName("Заявка")]
        public int OrderID { get; set; }
        public virtual DeliveryOrder Order { get; set; }

        //[DisplayName("Возврат")]
        //public int ReturnOrderID { get; set; }
        //public virtual ReturnOrder ReturnOrder { get; set; }


        [DisplayName("Вернуть до")]
        public DateTime ReturnBefore { get; set; }


        [DisplayName("Продление")]
        public bool HasBeenIncresed { get; set; }
    }
}
