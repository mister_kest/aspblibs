﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{
    [DisplayName("Журнал")]
    public class Journal
    {
        [Key]
        public int ID { get; set; }

        public int LiterID { get; set; }
        public virtual Liter Liter { get; set; }


    }
}
