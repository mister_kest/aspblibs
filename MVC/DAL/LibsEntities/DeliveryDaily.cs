﻿using ApplicationDb.Types;
using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.DAL.LibsEntities
{
    public class DeliveryDaily: DailyStatsTable
    {
        public int ID { get; set; }

        public int HolderID { get; set; }
        public virtual Holder Holder { get; set; }
        public int Value { get; internal set; }
    }
}
