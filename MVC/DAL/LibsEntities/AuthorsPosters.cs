﻿using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL.LibsEntities
{
    public class AuthorsPosters
    {
        [Key]
        public int ID { get; set; }

        public int PosterID { get; set; }
        public virtual LiterDescription Poster { get; set; }

        public int AuthorID { get; set; }
        public virtual Author Author { get; set; }
    }
}
