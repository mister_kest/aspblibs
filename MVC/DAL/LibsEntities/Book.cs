﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{
    [DisplayName("Книга")]
    public class Book 
    {

        [Key]
        public int ID { get; set; }

        public int DescriptionID { get; set; }
        public virtual LiterDescription Description { get; set; }
    }
}
