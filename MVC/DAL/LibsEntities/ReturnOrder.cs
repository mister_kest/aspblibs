﻿using ASpbLibs.DAL.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibsDb.LibsEntities
{
    public class ReturnOrder
    {
        public ReturnOrder()
        {
            
        }

        public int ID { get; set; }
 

        public virtual List<ReturnFact> ReturnFacts { get; set; }
    }
}
