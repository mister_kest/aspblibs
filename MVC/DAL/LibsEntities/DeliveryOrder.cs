﻿using ApplicationDb.Entities;
using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibsDb.LibsEntities
{

    [DisplayName("Заявка на выдачу литературы")]
    public class DeliveryOrder
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Читатель")]
        public int UserID { get; set; }
        public virtual User User { get; set; }



        /// <summary>
        /// Ожидание потребителя
        /// </summary>
        public bool WaitingConsumer { get; set; }

        /// <summary>
        /// Время поступления на пункт выдачи
        /// </summary>
        public DateTime Arrived { get; set; }


        public int Status { get; set; }


        public List<DeliveryFact> Deliveries = new List<DeliveryFact>();
    }
}
