﻿using ApplicationDb.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibsDb.LibsEntities
{
    public class Reception
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Ресепшен")]
        public int UserID { get; set; }
        public virtual User User { get; set; }


        [DisplayName("Место выдачи")]
        public int HolderID { get; set; }
        public virtual Holder Holder { get; set; }
    }
}
