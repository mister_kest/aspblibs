﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibsDb.LibsEntities
{
    [DisplayName("Литературная единица выдачи")]
    public class Liter
    {
        [Key]
        public int ID { get; set; }


        public int DescriptionID { get; set; }
        public virtual LiterDescription Description { get; set; }


        [DisplayName("Статус")]
        public int Status { get; set; }

        [DisplayName("Держатель")]
        public int HolderID { get; set; }
        public virtual Holder Holder { get; set; }




        [DisplayName("Радиометка")]
        public string RFID { get; set; }


        


    }
}
