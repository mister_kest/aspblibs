﻿using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL.LibsEntities
{
    public class ReturnFact
    {
        public int ID { get; set; }
 
        public int ReturnOrderID { get; set; }
        public virtual ReturnOrder ReturnOrder { get;set;}
    }
}
