﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibsDb.LibsEntities
{
    public class LiterDescription
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Заголовок")]
        [Required(ErrorMessage = "Необходимо указать заглавие")]
        public string Title { get; set; }


        [DisplayName("Аннотация")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Необходимо ввести аннотацию")]
        public string Overview { get; set; }

        [DisplayName("Год")]
        [Required(ErrorMessage = "Необходимо указать год издания")]
        public int Year { get; set; }

        [DisplayName("Количество просмотров")]
        public int SelectCount { get; set; } = 0;


        [DisplayName("Обложка")]
        public int? ImageID { get; set; }
        public virtual ApplicationDb.Entities.Resource Image { get; set; }



        public string GetUrlLocaion()
        {
            return "/LiterDescription/" + ID;
        }

        [DisplayName("Категория")]
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }


        [NotMapped]
        public virtual List<Author> Authors { get; set; }
        public virtual List<Liter> Liters { get; set; }
    }
}