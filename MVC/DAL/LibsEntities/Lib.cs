﻿using ApplicationCommon.CommonUtils;

using ApplicationDb.Types;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{

    [DisplayName("Библиотечная система")]
    public partial class Lib: DimensionTable
    {         
        public Lib()
        {
            Holders = new List<Holder>();
            Curators = new List<Curator>();
        }

        public virtual List<Curator> Curators { get; set; }



        [Key]
        public int ID { get; set; }

        [DisplayName("Краткое наименование")]
        [Display]
        public string NameShort { get; set; }

        [DisplayName("Полное наименование")]
        public string NameFull { get; set; }

        [DisplayName("Сигла")]
        public string Sigla { get; set; }

        [DisplayName("Ссылка на сайт")]
        [Url]
        public string Url { get; set; }

     
        [DisplayName("Места выдачи")]
        public virtual List<Holder> Holders { get; set; }

        public string GetUrl()
        {
            return $"/"+Counting.GetMultiCountName(GetType().Name) +"/Details/{ID}";
        }

    }
}
