﻿using ApplicationDb.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL.LibsEntities
{
    public class Reader
    {
        public int ID { get; set; }


        [DisplayName("Пользователь")]
        public int UserID { get; set; }
        public virtual User User { get; set; }
    }
}
