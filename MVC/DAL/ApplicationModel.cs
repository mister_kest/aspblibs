﻿using ApplicationCommon.CommonUtils;
using ApplicationDb.Entities;
using ASpbLibs.Areas.ReaderFace;
using ASpbLibs.Core.Services;
using ASpbLibs.Core.Services.Core.Services;
using ASpbLibs.Core.Services.Core.Services.Libs;
using CoreApp.AppActive;
using LibsDb.LibsEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL
{
    /// <summary>
    /// Модель приложения эмитирует работу системы.
    /// </summary>
    public class ApplicationModel
    {
        


        public void Start()
        {
            Random random = new Random();
            
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if(HasReaders(db,120) == false)
                {
                    InitiallizeReaders(db, 120);
                }

                DeliveryService delivery = new DeliveryService(db);

                List<User> readers = GetReaders(db);
                int i = 0;
                foreach (User reader in readers)
                {
                    i++;
                    int j = 0;
                    List<Holder> holders = GetHolders(db);
                    foreach (Holder holder in holders)
                    {
                        j++;
                        Writing.ToConsole($"Выдача литературы {i}/{j} из {readers.Count}/{holders.Count}");
                        Liter[] liters = (from l in holder.Liters where l.Status==0 select l).ToArray();
                        if (liters.Length > 0)
                        {
                            Liter liter = liters[random.Next(0, liters.Length - 1)];
                            if( liter.Status == 0)
                            {
                                delivery.DeliveryOrderCheckout(reader, new List<Liter>() { liter }, DateTime.Now.AddDays(1));
                            }
                                
                        }
                        
                    }

                    //delivery.DeliveryOrderCheckout(reader, );
                }
            }
        }


        public List<Holder> GetHolders(ApplicationDbContext db)
        {
            return db.Holders.Include(h=>h.Liters).ToList();
        }


        /// <summary>
        /// Получение списка читателей
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        private List<User> GetReaders(ApplicationDbContext db)
        {
            return (from p in db.Users.Include(u => u.Role) where p.Role.Code == "Reader" select p).ToList();
        }


        /// <summary>
        /// Поверка регистрации заданного кол-ва читателей
        /// </summary>
        /// <param name="db"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private bool HasReaders(ApplicationDbContext db, int n)
        {
            return (from p in db.Users.Include(u => u.Role) where p.Role.Code == "Reader" select p).Count() > n;
        }


        /// <summary>
        /// Регистрация виртуальных читателей
        /// </summary>
        /// <param name="db"></param>
        /// <param name="n"></param>
        private void InitiallizeReaders(ApplicationDbContext db, int n)
        {
            AuthorizationOptions options = new AuthorizationOptions();
            RegistrationService registration = new RegistrationService(db, options, new ApplicationCommon.CommonServices.EmailService());
            RealmService realm = new RealmService(db);
            LibsRegistrationService libsRegistration = new LibsRegistrationService(null, db, realm);


            int countOfReaders = (from p in db.Users.Include(u => u.Role) where p.Role.Code == "Reader" select p).Count() - 1;
            int needRegistrate = n - countOfReaders;
            for (int i = 0; i < needRegistrate; i++)
            {
                Writing.ToConsole($"Регистрация читателя {i} из {needRegistrate}");
                registration.Signup(
                    $"reader_{i}@gmail.com",
                    "eckumoc@gmail.com",
                    "eckumoc@gmail.com",
                    "Батов",
                    "Константин",
                    "Александрович",
                    DateTime.Parse("26.08.1989"),
                    "7-904-334-11-26"
                );
            }
        }


    }
}
