﻿using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.DAL.LibsInitiallizers
{
    public class CategoriesInitiallizer
    {

        public static void Init(ApplicationDbContext db)
        {
            if (db.Categories.Count() == 0)
            {
                Category p, p1, p3, p4, root = null;
                db.Categories.Add(root = new Category(1, "Литература", null) { });
                db.Categories.Add(p1 = new Category(2, "Русская", root) { });
                db.Categories.Add(p3 = new Category(3, "Иностранная", root) { });
                db.Categories.Add(p4 = new Category(4, "Художественная", p1) { });
                db.Categories.Add(p = new Category(5, "Словари, справочники", p1) { });
                db.Categories.Add(p = new Category(6, "Учебная литература", p1) { });
                db.Categories.Add(p = new Category(7, "Поэзия 20 века", p4) { });
                db.Categories.Add(p = new Category(8, "Поэзия 19 века", p4) { });
                db.Categories.Add(p = new Category(9, "Комиксы", p3) { });
                db.SaveChanges();
            }
        }
    }
}
