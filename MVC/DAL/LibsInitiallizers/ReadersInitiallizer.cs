﻿using ASpbLibs.Core.Services;
using ASpbLibs.Core.Services.Core.Services;
using ASpbLibs.Core.Services.Core.Services.Libs;

using CoreApp.AppActive;

using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using ApplicationDb.Entities;
using ApplicationCommon.CommonUtils;
using ASpbLibsTests;

namespace ASpbLibs.DAL.LibsInitiallizers
{
    public class ReadersInitiallizer
    {
        public static void Init(ApplicationDbContext db, int n)
        {
            AuthorizationOptions options = new AuthorizationOptions();
            RegistrationService registration = new RegistrationService(db, options, new ApplicationCommon.CommonServices.EmailService());
            RealmService realm = new RealmService(db);
            LibsRegistrationService libsRegistration = new LibsRegistrationService(null, db, realm);


            int countOfReaders = (from p in db.Users.Include(u => u.Role) where p.Role.Code == "Reader" select p).Count() - 1;
            int needRegistrate = n - countOfReaders;
            for (int i = 0; i < needRegistrate; i++)
            {
                Account account = new Account()
                {
                    Email = $"reader_{i}@gmail.com",
                    Hash = Hashing.GetHashSha256("eckumoc@gmail.com")
                };
                registration.Signup(account,
                    PersonNamesProvider.GetRandomPerson()
                );
            }
        }
    }
}
