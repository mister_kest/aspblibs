﻿using ASpbLibs.Core.Services.DAL.LibsEntities;
using ASpbLibs.DAL.LibsEntities;
using LibsDb.LibsEntities;
using Microsoft.EntityFrameworkCore;

public partial class ApplicationDbContext : DbContext
{     
    public virtual DbSet<Complectator> Complectators { get; set; }
    public virtual DbSet<Curator> Curators { get; set; }
    public virtual DbSet<Reception> Receptions { get; set; }    
    public virtual DbSet<Lib> Libs { get; set; }
    public virtual DbSet<Holder> Holders { get; set; }
    public virtual DbSet<Liter> Liters { get; set; }
    public virtual DbSet<Book> Books { get; set; }
    public virtual DbSet<Journal> Journals { get; set; }
    public virtual DbSet<Category> Categories { get; set; }
    public virtual DbSet<DeliveryOrder> DeliveryOrders { get; set; }
    public virtual DbSet<DeliveryFact> DeliveryFacts { get; set; }
    public virtual DbSet<DeliveryDaily> DeliveryDaily { get; set; }
    
    public virtual DbSet<ReturnFact> ReturnFacts { get; set; }

    public virtual DbSet<ReturnOrder> ReturnOrders { get; set; }
    public virtual DbSet<Author> Authors { get; set; }
    public virtual DbSet<LiterDescription> LiterDescriptions { get; set; }
    public virtual DbSet<AuthorsPosters> AuthorsPosters { get; set; }

    


}
