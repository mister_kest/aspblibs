﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using ASpbLibs.Core.Services;
using ASpbLibs.Core.Services.CRUD;
using ASpbLibs.Core.Services.Core.Services;
using ASpbLibs.Core.Services.CoreMiddleware;
using ASpbLibs.Core.Services.Domain.Reflection;
using SpbPublicLibsCore.Domain;
using ASpbLibs.Core.Services.Core.Services.Libs;

namespace CoreApp
{
    /// <summary>
    /// Подключение служб приложения
    /// </summary>
    public static class CoreExtensions
    {

        /// <summary>
        /// Конфигурация служб приложения
        /// </summary>
        /// <param name="services">коллекция сервисов</param>
        /// <param name="configuration">конфигурации приложения</param>
        /// <returns></returns>
        public static IServiceCollection AddCoreApp( this IServiceCollection services) 
        {
            //services.AddOdbc();
            services.AddStatistics();
            services.AddGoogleServices();
            services.AddApplicationAuthorization();
            
            services.AddTransient<RealmService>();
            services.AddTransient<LibsRegistrationService>();
            services.AddTransient<AppService>();
            services.AddTransient<AppService>();
            services.AddTransient<ReflectionService>();
            services.AddTransient<CRUDProtocol>();
            services.AddTransient<NotificationsService>();
            return services;
        }



        
        /// <summary>
        /// Конфигурация конвеера обработки запросов
        /// </summary>
        /// <param name="app">конвеер обработки запросов</param>
        /// <param name="configuration">конфигурация приложения</param>
        /// <returns></returns>
        public static IApplicationBuilder UseCoreApp( this IApplicationBuilder app )
        {
            // конфигурация доступа
            app.UseMiddleware<RequestLogging>();
            app.UseMiddleware<OnlyHttpsRequests>();
            app.UseApplicationAuthorization();
            app.UseMiddleware<ReflectionComponent>();

            //app.UseOdbcForSqlServer(ApplicationDbContext.DefaultConnectionString);
            return app;
        }
    }
}
