﻿using ApplicationCommon.CommonServices;
using ASpbLibs.Areas.CuratorFace;
using ASpbLibs.Areas.SuperviserFace;
using ASpbLibs.Core.Services.Core.Services;
using ASpbLibs.Core.Services.Core.Services.Libs;
using ASpbLibs.Core.Services.CoreDomain;
using CoreApp;
using CoreApp.AppActive;
using CoreApp.AppAPI;
using LibsMVC.Areas.ComplectatorFace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core
{
    public class CoreProvider
    {
        public static APIAuthorization GetAuthorizationService()
        {
            return new AuthorizationService(null, null, null, new AuthorizationOptions(),new EmailService(),null,null);
        }
      
        public static ComplectatorService GetComplectator(ApplicationDbContext db)
        {
            RealmService realm = new RealmService(db);
            LibsRegistrationService reg = new LibsRegistrationService(null,db,realm); 

            return new ComplectatorService(db, GetAuthorizationService(), new Areas.AdminFace.NewsService(db),reg);
        }
        /* public static ComplectatorService GetComplectator()
       {
           return new ComplectatorService(new ApplicationDbContext(), GetAuthorizationService(), new Areas.AdminFace.NewsService(new ApplicationDbContext()));
       }*/

        public static CuratorService GetCurator()
        {
            return new CuratorService(null,new ApplicationDbContext(),new Areas.AdminFace.NewsService(new ApplicationDbContext()));
        }

        public static SuperviserService GetSuperviserService()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return new SuperviserService(new EmailService(), GetAuthorizationService(), db, new LibsRegistrationService(null, db, new RealmService(db)));
        }
    }
}
