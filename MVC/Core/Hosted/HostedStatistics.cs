﻿using ApplicationCommon.CommonUtils;
using ApplicationDb.Types;
using ASpbLibs.Core.Services.Core.Services.Statistics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCommon.CommonHosting
{
   
    /// <summary>
    /// Фоновая служба по истечению временного интервала формирует статистические данные
    /// по таблицам фактов.
    /// </summary>
    public class HostedStatistics : BackgroundService
    {
        private readonly ILogger<HostedStatistics> _logger;
        private readonly StatisticsService _statistics;

        public HostedStatistics(ILogger<HostedStatistics> logger, StatisticsService statistics )
        {            
            _logger = logger;
            _statistics = statistics;
            _logger.LogInformation("Create");       
        }


        /// <summary>
        /// Запуск выполнения службы
        /// </summary>
        /// <param name="stoppingToken"></param>
        /// <returns></returns>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {            
            while (!stoppingToken.IsCancellationRequested)
            {
                long timeout = (Timing.GetTodayBeginTime() + 24 * 60 * 60 * 1000) - Timing.GetTime();
                Thread.Sleep((int)timeout);
                await SaveStatistics();
                Thread.Sleep(10000);
            }
        }

        private async Task SaveStatistics()
        {
            await _statistics.SaveStatistics();
        }
    }
}
