﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.CoreMiddleware
{
    /// <summary>
    /// Компонент промежуточного ПО выполняет логирование запросов
    /// </summary>
    public class RequestLogging
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestLogging> _logger;

        public RequestLogging(ILogger<RequestLogging> logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }


        public async Task Invoke(HttpContext httpContext)
        {
            HttpRequest request = httpContext.Request;
            _logger.LogInformation($"URI: {request.Path.Value.ToString()}");
            _logger.LogInformation("Headers:");
            foreach (var header in request.Headers)
            {
                _logger.LogInformation($"\t{header.Key}={header.Value}");
            }
            _logger.LogInformation("Cookies:");
            foreach (var header in request.Cookies)
            {
                _logger.LogInformation($"\t{header.Key}={header.Value}");
            }
            await _next.Invoke(httpContext);
        }
    }
}
