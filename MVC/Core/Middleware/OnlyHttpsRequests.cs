﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.CoreMiddleware
{
    /// <summary>
    /// Компонент промежуточного ПО выполняет фильтрацию запросов протокола HTTP
    /// </summary>
    public class OnlyHttpsRequests
    {
        private readonly RequestDelegate _next;

        public OnlyHttpsRequests(RequestDelegate next)
        {
            _next = next;
        }


        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.IsHttps)
            {
                await _next.Invoke(httpContext);
            }
        }
    }
}
