﻿using ApplicationDb.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.MiddlewareComponents
{
    public class RegisterAppService
    {
        private readonly RequestDelegate _next;
        private bool _registered = false;

        public RegisterAppService(RequestDelegate next)
        {
            _next = next;
        }


        public async Task Invoke(HttpContext httpContext)
        {
            if (_registered==false)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Services.Add(new Service() { 
                        Name = httpContext.Request.Scheme+httpContext.Request.Host
                    });
                    await db.SaveChangesAsync();

                }
                _registered = true;
            }
            await _next.Invoke(httpContext);
        }
    }
}
