﻿using LibsDb.LibsEntities;
using Microsoft.Extensions.Logging;

namespace ASpbLibs.Core.Services.Core.Services.Libs
{
    public class LibsRegistrationService
    {
        private readonly ApplicationDbContext _db;
        private readonly RealmService _realm;
        private readonly ILogger<LibsRegistrationService> _logger;

        public LibsRegistrationService(ILogger<LibsRegistrationService> logger, ApplicationDbContext db, RealmService realm)
        {
            _db = db;
            _realm = realm;
            _logger = logger;
        }


 

        /// <summary>
        /// Назначение функций комплектования литературы для заданного места выдачи заданному пользователю
        /// </summary>
        /// <param name="user"> пользователь </param>
        /// <param name="holder"> место выдачи </param>
        public void SetComplectatorRole( ApplicationDb.Entities.User user, Holder holder )
        {
            if(_logger!=null) _logger.LogInformation($"SetComplectatorRole( {user.ID}, {holder.ID} )");
            ApplicationDb.Entities.User puser = _db.Users.Find(user.ID);
            puser.Role = _realm.FindRoleByCode("Complectator");
            
            _db.Complectators.Add(new Complectator() { 
                UserID = puser.ID,
                HolderID = holder.ID
            });
            _db.SaveChanges();
        }

        

        /// <summary>
        /// Назначение функций куратора ЦБС
        /// </summary>
        /// <param name="user"> пользователь </param>
        /// <param name="holder"> ЦБС </param>
        public void SetCuratorRole(ApplicationDb.Entities.User user, Lib lib)
        {
            if (_logger != null) _logger.LogInformation($"SetCuratorRole( {user.ID}, {lib.ID} )");
            ApplicationDb.Entities.User puser = _db.Users.Find(user.ID);
            puser.Role = _realm.FindRoleByCode("Curator");

            _db.Curators.Add(new Curator()
            {
                UserID = puser.ID,
                LibID = lib.ID
            });
            _db.SaveChanges();
        }


        /// <summary>
        /// Назначение функций куратора ЦБС
        /// </summary>
        /// <param name="user"> пользователь </param>
        /// <param name="holder"> ЦБС </param>
        public void SetReceptionRole(ApplicationDb.Entities.User user, Holder holder)
        {
            if (_logger != null) _logger.LogInformation($"SetCuratorRole( {user.ID}, {holder.ID} )");
            ApplicationDb.Entities.User puser = _db.Users.Find(user.ID);
            puser.Role = _realm.FindRoleByCode("Reception");

            _db.Receptions.Add(new Reception()
            {
                UserID = puser.ID,
                HolderID = holder.ID
            });
            _db.SaveChanges();
        }
    }
}
