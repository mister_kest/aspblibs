﻿using ASpbLibs.Views.Categories;

using CoreApp.AppAPI;

using LibsDb.LibsEntities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Libs
{
    public class CategoriesService
    {
        private readonly ApplicationDbContext _context;
        private readonly APIAuthorization _auth;
        private readonly ILogger<CategoriesService> _logger;

        public CategoriesService(ILogger<CategoriesService> logger, ApplicationDbContext context, APIAuthorization auth )
        {
            _context = context;
            _auth = auth;
            _logger = logger;
        }


        public List<int> GetSelectedCategoryIds()
        {
            List<int> ids = new List<int>();
            GetCategoryHier().DoRecursive((p)=>{
                if (p.Checked)
                {
                    ids.Add(p.Item.ID);
                }
            });
            return ids;
        }

        
        /// <summary>
        /// Получение иерархии
        /// </summary>
        /// <returns></returns>
        public CategoryHier GetCategoryHier()
        {            
            if (_auth.Session().ContainsKey(typeof(CategoryHier).FullName))
            {
                return (CategoryHier)_auth.Session()[typeof(CategoryHier).FullName];
            }
            List<CategoryHier> nodes = new List<CategoryHier>();
            foreach (Category record in _context.Categories.ToList())
            {
                nodes.Add(new CategoryHier(null, record));
            }
            foreach (CategoryHier pnode in nodes.ToArray())
            {
                if (pnode.GetItem().ParentID != null)
                {
                    CategoryHier parent = (from p in nodes where p.GetItem().ID == pnode.GetItem().ParentID select p).SingleOrDefault();
                    pnode.SetParent(parent);
                }
            }
            CategoryHier root = (from p in nodes where p.GetItem().ParentID == null select p).SingleOrDefault();
            _auth.Session()[root.GetType().FullName] = root;
            return root;
        }
    }
}
