﻿using ASpbLibs.Common.CommonTypes;
using ASpbLibs.Core.Services.Libs;
using ASpbLibs.Views.Shared;

using CoreApp.AppAPI;

using System;
using System.Collections.Generic;

namespace ASpbLibs.Core.Services
{
    public class PageLayout {

        public bool ShowRight { get; set; } = true;
        public bool ShowLeft { get; set; } = false;

        

        public PageLayout()
        {
            TabViewModel tab = new TabViewModel()
            {
                Title = "messages",
                Icon = "person",
                View = "_PieChart",
                Model = new ASpbLibs.Views.Shared.PieChartModel()
                {
                    Title = "Sales",
                    Width = "1000px",
                    Height = "1000px",
                    KeyValues = new Dictionary<string, float>() {
                        { "Work", 11 },
                        { "Eat", 2 },
                        { "Commute", 2 },
                        { "WatchTV", 2 },
                        { "Sleep", 7 }
                    }
                }
            
            };
            Right = new List<PartialComponent>() {
                /*new PartialComponent() {
                    View = "_Tabs",
                    Model = new TabsViewModel()
                    {
                        Active = tab,
                        Tabs = new List<TabViewModel>() {
                            tab
                        }
                    }
                }*/
             };
        }
   

        public List<PartialComponent> Left = new List<PartialComponent>();
        public List<PartialComponent> Right = new List<PartialComponent>();


        public void ToggleLeft()
        {
            ShowLeft =  ShowLeft == true? false: true;
        }

        public void ToggleRight()
        {
            ShowRight = ShowRight == true ? false : true;
        }
    }


    public class PageLayoutService: UserSessionService<PageLayout>
    {
        private readonly CategoriesService _categories;

        public PageLayoutService(
                APIAuthorization authorization, 
                CategoriesService categories ) :base(authorization)
        {
            _categories = categories;
        }

       
        public void ShowCategories()
        {
            GetModel().Left.Clear();
            GetModel().Left.Add(new PartialComponent() { 
                View = "_Categories",
                Model = _categories.GetCategoryHier()
            });
        }

        public override void InitModel(PageLayout model)
        {            
        }
    }
}
