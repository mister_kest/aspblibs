﻿using ASpbLibs.Core.Services.Core.Services.Statistics;
using ASpbLibs.Core.Services.Libs;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Core.Services
{
    public static class ServicesExtensions
    {


        public static IServiceCollection AddGoogleServices(this IServiceCollection services)
        {
            services.AddTransient<GoogleFontsProvider>();

            return services;
        }

        public static IServiceCollection AddStatistics(this IServiceCollection services)
        {
            services.AddTransient<PageLayoutService>();
            services.AddTransient<CategoriesService>();
            services.AddTransient<DeliveryStatisticsService>();
            services.AddTransient<StatisticsService>();
            return services;
        }
    }
}
