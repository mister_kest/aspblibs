﻿using ApplicationDb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Core.Services
{
    public class RealmService
    {
        private ApplicationDbContext _db;


        public RealmService(ApplicationDbContext db)
        {
            _db = db;
        }


        /// <summary>
        /// Поиск роли по коду
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Role FindRoleByCode(string code)
        {
            Role role = (from r in _db.Roles where r.Code == code select r).SingleOrDefault();
            if( role == null)
            {
                throw new ArgumentNullException("role");
            }
            return role;
        }


        /// <summary>
        /// Создание новой роли в приложении
        /// </summary>
        /// <param name="name">наименование</param>
        /// <param name="description">описание</param>
        /// <param name="code">код</param>        
        public Role CreateRole(string name, string description, string code)
        {
            Role role = null;
            _db.Roles.Add(role = new Role() { 
                Name = name,
                Description = description,
                Code = code
            });
            _db.SaveChanges();
            return role;
        }
    }
}
