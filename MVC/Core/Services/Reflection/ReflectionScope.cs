﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
 
using System.Reflection;
using System;
using ASpbLibs.Core.Services.Converter.Models;

namespace ASpbLibs.Core.Services.Domain.Reflection
{
    public class ReflectionScope: MyControllerModel
    {
        
        private readonly object _target;


        public ReflectionScope( string path, object target ): base( )
        {
            Path = path;
            Name = $"{target.GetType().Name}Scope";
            _target = target;
            Actions = new Dictionary<string, MyActionModel>();
            List<string> methodNames = new List<string>();
            foreach (MethodInfo method in _target.GetType().GetMethods())
            {
                if (method.IsPublic && method.IsStatic == false)
                {
                    Actions[method.Name] = ToModel(method);

                }                    
            }
        }



        /// <summary>
        /// Получения внутреннего обьекта
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ReflectionScope Get(string name)
        {
            ReflectionScope p = this;
            
            string[] ids = name.Split("/");
            if(ids.Length > 1)
            {
                for ( int i=0; i<(ids.Length-1); i++)
                {
                    string key = ids[i];
                    p = p.Get(key);
                    if (p == null)
                    {
                        throw new Exception("NotFound " + name);
                    }
                }
                name = ids[ids.Length - 1];
            }
             
            var field = _target.GetType().GetField(name);
            if(field != null)
            {
                return field != null ? new ReflectionScope($"{Path}/{name}", field.GetValue(_target)) : null;
            }
            else
            {
                throw new Exception("NotFound " + name);
            }            
        }


        /// <summary>
        /// Метод вызова функции
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ReflectionScope Call(string name, MyParameterModel parameters)
        {
            var field = _target.GetType().GetField(name);
            return field != null ? new ReflectionScope($"{Path}/{name}", field.GetValue(_target)) : null;
        }


        /// <summary>
        /// Получение списка параметров
        /// </summary>
        /// <returns></returns>
        public List<string> ListPropertyNames()
        {
            List<string> methodNames = new List<string>();
            foreach (var method in _target.GetType().GetFields())
            {
                methodNames.Add(method.Name);
            }
            return methodNames;
        }


        /// <summary>
        /// Получение списка методов
        /// </summary>
        /// <returns></returns>
        public List<string> ListMethodNames()
        {
            List<string> methodNames = new List<string>();
            foreach (var method in _target.GetType().GetMethods())
            {
                if (method.IsPublic && method.IsStatic == false)
                    methodNames.Add(method.Name);
            }
            return methodNames;
        }


        /// <summary>
        /// Преобразование метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private MyActionModel ToModel(MethodInfo method)
        {
            MyActionModel model = new MyActionModel();
            model.Parameters = new Dictionary<string, MyParameterModel>();
            model.Name = method.Name;
            model.Path = Path + "/" + model.Name;
            foreach(var p in method.GetParameters())
            {
                //IsTypeSupported(p.GetType());
                model.Parameters[p.Name] = new MyParameterModel()
                {
                    Name = p.Name,
                    Type = p.ParameterType.Name,
                    IsOptional = p.IsOptional
                };
            }
            return model;
        }


     

  


        
    }
}
