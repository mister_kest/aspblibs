﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace ASpbLibs.Core.Services.Domain.Reflection
{
    public interface IReflection
    {
        object Invoke(MethodInfo method, object target, JObject args);
        Dictionary<string, Object> Find(object root, string path);
    }
}
