﻿using ApplicationCommon.CommonResources;
using ApplicationCommon.CommonUtils;
using ASpbLibs.Core.Services.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SpbPublicLibsCore.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Domain.Reflection
{
    public class ReflectionComponent
    {     
        private readonly ILogger<ReflectionComponent> _logger;
        private readonly RequestDelegate _next;
        private readonly AppService _app;
        private readonly ReflectionScope _scope;

        public ReflectionComponent( ILogger<ReflectionComponent> logger, RequestDelegate next )
        {           
            _logger = logger;
            _next = next;
           
            _scope = new ReflectionScope("/$",new ResourcesService(new ApplicationDbContext()));
            _logger.LogInformation("CREATED");
        }


        public async Task Invoke( HttpContext httpContext, AppService app)
        {            
            try
            {
                string path = httpContext.Request.Path.ToString();
                if (path.StartsWith("/$") )
                {
                    RequestMessage req = GetRequestMessage(httpContext, out path);
                    var response = new ResponseMessage()
                    {
                        request = req
                    };
                    app.OnMessage(req, response);
                    //_scope.Get(path.Substring(2));
                    Writing.ToConsole(Formating.Json(response));
                }
                else
                {
                    await _next.Invoke(httpContext);
                }
                
            }
            catch(Exception ex)
            {
                await httpContext.Response.WriteAsync( ex.Message );
                await httpContext.Response.WriteAsync( ex.StackTrace );
            } 
            
        }

        private RequestMessage GetRequestMessage(HttpContext httpContext, out string path )
        {

            Dictionary<string, object> args = new Dictionary<string, object>();
            foreach(var p in httpContext.Request.Query)
            {
                args[p.Key] = p.Value;
            }

            path = httpContext.Request.Path.ToString().Substring(2);
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            while (path.IndexOf("/") != -1)
            {
                path = path.Replace("/", ".");
            }
            return new RequestMessage()
            {
                request = new RequestParams()
                {
                    pars = JObject.FromObject(args),
                    path = path
                }
            };
        }
    }
}
