﻿using CoreApp.AppAPI;
using System.Collections.Concurrent;

namespace ASpbLibs.Core.Services
{
    public class NotificationsService
    {
        private readonly APIUsers _users;
        private readonly APIAuthorization _authorization;

        public NotificationsService(APIAuthorization authorization, APIUsers users)
        {
            this._users = users;
            this._authorization = authorization;
        }


        public ConcurrentDictionary<string, object> GetSession()
        {
            if (this._authorization.IsSignin() == false)
            {
                return new ConcurrentDictionary<string, object>();
            }
            return this._users.GetSession(this._authorization.Verify().SecretKey);
        }


        /**
        * Передать сообщение пользователю, через функцию уведомлений
        */
        public void PushMessage<T>(T message)
        {
            this.GetMessageQueue<T>().Enqueue(message);
        }


        public ConcurrentQueue<T> GetMessageQueue<T>()
        {
            if (this._authorization.IsSignin() == false)
            {
                return new ConcurrentQueue<T>();
            }
            if (GetSession().ContainsKey(typeof(T).FullName) == false)
            {
                GetSession()[typeof(T).FullName] = new ConcurrentQueue<T>();
            }
            return (ConcurrentQueue<T>)
                GetSession()[typeof(T).FullName];

        }


        /// <summary>
        /// Запросить уведомления для данного пользователя
        /// </summary>
        /// <returns> Уведомления </returns>
        public T[] GetMessages<T>()
        {
            ConcurrentQueue<T> q = GetMessageQueue<T>();
            T[] messages = q.ToArray();
            q.Clear();         
            return messages;
        }

        public NotificationMessage[] GetNotifications()
        {
            return this.GetMessages<NotificationMessage>();
        }

        public void AddNotification(NotificationMessage message)
        {
            this.GetMessageQueue<NotificationMessage>().Enqueue(message);
        }


    }
}
