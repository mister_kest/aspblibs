﻿using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.APIS.OpenWeatherAPI
{
    public class OpenWeatherService
    {
        private readonly ILogger<OpenWeatherService> _logger;
        private readonly WeatherForecastAPI _apiWeather;
        private readonly OnecallForecastAPI _apiOneCall;
        
        public OpenWeatherService(
            ILogger<OpenWeatherService> logger,
            OnecallForecastAPI apiOneCall,
            WeatherForecastAPI apiWeather)
        {
            _logger = logger;
            _apiOneCall = apiOneCall;
            _apiWeather = apiWeather;
            _logger.LogInformation("Create");
        }


        public async Task<string> GetWeatherForecastByCity(string cityname)
        {
            return await _apiWeather.GetWeatherForecastByCity(cityname );
        }

        public async Task<string> GetOneCall(double lat, double lon, long time)
        {
            return await _apiOneCall.GetOneCall(lat, lon, time);
        }
    }
}
