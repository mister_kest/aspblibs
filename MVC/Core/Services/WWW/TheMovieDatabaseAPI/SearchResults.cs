﻿using System.Collections.Generic;

namespace ASpbLibs.Core.Services.APIS.TheMovieDatabaseAPI
{
    public class SearchResults
    {
        public int page { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }

        [Microsoft.AspNetCore.Mvc.BindProperty]
        public List<MovieResult> results { get; set; }        
    }
}
