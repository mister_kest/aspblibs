﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services
{
    public class GoogleFontsProvider
    {
        private static string[] FAMALY = new string[] {
            "Quicksand", "Roboto"
        };



        public string Family { get; set; } = "Roboto";
        public string GetUrl()
        {
            return $"https://fonts.googleapis.com/css2?family="+$"{Family}&display=swap";
        }
    }
}
