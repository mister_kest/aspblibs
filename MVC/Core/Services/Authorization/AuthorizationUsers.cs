﻿using ApplicationDb.Entities;

using CoreApp.AppAPI;
using Microsoft.Extensions.Logging;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Служба уровня приложения, содержит пользователей сеансов
    /// </summary>
    public class AuthorizationUsers: AuthorizationCollection<User>, APIUsers
    {
        public AuthorizationUsers( ILogger<AuthorizationUsers> logger, AuthorizationOptions options ): base( logger, options){}
    }
}
