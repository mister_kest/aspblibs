﻿using ApplicationDb.Entities;

using CoreApp.AppAPI;
using Microsoft.Extensions.Logging;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Сервис уровня приложения, содержит службы сеансов
    /// </summary>
    public class AuthorizationServices : AuthorizationCollection<Service>, APIServices
    {        
        public AuthorizationServices( ILogger<AuthorizationServices> logger, AuthorizationOptions options ): base( logger, options){}
    }
}
