﻿using ApplicationDb.Types;
using System.Collections.Concurrent;

namespace CoreApp.AppAPI
{
    public interface APIActiveCollection<T>: APICollection<T> where T : ActiveObject
    {
        void DoCheck();
        ConcurrentDictionary<string, object> GetSession(string key);
    }
}
