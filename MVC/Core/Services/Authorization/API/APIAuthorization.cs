﻿using ASpbLibs.Core.Services.CoreAPI;

using ApplicationDb.Entities;
using System;
using System.Collections.Concurrent;

namespace CoreApp.AppAPI
{
    public interface APIAuthorization: APIRegistration
    {
        void Signout();
      
        

        User Signin(string RFIDLabel);
        User Signin(string Email, string Password);        

        User Verify();
        ConcurrentDictionary<string,object> Session();

       

        bool IsSignin();
        bool InRole(string roleName);
        bool IsActivated();
        
    }
}
