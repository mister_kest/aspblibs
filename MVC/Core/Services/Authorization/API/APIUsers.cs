﻿using ApplicationDb.Entities;

using CoreApp.AppActive;

namespace CoreApp.AppAPI
{
    public interface APIUsers: APIActiveCollection<User>
    {
    }
}
