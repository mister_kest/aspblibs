﻿using ApplicationCommon.CommonUtils;

using ASpbLibs.Core.Services;
using ApplicationDb.Entities;
using CoreApp.AppActive;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ASpbLibs.Core.Services.Core.Services.Statistics;
using ApplicationCommon.CommonServices;
using ASpbLibs.Core.Services.CoreDomain;

namespace CoreApp
{
    /// <summary>
    /// Служба транспортного уровня, выполняет авторизацию, регистрацию и идентификацияю пользователей
    /// </summary>
    public class AuthorizationService: RegistrationService, APIAuthorization
    {
        
        private User _user;

        private readonly ILogger<AuthorizationService> _logger;
        private readonly APIUsers _users;        
        private readonly ApplicationDbContext _db;
 
        private readonly StatisticsService _statistics;
        private readonly AuthorizationOptions _options;
        private readonly IHttpContextAccessor _accessor;

        public AuthorizationService(
                ILogger<AuthorizationService> logger,
                IHttpContextAccessor accessor,
                StatisticsService statistics,
             
                AuthorizationOptions options,
                EmailService email,
                APIUsers users,                
                ApplicationDbContext db ): base(db, options, email) {
            _logger = logger;
            _users = users;            
            _db = db;
       
            _statistics = statistics;
            _options = options;
            _accessor = accessor;            
        }


        /// <summary>
        /// Метод выхода пользователя из сеанса
        /// </summary>
        public void Signout()
        {
            User user = Verify();
            lock (_users)
            {
                if (user != null && _users.Has(user.SecretKey))
                {
                    _users.Remove(user.SecretKey);
                    _db.SaveChanges();
                }
                _accessor.HttpContext.Response.Redirect(_options.LoginPagePath);
            }            
        }


        /// <summary>
        /// Авторизация пользователя в системе
        /// </summary>
        /// <param name="Email">электронный адрес</param>
        /// <param name="Password">пароль</param>
        public User Signin( string Email, string Password )
        {
            if( IsSignin())
            {
                Signout();
            }

            User current = (from user 
                            in _db.Users
                                    .Include(a=>a.Account)
                                    .Include(a => a.Settings)                                    
                                    .Include(a => a.Person)
                                    .Include(a => a.Role)
                                    .Include(a => a.UserGroups)
                            where user.Account.Email == Email select user).FirstOrDefault();
            
            if (current == null || GetHashSha256(Password) != current.Account.Hash)
            {
                throw new Exception("Учетные данные не зарегистрированы");
            }
            else
            {
                _user = current;
                current.Groups = (from grs
                                  in _db.Groups
                                  where (from ug in current.UserGroups select ug.GroupID).Contains(grs.ID)
                                  select grs).ToList();
                string currentKey = _users.Find(current);
                current.SecretKey = currentKey!=null? currentKey: _users.Put(current);
                current.IsActive = true;
                long timestamp = Timing.GetTodayBeginTime();
                _db.SaveChanges();
                _db.LoginFacts.Add(new LoginFact() { 
                    Created=DateTime.Now,
                    User=current,
                    Calendar=_statistics.GetTodayCalendar()
                });
                _db.SaveChanges();
                _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, current.SecretKey, new CookieOptions() { 
                    IsEssential = true,
                    Secure = true
                });
            }
            return current;
        }


        /// <summary>
        /// Идентификация пользователя в системе
        /// </summary>
        /// <returns>ссылка на обьект сеанса</returns>
        public User Verify()
        {
            if( _user != null)
            {
                return _user;
            }
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if(SecretKey == null)
            {
                return null;
            }
            else
            {
                User user = _users.Take(SecretKey);
                if (user == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                }
                return user;
            }
        }


        


        /// <summary>
        /// Получечние текущего времени в милисекундах
        /// </summary>
        /// <returns></returns>
        private long GetTimestamp()
        {
            return (long)(((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0))).TotalMilliseconds);
        }




        /// <summary>
        /// Применение функции хэширования к последовательности символов
        /// </summary>
        /// <param name="text">последовательность</param>
        /// <returns>хэш</returns>
        private string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }


        /// <summary>
        /// Проверка выполнения процедуры авторизации
        /// </summary>
        /// <returns></returns>
        public bool IsSignin()
        {
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if (SecretKey == null)
            {
                return false;
            }
            else
            {
                User user = _users.Take(SecretKey);
                if (user == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                    
                    return false;
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                    return true;
                }               
            }

        }


        /// <summary>
        /// Метод получения атрибутов сеанса
        /// </summary>
        /// <returns></returns>
        public ConcurrentDictionary<string, object> Session()
        {
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if (SecretKey == null)
            {
                return null;
            }
            else
            {
                ConcurrentDictionary<string, object> session = _users.GetSession(SecretKey);
                if (session == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                }
                return session;
            }
        }



        /// <summary>
        /// Проверка принадлежности пользователя к роли
        /// </summary>
        /// <param name="roleName">наменование роли</param>
        /// <returns></returns>
        public bool InRole(string roleName)
        {
            User user = this.Verify();
            if (user == null)
            {
                return false;
            }
            else
            {
                Role prole = user.Role;
                while( prole != null)
                {
                    if (prole.Code == roleName)
                    {
                        return true;
                    }
                    if( prole.ParentID == null)
                    {
                        break;
                    }
                    else
                    {
                        prole = _db.Roles.Find((int)prole.ParentID);
                    }
                }
                return false;
            }
        }


        /// <summary>
        /// Проверка активации учетной записи
        /// </summary>
        /// <returns></returns>
        public bool IsActivated()
        {
            User user = Verify();
            if( user == null)
            {
                return false;
            }
            else
            {
                return _db.Accounts.Find(user.Account.ID).Activated != null;
            }
        }

        public User Signin(string RFIDLabel)
        {
            if (IsSignin())
            {
                Signout();
            }

            User current = (from user
                            in _db.Users
                                .Include(a => a.Account)
                                .Include(a => a.Settings)
                                .Include(a => a.Person)
                                .Include(a => a.Role)
                                .Include(a => a.UserGroups)
                            where user.Account.RFID == RFIDLabel
                            select user).FirstOrDefault();
            if (current == null )
            {
                throw new Exception("Учетные данные не зарегистрированы");
            }
            else
            {
                _user = current;
                current.Groups = (from grs
                                    in _db.Groups
                                  where (from ug in current.UserGroups select ug.GroupID).Contains(grs.ID)
                                  select grs).ToList();
                string currentKey = _users.Find(current);
                current.SecretKey = currentKey != null ? currentKey : _users.Put(current);
                current.IsActive = true;
                long timestamp = Timing.GetTodayBeginTime();
                _db.SaveChanges();
                _db.LoginFacts.Add(new LoginFact()
                {
                    Created = DateTime.Now,
                    User = current,
                    Calendar = (from cal in _db.Calendars where cal.Timestamp == timestamp select cal).First()
                });
                _db.SaveChanges();
                _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, current.SecretKey, new CookieOptions()
                {
                    IsEssential = true,
                    Secure = true
                });
                return current;
            }
            
        }
    }
}
