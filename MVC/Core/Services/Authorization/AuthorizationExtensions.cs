﻿using CoreApp.AppActive;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using ApplicationCommon.CommonServices;
using LibsMVC.Areas;
using ASpbLibs.Areas;

namespace CoreApp
{
    /// <summary>
    /// Подключение служб приложения
    /// </summary>
    public static class AuthorizationExtensions
    {
        public static IServiceCollection AddCoreSession(this IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(60);
                options.Cookie.HttpOnly = false;
                options.Cookie.IsEssential = true;
            });
            return services;
        }

        /// <summary>
        /// Конфигурация служб приложения
        /// </summary>
        /// <param name="services">коллекция сервисов</param>
        /// <param name="configuration">конфигурации приложения</param>
        /// <returns></returns>
        public static IServiceCollection AddApplicationAuthorization( this IServiceCollection services) 
        {
            
            Console.WriteLine($"CoreAppExtensions.AddAddCoreApp()");
            Debug.WriteLine($"CoreAppExtensions.AddAddCoreApp()");
          
            services.AddHttpContextAccessor();
            services.AddTransient<EmailService>();
            services.AddSingleton<AuthorizationOptions>();
            services.AddSingleton<APIServices, AuthorizationServices>();
            services.AddSingleton<APIUsers, AuthorizationUsers>();
                                  
            services.AddTransient<APIAuthorization, AuthorizationService>();      
            services.AddDbContext<ApplicationDbContext>(options => {
                options.UseSqlServer(ApplicationDbContext.DefaultConnectionString);
            });
            return services;
        }



        
        /// <summary>
        /// Конфигурация конвеера обработки запросов
        /// </summary>
        /// <param name="app">конвеер обработки запросов</param>
        /// <param name="configuration">конфигурация приложения</param>
        /// <returns></returns>
        public static IApplicationBuilder UseApplicationAuthorization( this IApplicationBuilder app )
        {
            // конфигурация доступа       
            app.UseCanActivateComponent();     
            return app;
        }

        public static IApplicationBuilder UseNetworkComponent(this IApplicationBuilder app)
        {
            // конфигурация доступа
            app.UseSession();                       
            return app;
        }

        
    }
}
