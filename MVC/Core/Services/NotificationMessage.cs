﻿using ApplicationDb.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASpbLibs.Core.Services
{
    public class NotificationMessage: News
    {
        /// <summary>
        /// "Error" - если сообщение содержит информацию об ошибке
        /// </summary>
        public string Type { get; set; }
    }
}
