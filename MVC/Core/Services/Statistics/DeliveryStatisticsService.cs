﻿using LibsDb.LibsEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Core.Services.Statistics
{
    public class DeliveryStatisticsService
    {
        private readonly ApplicationDbContext _context;

        public DeliveryStatisticsService(ApplicationDbContext context)
        {
            _context = context;
        }

        
        public int IncSelectCount(LiterDescription description)
        {
            try
            {
                LiterDescription fromDb = _context.LiterDescriptions.Find(description.ID);
                return description.SelectCount = fromDb.SelectCount = description.SelectCount + 1;                
            }
            finally
            {
                _context.SaveChanges();
            }
        }
        /// <summary>
        /// Получение количества выполненых операций выдачи для заданной литературы
        /// </summary>
        /// <param name="liter"></param>
        /// <returns></returns>
        public int GetDeliveriesCount(Liter liter)
        {
            return GetDeliveriesCount(liter.ID);
        }

        public int GetDeliveriesCount(int literDescriptionId)
        {
            return  (from p in _context.DeliveryFacts.Include(f => f.Literature) where p.Literature.DescriptionID == literDescriptionId select p).Count();
            
            //return (from p in _context.DeliveryFacts where p.LiteratureID==literId select p).Count();
        }
    }
}
