﻿using ApplicationCommon.CommonUtils;
using ApplicationDb.Entities;
using ApplicationDb.Types;
using ASpbLibs.Core.Services.DAL.LibsEntities;
using LibsDb.LibsEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.Core.Services.Statistics
{
    public class StatisticsService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<StatisticsService> _logger;

        public StatisticsService(ILogger<StatisticsService> logger, ApplicationDbContext db)
        {
            this._context = db;
            _logger = logger;
        }


        public async Task SaveStatisticsOLAP()
        {
            foreach (Type entityType in GetFactsTables(_context))
            {

                IEnumerable<INavigation> navs = GetNavigationProperties(entityType);
                foreach (INavigation nav in navs)
                {
                    _logger.LogInformation(entityType.FullName + " " + nav.ForeignKey);
                }
                //await SaveStatistics(_context, entityType);
            }

            await _context.SaveChangesAsync();
        }

        public IEnumerable<INavigation> GetNavigationProperties(Type t)
        {
            IEntityType entity = (from navs in _context.Model.GetEntityTypes() where navs.Name == t.FullName select navs).SingleOrDefault();
            return entity.GetNavigations();
        }


        /// <summary>
        /// Формирование и сохранение статистики
        /// </summary>
        public async Task SaveStatistics( )
        {
            await SaveStatisticsDelivery(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        }
        public async Task SaveStatistics( int day, int month, int year )
        {
            await SaveStatisticsDelivery(day,month,year);
        }

        private async Task SaveStatisticsDelivery(int day, int month, int year)
        {

            Calendar calendar = (from c in _context.Calendars where c.Timestamp == Timing.GetTime(new DateTime(year, month, day)) select c).SingleOrDefault();
            foreach( Holder h in _context.Holders.ToList())
            {
                int holderId = h.ID;
                int deliveryCount = (from f in _context.DeliveryFacts.Include(f => f.Literature) where f.CalendarID == calendar.ID && f.Literature.HolderID == holderId select f).Count();
                _context.DeliveryDaily.Add(new DeliveryDaily() { 
                    HolderID = holderId,
                    BeginDate = new DateTime(year, month, day),
                    EndDate = new DateTime(year, month, day).AddDays(1),
                    Value = deliveryCount
                });
                await _context.SaveChangesAsync();
            }
        }


        /// <summary>
        /// Получение сущностей типа таблица фактов
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        private HashSet<Type> GetFactsTables(DbContext db)
        {
            HashSet<Type> entities = new HashSet<Type>();
            foreach (Type dbsetType in GetEntitiesTypes(db))
            {
                Type entityType = dbsetType.GenericTypeArguments[0];
                Type p = entityType;
                while (p != typeof(Object) && p != null)
                {
                    if (p.Equals(typeof(FactsTable)))
                    {
                        entities.Add(entityType);
                        break;
                    }
                    p = p.BaseType;
                }
            }
            return entities;
        }

        public Calendar GetTodayCalendar()
        {
            Calendar c = (from cal in _context.Calendars where cal.Timestamp == Timing.GetTodayBeginTime() select cal).SingleOrDefault();
            DateTime p = DateTime.Now;
            if(c == null)
            {
                _context.Calendars.Add(c = new Calendar()
                {
                    Day = p.Day,
                    Quarter = p.Month < 4 ? 1 : p.Month < 7 ? 2 : p.Month < 10 ? 3 : 4,
                    Month = p.Month,
                    Week = 1,
                    Year = p.Year,
                    Timestamp = (long)((new DateTime(p.Year, p.Month, p.Day) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds)
                });
                _context.SaveChanges();
            }
            return c;
        }



        /// <summary>
        /// Нехороший способ извеления наименований сущностей
        /// </summary>
        /// <param name="subject"> контекст данных </param>
        /// <returns> множество наименований сущностей </returns>
        private HashSet<Type> GetEntitiesTypes(DbContext subject)
        {
            Type type = subject.GetType();
            HashSet<Type> entities = new HashSet<Type>();
            foreach (MethodInfo info in type.GetMethods())
            {
                if (info.Name.StartsWith("get_") == true && info.ReturnType.Name.StartsWith("DbSet"))
                {
                    if (info.Name.IndexOf("MigrationHistory") == -1)
                    {
                        entities.Add(info.ReturnType);
                    }
                }
            }
            return entities;
        }
    }
}
