﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

using CoreApp.AppAPI;
using ASpbLibs.Core.Services.Messaging;
using ASpbLibs.Core.Services.Domain.Reflection;

namespace SpbPublicLibsCore.Domain
{
    public class AppService 
    {
        public readonly ILogger<AppService> _logger;
        public readonly APIAuthorization _authentication;
 

        public readonly ConcurrentDictionary<string, object> publicModel;

        public AppService(             
            
            ILogger<AppService> logger,
            APIAuthorization authentication  )
        {     
            this._logger = logger;
            this._authentication = authentication; 
            this.publicModel = new ConcurrentDictionary<string, object>() {};
            this.publicModel["authentication"] = this._authentication;
                      
            this.publicModel["authentication"] = authentication;
        }

        /*/// <summary>
        /// Событие быстрой сборки приложения
        /// </summary>
        public void OnGenerate()
        {
            TypeScriptGenerator generator = 
                new TypeScriptGenerator("ProfileContext","profile-context","ClientApp/src/app/app-core/core-datamodel", "sqlserver", "root", "sgdf");
            generator.createDataContext();
        }*/


        public object Options()
        {
            return new ReflectionService().GetSkeleton(this.publicModel);
        }

        //https://localhost:44392/$/authentication/IsSignin
        /// <summary>
        /// Метод обработки сообщения приложением
        /// </summary>
        /// <param name="request">запрос</param>
        /// <param name="response">ответ</param>
        public void OnMessage(RequestMessage request, ResponseMessage response)
        {
            try
            {
                if(request==null || request.request == null || request.request.path == null)
                {
                    throw new Exception("Request message invalid");
                }
                this._logger.LogInformation(request.request.path);
                this._logger.LogInformation(JObject.FromObject(request.request.pars).ToString());
                Dictionary<string, object> metadata = null;
                try
                {
                    object subject = this.publicModel;
                    metadata =
                        new ReflectionService().Find(subject, request.request.path);
                }
                catch (Exception ex)
                {
                    throw new Exception("Function " + request.request.path + " not founded. " + ex.StackTrace, ex);
                }


                try
                {
                    object result =

                    response.data =
                        new ReflectionService().Invoke((MethodInfo)metadata["method"], metadata["target"], request.request.pars);

                    response.status = "success";

                }
                catch (Exception ex)
                {
                    throw new Exception("path: " + request.request.path + "\n error execute function: " + ex.Message, ex);
                }


                
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Debug.WriteLine(ex);
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    Console.WriteLine(ex.Message);
                }
                throw;
            }
        }

        

    }
}
