﻿using Newtonsoft.Json.Linq;

using System.Collections.Generic;

namespace ASpbLibs.Core.Services.Messaging
{
    public class RequestParams
    {
        public string path { get; set; }
        public JObject pars { get; set; }
        public byte[] blob { get; set; }

        public List<string> list()
        {
            return new List<string>(path.Split("/"));
        }
    }
}
