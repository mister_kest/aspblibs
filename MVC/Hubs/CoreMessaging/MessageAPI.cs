﻿using ASpbLibs.Core.Services.Messaging;

namespace ASpbLibs.Core.Services.Messaging
{
    /// <summary>
    /// Интерфейс служб, сервисов, клиентов и посредников.
    /// </summary>
    public interface MessageAPI
    {
        void OnMessage(RequestMessage request, ResponseMessage response);
    }
}
