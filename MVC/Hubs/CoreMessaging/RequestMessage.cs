﻿
using Newtonsoft.Json;

using System.Collections.Generic;

namespace ASpbLibs.Core.Services.Messaging
{
    public class RequestMessage 
    {
        public string token { get; set; }       
        public string mid { get; set; }       
        public long created { get; set; }
        public RequestParams request { get; set; }

        public string method { get; set; }

        public Dictionary<string,object> getParametersMap()
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(this.request.pars.ToString());
        }
    }
}
