﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.CommonCore.CoreMessaging.Model
{
    class ClientConnectionInfo
    { 
        public string IP { get; set; }
        public int Port { get; set; }


        public ClientConnectionInfo( HttpContext httpContext )
        {
            IP = httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            Port = httpContext.Connection.RemotePort;
        }
                
    }
}
