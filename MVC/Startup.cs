using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ApplicationCommon.CommonUtils;
using ASpbLibs.Core.Services;
using ApplicationDb;

using CoreApp;
using LibsMVC.Areas;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SpbPublicLibsCore.Domain;
using ASpbLibs.Areas;

namespace ASpbLibs.Core.Services
{
    public class Startup
    {
        public IConfiguration Configuration { get; }


        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Writing.ToConsole("Конфигурация при старте хоста: ", configuration);
            Writing.ToConsole(
                configuration.GetReloadToken().ActiveChangeCallbacks==true?
                "Изменения конфигурации отслеживаются": "Изменения конфигурации не отслеживаются");
            Configuration = configuration;            
        }


        /// <summary>
        /// Подключение сервисов
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();//.AddRazorRuntimeCompilation( ); 
            services.AddCoreApp();
            services.AddSignalR();
            services.AddTransient<AppService>();
            services.AddDbContext<ApplicationDbContext>();
            services.AddAreasServices();


            /*services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });*/

           
        }


        /// <summary>
        /// Конфигурация конвеера
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            Writing.ToConsole("Название приложения: " + env.ApplicationName);
            Writing.ToConsole("Название среды выполнения: " + env.EnvironmentName);
            Writing.ToConsole("Директория веб-хоста: " + env.WebRootPath.ToString());
            Writing.ToConsole("Директория контента: " + env.ContentRootPath.ToString());
            Writing.ToConsole("Изменения файлов: " + 
                ((env.ContentRootFileProvider.Watch("*.*").ActiveChangeCallbacks==true)? "отслживают": "не отслеживаются"));

            app.UseCoreApp();
            app.UseAreasMiddleware();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            
            app.UseRouting();
            //app.UseSession();
            //app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<AppHub>("/Message");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{area=}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
           
        }
    }
}
