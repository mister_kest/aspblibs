﻿using ApplicationDb.Entities;
using CoreApp.AppAPI;
using LibsDb.LibsEntities;

using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using ASpbLibs.Areas.AdminFace;

namespace ASpbLibs.Areas.CuratorFace
{
    public class CuratorService
    {
        private readonly APIAuthorization _authorization;
        private readonly NewsService _news;
        private readonly ApplicationDbContext _context;

        public CuratorService( APIAuthorization authorization, ApplicationDbContext context, NewsService news )
        {
            _authorization = authorization;
            _news = news;
            _context = context;
        }


        /// <summary>
        /// Получение ссылки на поведомственную ЦБС
        /// </summary>
        /// <returns></returns>
        public Lib GetLib()
        {
            ApplicationDb.Entities.User user = _authorization.Verify();
            if( user != null)
            {
                Lib l = (from p in _context.Curators.Include(rec => rec.Lib) where p.UserID == user.ID select p.Lib).SingleOrDefault();
                l.Holders = _context.Holders.Where(h => h.LibID == l.ID).ToList();
                return l;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void AddHolder(Lib lib, Holder holder)
        {
            holder.LibID = lib.ID;
            _context.Holders.Add(holder);
            _context.SaveChanges();
            _news.AddMessageAboutNewHolder(lib,holder);
        }
    }
}
