﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.CuratorFace
{
   
    public static class CuratorExtensions
    {
        public static IServiceCollection AddCuratorServices(this IServiceCollection services)
        {
            Writing.ToConsole("Регистрация сервисов куратора");
            services.AddTransient<CuratorService>();
            return services;
        }
        public static IApplicationBuilder UseCuratorMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
