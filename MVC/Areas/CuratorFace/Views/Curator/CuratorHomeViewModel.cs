﻿using ASpbLibs.Views.Shared;

using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.CuratorFace.Views.Curator
{
    public class CuratorHomeViewModel
    {
        public PieChartModel Pie { get; set; }

        public Lib Lib { get; set; }
    }
}
