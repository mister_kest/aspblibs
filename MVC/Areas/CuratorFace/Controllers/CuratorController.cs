﻿using ASpbLibs.Areas.CuratorFace.Views.Curator;
using ASpbLibs.Views.Shared;
using LibsDb.LibsEntities;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.CuratorFace.Controllers
{
    [Area("CuratorFace")]
    public class CuratorController: Controller
    {
        private readonly CuratorService _curator;

        public CuratorController(CuratorService curator)
        {
            _curator = curator;
        }

        public IActionResult CuratorHome()
        {          
            return View(new CuratorHomeViewModel() {
                Lib = _curator.GetLib(),
                Pie = new PieChartModel()
            });
        }
    }
}
