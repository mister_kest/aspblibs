﻿using ApplicationCommon.CommonUtils;
using ASpbLibs.Core.Services.CoreDomain;
using ASpbLibs.Areas.AdminFace;
using ASpbLibs.Areas.CuratorFace;
using ASpbLibs.Areas.ReaderFace;
using ASpbLibs.Areas.ReceptionFace;
using ASpbLibs.Areas.SuperviserFace;
using ASpbLibs.Areas.UserFace;
using LibsMVC.Areas.ComplectatorFace;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ASpbLibs.Areas
{
    /// <summary>
    /// Подключение сервисов обслуживающих пользователей одной роли
    /// </summary>
    public static class AreasExtensions
    {

        /// <summary>
        /// Подключение сервисов обслуживающих пользователей одной роли
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddAreasServices(this IServiceCollection services)
        {
            Writing.ToConsole("Подключение сервисов, обслуживающих области приложения");
            services.AddAdminServices();
            services.AddComplectatorServices();
            services.AddCuratorServices();
            services.AddReaderServices();
            services.AddReceptionServices();
            services.AddSuperviserServices();
            services.AddUserServices();
            return services;
        }
 
        public static IApplicationBuilder UseAreasMiddleware(this IApplicationBuilder app )
        {
            Writing.ToConsole($"Подключение промежуточного ПО,  обслуживающего области приложения.");
            app.UseCanActivateComponent();
            return app;
        }

        /// <summary>
        /// Подключение компонента промежуточного ПО, отвечающим за управление доступом.
        /// </summary>
        /// <param name="configuration"> Функция корректировки параметров </param>
        /// <returns></returns>
        public static IApplicationBuilder UseCanActivateComponent(this IApplicationBuilder app, Action<CanActivateOptions>? configuration = null)
        {

            Writing.ToConsole($"Подключение компонента промежуточного ПО, отвечающим за управление доступом.");
            CanActivateOptions options = new CanActivateOptions();
            if (configuration != null)
            {
                configuration(options);
            }
            app.UseMiddleware<CanActivateComponent>(options);
            return app;
        }

    }
}
