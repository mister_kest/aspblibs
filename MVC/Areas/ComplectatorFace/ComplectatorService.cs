﻿using ApplicationDb.Entities;
using ASpbLibs.Areas.AdminFace;
using ASpbLibs.Core.Services.Core.Services;
using ASpbLibs.Core.Services.Core.Services.Libs;
using CoreApp.AppAPI;
using LibsDb;
using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibsMVC.Areas.ComplectatorFace
{
    public class ComplectatorService
    {
        private readonly APIAuthorization _authorization;
        private readonly LibsRegistrationService _registration;
        private readonly ApplicationDbContext _db;
        private readonly NewsService _news;

        public ComplectatorService(ApplicationDbContext db, APIAuthorization authorization, NewsService news, LibsRegistrationService registration)
        {
            _authorization = authorization;
            _registration = registration;
            _db = db;
            _news = news;
        }

        public Holder GetHolder()
        {
            return _db.Holders.Find((from p in _db.Complectators where p.UserID == _authorization.Verify().ID select p.Holder.ID).SingleOrDefault());
        }



        public void AddLiter( Holder holder, Liter liter)
        {
            liter.HolderID = holder.ID;
            _db.Liters.Add(liter);
            _db.SaveChanges();
            _news.AddMessageAboutNewLiter(DateTime.Now, holder, liter);
        }


        /// <summary>
        /// Выполнение доставки заказа на пункт выдачи
        /// </summary>
        /// <param name="order"> заказ </param>
        /// <param name="user"> пользователь </param>
        public void DeliveryComplete(DeliveryOrder order, User user)
        {
            order.WaitingConsumer = true;
        }
    }
}
