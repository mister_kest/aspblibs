﻿using ASpbLibs.Application.Abstraction.TabsView;
using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.ComplectatorFace.Views.Complectator
{
    public class ComplectatorViewModel: TabsViewModel
    {
        public Holder Holder { get; set; }
        public ComplectatorViewModel()
        {
            this.Nav["История книговыдачи"] = new DeliveryHistoryViewModel();
            this.Nav["Статистика"] = new DeliveryStatsViewModel();
            this.Nav["Литература"] = new LitersViewModel();
            this.Active = "Литература";
        }
    }
}
