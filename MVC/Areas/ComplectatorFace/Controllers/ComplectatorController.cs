﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibsDb;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using LibsDb.LibsEntities;
using LibsMVC.Areas.ComplectatorFace;
using ASpbLibs.Areas.ComplectatorFace.Views.Complectator;
using ASpbLibs.Application.Abstraction;
using CoreApp.AppAPI;

namespace ASpbLibs.Areas.ComplectatorFace.Controllers
{
    [Area("ComplectatorFace")]
    public class ComplectatorController : UserSessionController<ComplectatorViewModel>
    {
        private readonly ComplectatorService _service;
        private readonly ApplicationDbContext _context;

        public ComplectatorController(ApplicationDbContext context, ComplectatorService service, APIAuthorization authorization) : base(authorization)
        {
            _service = service;
            _context = context;
        }

        public IActionResult ComplectatorHome()
        {
            ComplectatorViewModel model = GetModel();
            return View(model);
        }

        public override void InitModel(ComplectatorViewModel model)
        {
            Holder holder = _service.GetHolder();
            int id = holder.ID;
            Holder record = (from p in _context.Holders.Include(h => h.Liters) where p.ID == _service.GetHolder().ID select p).SingleOrDefault();
            model.Holder = record;
        }

        public IActionResult NavigateTo( string key )
        {
            ComplectatorViewModel model = GetModel();
            model.Active = key;
            return View("ComplectatorHome",model);
        }
    }
}
