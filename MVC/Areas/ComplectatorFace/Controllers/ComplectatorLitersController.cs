﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb.LibsEntities;
using LibsMVC.Areas.ComplectatorFace;
using CoreApp.AppAPI;
using ASpbLibs.Core.Services.CRUD;
using ASpbLibs.Application.Abstraction.BookView;
using ASpbLibs.Core.Services;
using ASpbLibs.Application.Abstraction;
using System;
using ASpbLibs.Application.Abstraction.SearchView;

namespace ASpbLibs.Areas.ComplectatorFace.Controllers
{
    [Area("ComplectatorFace")]
    public class ComplectatorLitersController : BookViewController<Liter>
    {
        private readonly ApplicationDbContext _context;
        private readonly ComplectatorService _service;
        private readonly NotificationsService _notifications;

        public ComplectatorLitersController(
             
                APIAuthorization authorization, 
                NotificationsService notifications,
                ApplicationDbContext context, 
                ComplectatorService service) : base( authorization)
        {
            _context = context;                    
            _service = service;
            _notifications = notifications;           
        }

  
         

         


        // GET: HolderFace/Liters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                //.Include(l => l.Image)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // GET: HolderFace/Liters/Create
        public IActionResult Create()
        {
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID");
            ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime");
            return View(new Liter());
        }

        // POST: HolderFace/Liters/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Overview,Year,Status,HolderID,ImageID,RFID")] Liter liter)
        {
            liter.Holder = _context.Holders.Find(_service.GetHolder().ID);
            if (ModelState.IsValid)
            {
                _context.Add(liter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            //ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", liter.ImageID);
            return View(liter);
        }

        // GET: HolderFace/Liters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters.FindAsync(id);
            if (liter == null)
            {
                return NotFound();
            }
            
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            //ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", liter.ImageID);
            return View(liter);
        }

        // POST: HolderFace/Liters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Overview,Year,Status,HolderID,ImageID,RFID")] Liter liter)
        {
            if (id != liter.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(liter);
                    await _context.SaveChangesAsync();
                    _notifications.AddNotification(new NotificationMessage()
                    {
                        Title = "Сведения о литературе успешно сохранены"
                    });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiterExists(liter.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            //ViewData["ImageID"] = new SelectList(_context.Resources, "ID", "Mime", liter.ImageID);
            return View(liter);
        }



        // GET: HolderFace/Liters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                //.Include(l => l.Image)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // POST: HolderFace/Liters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var liter = await _context.Liters.FindAsync(id);
            _context.Liters.Remove(liter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiterExists(int id)
        {
            return _context.Liters.Any(e => e.ID == id);
        }

        public override IQueryable<Liter> GetAll()
        {
            return _context.Liters.Where(l => l.HolderID == _service.GetHolder().ID);
        }

        public override string GetViewName()
        {
            return "Index";
        }
    }
}
