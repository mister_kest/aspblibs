﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace LibsMVC.Areas.ComplectatorFace
{



    /// <summary>
    /// Подключение служб приложения
    /// </summary>
    public static class ComplectatorExtensions
    {
       
 
        public static IServiceCollection AddComplectatorServices( this IServiceCollection services) 
        {
            Writing.ToConsole("Регистрация сервисов комплектатора");
            services.AddTransient<ComplectatorService>();
            return services;
        }

        public static IApplicationBuilder UseComplectatorMiddleware(this IApplicationBuilder app)
        {
            return app;
        }

    }
}
