﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationDb.Entities;
using ApplicationMVC.Views.Account;
using Microsoft.Extensions.Logging;
using CoreApp.AppAPI;
using CoreApp.AppActive;
using ASpbLibs.Core.Services;
using ApplicationCommon.CommonServices;
 
using ASpbLibs.Application.Abstraction.SearchView;

namespace ASpbLibs.Areas.ReceptionFace.Controllers
{
    [Area("ReceptionFace")]
    public class PersonsController : SearchViewController<Person>
    {
        private ApplicationDbContext _context;
        private readonly EmailService _email;
        private readonly AuthorizationOptions _options;
        
        private readonly NotificationsService _user;

        public PersonsController(
                EmailService email,
                ApplicationDbContext context,
                AuthorizationOptions options,          
                APIAuthorization authorization,
                NotificationsService user):base(authorization)
        {        
            _user = user;
            _context = context;
            _email = email;
            _options = options;
        }



        

        // GET: ReceptionFace/People/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            {            if (id == null)

                return NotFound();
            }


            var person = await (from p in _context.Users.Include(r => r.Person) where p.PersonID == (int)id select p).SingleOrDefaultAsync();
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        // GET: ReceptionFace/People/Details/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _context = null;
            var person = await _context.Persons
                .FirstOrDefaultAsync(m => m.ID == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        /// <summary>
        /// Переход на страницу регистрации пользователя
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View(new RegistrationModel()
            {
               
            });
        }

        /// <summary>
        /// Запрос регитсрации учетной записи пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([Bind("Email,Password,Confirmation,FirstName,LastName,SurName,Birthday,Tel")] RegistrationModel model)
        {
           

            if (model.Birthday.Year < 1920)
            {
                ModelState.AddModelError("Birthday", "Дата рождения указана неверно");
                return View(model);
            }

            if (model.Confirmation != model.Password)
            {
                ModelState.AddModelError("Confirmation", "Подтверждение пароля отчается от самого пароля");
                return View(model);
            }

            if (_authorization.HasUserWithEmail(model.Email) == true)
            {
                ModelState.AddModelError("Email", "Данный электронный адрес уже зарегистрирован");
                return View(model);
            }

            if (_authorization.HasUserWithTel(model.Tel) == true)
            {
                ModelState.AddModelError("Tel", "Данный номер телефона уже зарегистрирован");
                return View(model);
            }

            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    switch (state.Key)
                    {
                        case "SurName": model.SurNameValidationState = GetValidationState(state.Key); break;
                        case "Last": model.LastNameValidationState = GetValidationState(state.Key); break;
                        case "First": model.FirstNameValidationState = GetValidationState(state.Key); break;
                        case "Birthday": model.BirthdayValidationState = GetValidationState(state.Key); break;
                        case "Email": model.EmailValidationState = GetValidationState(state.Key); break;
                        case "Password": model.PasswordValidationState = GetValidationState(state.Key); break;
                        case "Confirmation": model.ConfirmationValidationState = GetValidationState(state.Key); break;
                        case "Tel": model.TelValidationState = GetValidationState(state.Key); break;

                    }
                }
                return View(model);
            }

            try
            {


                string EncodedResponse = Request.Form["g-Recaptcha-Response"];
                bool IsCaptchaValid = (ReCaptchaClass.Validate(EncodedResponse) == "true") ? true : false;
                if (IsCaptchaValid)
                {
                    _authorization.Signup(model.Email, model.Password, model.Confirmation, model.FirstName, model.LastName, model.SurName, model.Birthday, model.Tel);

                    string baseUrl = GetBaseUrl();
                    string activationKey = _authorization.GenerateActivationKey(_options.KeyLength);
                    _authorization.GetUserByEmail(model.Email).Account.ActivationKey = activationKey;
                    _context.SaveChanges();
                    _email.SendEmail(model.Email,
                        "Активация учетной записи",
                        $"Для активации учетной записи перейдите по ссылки: <a href=\"{baseUrl}/Account/Activate/{activationKey}\">{baseUrl}/Account/Activate/{activationKey}</a>"
                    );
                    return View($"RegistrationComplete", model);
                }
                else
                {
                    TempData["recaptcha"] = "Подтвердите что Вы не робот";
                    return View(model);
                }
            }
            catch (Exception ex)
            {

                model.ErrorMessage = ex.Message;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    model.ErrorMessage += " " + ex.Message;
                }

                return View(model);
            }
        }

        /// <summary>
        /// Получение URL-адреса приложения
        /// </summary>
        /// <returns></returns>
        private string GetBaseUrl()
        {
            string url = _options.ApplicationUrl + HttpContext.Request.Path.ToString();
            int commaIndex = url.IndexOf(":");
            int serverNameBeginIndex = commaIndex + 3;
            int increase = url.Substring(serverNameBeginIndex).IndexOf("/");
            return url.Substring(0, serverNameBeginIndex + increase);
        }


        /// <summary>
        /// Получение статуся проверки свойства модели
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public string GetValidationState(string property)
        {
            string state = ModelState[property] == null ? "valid" :
                ModelState[property].ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid == true ? "valid" : "invalid";
            return state;
        }

        // POST: ReceptionFace/People/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,SurName,FirstName,LastName,Birthday,Tel")] Person person)
        {
            if (id != person.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(person);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonExists(person.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(person);
        }

        // GET: ReceptionFace/People/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Persons
                .FirstOrDefaultAsync(m => m.ID == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        // POST: ReceptionFace/People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var person = await _context.Persons.FindAsync(id);
            _context.Persons.Remove(person);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonExists(int id)
        {
            return _context.Persons.Any(e => e.ID == id);
        }

        public override IQueryable<Person> GetAll(SearchViewModel<Person> model)
        {
           
            if( model.SearchQuery == null)
            {
                model.SearchQuery = "";
            }

            string query = model.SearchQuery;
            return _context.Persons.Where(p => String.IsNullOrEmpty(model.SearchQuery)   || p.SurName.ToLower().IndexOf(query.ToLower()) != -1 ||
                        p.LastName.ToLower().IndexOf(query.ToLower()) != -1 ||
                         p.FirstName.ToLower().IndexOf(query.ToLower()) != -1);
        
        }

        public override string GetViewName()
        {
            return "Search";
        }

        public override List<string> GetKeywords(string query)
        {            
            HashSet<string> keywords = new HashSet<string>();
            List<string> titles =
                (from p in _context.Persons
                 where p.SurName.ToLower().IndexOf(query.ToLower()) != -1 ||
                        p.LastName.ToLower().IndexOf(query.ToLower()) != -1 ||
                         p.FirstName.ToLower().IndexOf(query.ToLower()) != -1 
                 select p.SurName + " "+ p.LastName + " " +p.FirstName).Take(GetModel().PageSize).ToList();

            foreach (string title in titles)
            {
                foreach (string word in new List<string>(title.ToLower().Split(' ', '.', ',', '!')))
                {
                    keywords.Add(word);
                }
            }
            return new List<string>(keywords);
        }
    }
}
