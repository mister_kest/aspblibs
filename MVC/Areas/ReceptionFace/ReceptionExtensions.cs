﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.ReceptionFace
{

    public static class ReceptionExtensions
    {
        public static IServiceCollection AddReceptionServices(this IServiceCollection services)
        {
            Writing.ToConsole("Регистрация сервисов ресепшена");
            services.AddTransient<ReceptionService>();
            return services;
        }
        public static IApplicationBuilder UseReceptionMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
