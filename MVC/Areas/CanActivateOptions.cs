﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASpbLibs.Core.Services.CoreDomain
{
    /// <summary>
    /// Параметры фильтрации запросов
    /// </summary>
    public class CanActivateOptions
    {
        /// <summary>
        /// Страница авторизации
        /// </summary>
        public string LoginPagePath { get; set; } = "/Account/Login";

        /// <summary>
        /// Страница активации учетной записи
        /// </summary>
        public string ActivationRequirePath { get; set; } = "/Account/ActivationRequire";


        /// <summary>
        /// Маршруты только для авторизованных пользователей
        /// </summary> 
        public Dictionary<string, List<string>> RoleValidationRoutes
            = new Dictionary<string, List<string>>() {
                { "User", new List<string>{ "/UserFace" } },
                { "Admin", new List<string>{ "/AdminFace" } },
                { "Reader", new List<string>{ "/ReaderFace" } },
                { "Complectator", new List<string>{ "/ComplectatorFace" } },
                { "Curator", new List<string>{ "/CuratorFace" } },
                { "Superviser", new List<string>{ "/SuperviserFace" } },
                { "Holder", new List<string>{ "/HolderFace" } },
        };

    }
}
