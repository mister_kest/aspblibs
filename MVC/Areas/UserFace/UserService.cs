﻿using ApplicationDb.Entities;
using CoreApp.AppAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.UserFace
{
    public class UserService
    {
        private readonly APIAuthorization _authorization;
        private readonly ApplicationDbContext _context;

        public UserService(APIAuthorization authorization, ApplicationDbContext context)
        {
            _authorization = authorization;
            _context = context;
        }

        public List<Message> GetMessages()
        {
            User user = _authorization.Verify();
            List<Message> messages = (from m in _context.Messages where m.ToUserID == user.ID select m).ToList<Message>();
            return messages;
        }
    }
}
