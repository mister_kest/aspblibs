﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.UserFace
{

    public static class UserExtensions
    {
        public static IServiceCollection AddUserServices(this IServiceCollection services)
        {
            Writing.ToConsole("Регистрация сервисов пользователя");
            services.AddTransient<UserService>();
            return services;
        }
        public static IApplicationBuilder UseUserMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
