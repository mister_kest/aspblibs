﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationDb.Entities;
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Areas.UserFace.Views.User;
using ASpbLibs.Core.Services;
using CoreApp.AppAPI;

using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ASpbLibs.Areas.UserFace.Controllers
{
    [Area("UserFace")]
    public class UserController : UserSessionController<UserViewModel>
    {
     
        private readonly UserService _service;
        private readonly NotificationsService _notifications;
        private readonly ApplicationDbContext _context;

        public UserController(
                 
                    NotificationsService notifications,
                    UserService service,
                    APIAuthorization authorization,
                    ApplicationDbContext context ):base( authorization )
        {
           
            _service = service;
            _notifications = notifications;
            _context = context;
        }


        /*public IActionResult Messages()
        {
            SetNavViewData("Сообщения");
            SetMessagesNavViewData("Входящие");
            return View(_service.GetMessages());
        }*/

        private void SetMessagesNavViewData(string active)
        {
            List<string> Nav = new List<string>()
            {
                "Входящие"
            };
            ViewData["MessagesNav"] = Nav;
            ViewData["MessagesNavActive"] = active;
        }

        private void SetNavViewData(string active)
        {
            List<string> Nav = new List<string>()
            {
                //"Личные данные","Сообщения","Настройки"
                "Личные данные","Настройки"
            };
            ViewData["Nav"] = Nav;
            ViewData["Active"] = active;
        }


        public IActionResult Settings()
        {
            SetNavViewData("Настройки");
            return View(_authorization.Verify().Settings);
        }


        [HttpPost]
        public IActionResult Settings([Bind("SendNewsToEmail")] Settings model)
        {
            SetNavViewData("Настройки");
            Settings settings = _context.Settings.Find(_authorization.Verify().Settings.ID);
            settings.SendNewsToEmail = model.SendNewsToEmail;
            _context.SaveChanges();
            _notifications.AddNotification(new NotificationMessage() { 
                Title = "Пользовательские настройки сохранены успешно"
            });
            _authorization.Verify().Settings = settings;
            return View(_authorization.Verify().Settings);
        }


        /// <summary>
        /// Переход на страницу отображния личной информации в личном кабинете
        /// </summary>
        /// <returns></returns>
        public override async Task<IActionResult> Index()
        {
            SetNavViewData("Личные данные");
            return await base.Index();
        }


        /// <summary>
        /// Переход по разделам личного кабинета пользователя
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IActionResult NavigateTo( string key )
        {
            SetNavViewData(key);            
            string action = null;
            switch (key)
            {
                case "Личные данные":
                    action = "Index";
                    break;
                /*case "Сообщения":
                    action = "Messages";
                    break;*/
                case "Настройки":
                    action = "Settings";
                    break;
            }
            return RedirectToAction(action);
        }


        /// <summary>
        /// Включение режима работы со считывающем устройством
        /// </summary>
        /// <returns></returns>
        public IActionResult WaitForCardReader()
        {
            SetNavViewData("Личные данные");
            var model = GetModel();
            model.WaitForCardReader = true;
            return View("Index", model);
        }


        /// <summary>
        /// Завершение обработки операции получения метки со считывающего устройства
        /// </summary>
        /// <returns></returns>
        public IActionResult CardReaded()
        {
            SetNavViewData("Личные данные");
            var model = GetModel();
            model.WaitForCardReader = false;
            return View("Index", model);
        }


      


        [HttpPost]
        public IActionResult Index([Bind("PhotoID")] UserViewModel user ) 
        {
            SetNavViewData("Личные данные");
            GetModel().PhotoID = 
            _authorization.Verify().PhotoID =
                _context.Users.Find(_authorization.Verify().ID).PhotoID = user.PhotoID;
            _context.SaveChanges();
            _notifications.AddNotification(new NotificationMessage()
            {
                Title = "Личные данные успешно сохранены"
            });
            return View("Index", GetModel());
        }


        /// <summary>
        /// Инициаллизация модели
        /// </summary>
        /// <param name="model"></param>
        public override void InitModel(UserViewModel model)
        {
            model.Init(_authorization.Verify());
        }
    }
}
