﻿using ASpbLibs.Core.Services;
using ASpbLibs.Core.Services.Libs;
using Microsoft.AspNetCore.Mvc;

namespace ASpbLibs.Controllers
{

    /// <summary>
    /// Управление боковыми панелями страницы
    /// </summary>
    [Area("UserFace")]
    public class PageLayoutController: Controller
    {
        private readonly PageLayoutService _layout;
        private readonly CategoriesService _categories;

        public PageLayoutController( 
                PageLayoutService layout,
                CategoriesService categories)
        {
            _layout = layout;
            _categories = categories;
        }


        public IActionResult OnClickHome()
        {

            var model = _layout.GetModel();
            model.Left.Clear();
            model.Left.Add(new Common.CommonTypes.PartialComponent()
            {
                View = "_Categories",
                Model = _categories.GetCategoryHier()
            });
            model.ToggleLeft();
            return PartialView("_PageLeft");
        }

        public IActionResult OnClickMessages()
        {
            _layout.GetModel().ToggleRight();
            return PartialView("_PageRight");
        }

        public IActionResult OnClickProperties()
        {
            _layout.GetModel().Right.Clear();
            _layout.GetModel().Right.Add(new Common.CommonTypes.PartialComponent() { 
                View = "_Properties",
                Model = new PropertiesModel()
            });
            _layout.GetModel().ToggleRight();
            return PartialView("_PageRight");
        }

        public IActionResult OnClickApps()
        {
            _layout.GetModel().ToggleLeft();
            return PartialView();
        }

        public IActionResult OnClickMenu()
        {            
            _layout.GetModel().Right.Clear();
            _layout.GetModel().Right.Add(new Common.CommonTypes.PartialComponent()
            {
                View = "_Categories",
                Model = _categories.GetCategoryHier()
            }); 
            _layout.GetModel().ToggleLeft();
            return PartialView("_PageLeft");
        }



















    }
}
