﻿using ASpbLibs.Core.Services.Domain.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.UserFace.Views.User
{
    public class UserViewModel: ApplicationDb.Entities.User
    {
        public void Init(ApplicationDb.Entities.User user )
        {
            ID = user.ID;

            Person = user.Person;
            Settings = user.Settings;
            Account = user.Account;
            Role = user.Role;
            Groups = user.Groups;
            Photo = user.Photo;
            PhotoID = user.PhotoID;
            Inbox = user.Inbox;
            Outbox = user.Outbox;
        }

        public bool WaitForCardReader { get; set; } = false;
    }
}
