﻿using ApplicationCommon.CommonServices;
using ApplicationDb.Entities;
using ASpbLibs.Areas.SuperviserFace.Views.Libs;
using ASpbLibs.Core.Services.Core.Services.Libs;
using CoreApp.AppAPI;
using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.SuperviserFace
{
    public class SuperviserService
    {
        private readonly EmailService _email;
        private readonly LibsRegistrationService _registration;
        private readonly APIAuthorization _auth;
        private readonly ApplicationDbContext _db;

        public SuperviserService(EmailService email, APIAuthorization auth, ApplicationDbContext db, LibsRegistrationService registration)
        {
            _email = email;
            _registration = registration;
            _auth = auth;
            _db = db;
        }

        public void AddCurator(int libId, CuratorRegistrationModel model )
        {
            Lib lib = _db.Libs.Find(libId);
            ApplicationDb.Entities.User user = _auth.GetUserByEmail(model.Email);
            if (user == null)
            {
                string password = _auth.GenerateRandomPassword(10);
                string Email = model.Email; 
                string Password = password; 
                string Confirmation = password;
                    
                string SurName = model.SurName; 
                string FirstName = model.FirstName; 
                string LastName = model.LastName; 
                DateTime Birthday = model.Birthday; 
                string Tel = model.Tel;
                _auth.Signup(Email,Password,Confirmation,SurName,FirstName,LastName,Birthday,Tel);
            }
            user = _auth.GetUserByEmail(model.Email);
            _registration.SetCuratorRole(user, lib);
            _email.SendEmail(user.Account.Email, "Ваши полномочия расширены", $"Вам была назначена роль Куратора {_db.Libs.Find(model.LibID).NameFull}.");
        }
    }
}
