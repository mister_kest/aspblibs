﻿using ApplicationMVC.Views.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.SuperviserFace.Views.Libs
{
    public class CuratorRegistrationModel: RegistrationModel
    {
        public int LibID { get; set; }
    }
}
