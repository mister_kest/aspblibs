﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.SuperviserFace
{
    public static class SuperviserExtensions
    {
        public static IServiceCollection AddSuperviserServices(this IServiceCollection services)
        {

            services.AddTransient<SuperviserService>();
            return services;
        }
        public static IApplicationBuilder UseSuperviserMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
