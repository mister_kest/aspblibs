﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;
using ASpbLibs.Areas.SuperviserFace.Views.Libs;
using ASpbLibs.Core.Services;

namespace ASpbLibs.Areas.SuperviserFace.Controllers
{
    [Area("SuperviserFace")]
    public class LibsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SuperviserService _superviser;
        private readonly NotificationsService _notifications;

        public LibsController(NotificationsService notifications, ApplicationDbContext context, SuperviserService superviser)
        {
            _context = context;
            _superviser = superviser;
            _notifications = notifications;
        }

        public async Task<IActionResult> DeleteCurator( int? id )
        {
            if (id == null)
            {
                return NotFound();
            }
            Curator c = (from p in _context.Curators.Include(c => c.Lib).Include(c => c.User) where p.UserID==id select p).FirstOrDefault();
            c.User.Person = _context.Persons.Find(c.User.PersonID);
            return View(c);
        }




         
        [HttpPost]  
        public async Task<IActionResult> AddPerson([Bind("ID,SurName,FirstName,LastName,Birthday,Tel,Email,LibID")] CuratorRegistrationModel person)
        {             
            _superviser.AddCurator(person.LibID, person);
            _notifications.AddNotification(new NotificationMessage()
            {
                Title = $"Назначение выполнено успешно. На почту {person.Email} отправлено информационное сообщение. "
            }); ; ;
            return RedirectToAction(nameof(Index));            
        }


        // GET: HolderFace/Libs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Libs.Include(l=>l.Holders).ToListAsync());
        }

        // GET: HolderFace/Libs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await (from p in _context.Libs.Include(l => l.Holders).Include(l => l.Curators) where p.ID == id select p).SingleOrDefaultAsync();
            foreach(var curator in lib.Curators)
            {
                curator.User = (from p in _context.Users.Include(u=>u.Person) where p.ID==curator.UserID select p).SingleOrDefault();
            }
            if (lib == null)
            {
                return NotFound();
            }

            return View(lib);
        }

        // GET: HolderFace/Libs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HolderFace/Libs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,NameShort,NameFull,Sigla,Url")] Lib lib)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lib);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lib);
        }

        // GET: HolderFace/Libs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await (from p in _context.Libs.Include(l => l.Holders) where p.ID == id select p).SingleOrDefaultAsync();
            if (lib == null)
            {
                return NotFound();
            }
            return View(lib);
        }

        // POST: HolderFace/Libs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,NameShort,NameFull,Sigla,Url")] Lib lib)
        {
            if (id != lib.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lib);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LibExists(lib.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lib);
        }

        // GET: HolderFace/Libs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await (from p in _context.Libs.Include(l => l.Holders) where p.ID == id select p).SingleOrDefaultAsync();
            if (lib == null)
            {
                return NotFound();
            }

            return View(lib);
        }

        // POST: HolderFace/Libs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lib = await (from p in _context.Libs.Include(l => l.Holders) where p.ID == id select p).SingleOrDefaultAsync(); ;
            _context.Libs.Remove(lib);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LibExists(int id)
        {
            return _context.Libs.Any(e => e.ID == id);
        }
    }
}
