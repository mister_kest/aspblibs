﻿using ASpbLibs.Core.Services.Core.Services.Statistics;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.SuperviserFace.Controllers
{
    [Area("SuperviserFace")]
    public class SuperviserController: Controller
    {
        private readonly StatisticsService _stat;

        public SuperviserController(StatisticsService stat)
        {
            _stat = stat;
        }
        public IActionResult SuperviserHome()
        {
            return View();
        }

        public async Task<IActionResult> CreateStatistics()
        {
            await _stat.SaveStatistics();
            return View("SuperviserHome");
        }

        
    }
}
