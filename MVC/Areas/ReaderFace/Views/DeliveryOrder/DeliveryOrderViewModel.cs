﻿using LibsDb.LibsEntities;
using LibsMVC.Areas.ReaderFace.Views.Liters;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibsMVC.Areas.ReaderFace.Views.DeliveryOrder
{
    public class DeliveryOrderViewModel
    {
        
        [Display(Name = "Доставить курьером")]
        public bool DeliverByCourier { get; set; }

        public LitersViewModel Liters { get; set; }

        public Dictionary<int, List<LibsDb.LibsEntities.Liter>> ByHolders()
        {
            Dictionary<int, List<LibsDb.LibsEntities.Liter>> result = new Dictionary<int, List<LibsDb.LibsEntities.Liter>>();
            foreach(var liter in Liters.Order)
            {
                if (result.ContainsKey(liter.HolderID) == false)
                {
                    result[liter.HolderID] = new List<LibsDb.LibsEntities.Liter>();
                }
                result[liter.HolderID].Add(liter);
            }
            return result;
        }
    }
}
