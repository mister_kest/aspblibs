﻿using LibsDb.LibsEntities;
using System.Linq;
using System.Collections.Generic;
using ASpbLibs.Application.Abstraction.BookView;
using ASpbLibs.Application.Abstraction.SearchView;

namespace LibsMVC.Areas.ReaderFace.Controllers
{
    public class ReturnOrderViewModel : BookViewModel<DeliveryFact>
    {

        public string SearchQuery { get; set; }
        public List<Liter> Liters { get; set; } = new List<Liter>();
        public List<DeliveryOrder> Orders { get; set; }


        public List<Liter> GetAllLiters(ApplicationDbContext db)
        {
            List<Liter> all = new List<Liter>();
            foreach(DeliveryOrder order in Orders)
            {
                foreach(var fact in order.Deliveries)
                {
                    all.Add(db.Liters.Find(fact.LiteratureID));
                }                
            }
            return all;
        }





        public List<int> GetLitersIds()
        {
            return (from l in this.Liters select l.ID).ToList();
        }

        
    }
}