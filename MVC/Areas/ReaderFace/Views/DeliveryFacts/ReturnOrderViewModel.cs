﻿using LibsDb.LibsEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.ReaderFace.Views.DeliveryFacts
{
    public class ReturnOrderViewModel
    {
        public string SearchQuery { get; set; } = "";
        public List<DeliveryFact> Available { get; set; }
        public List<DeliveryFact> Selected { get; set; }

        public int CurrentPage { get; set; } = 1;
        public int TotalPages { get; set; } = 1;
        public int PageSize { get; set; } = 10;

     

        public int PrevPage()
        {
            return CurrentPage - 1;
        }


        public int NextPage()
        {
            return CurrentPage + 1;
        }
    }
}
