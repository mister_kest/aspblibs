﻿using ApplicationCommon.CommonUtils;

using ASpbLibs.Application.Abstraction.TreeView;
using ASpbLibs.Common.CommonTypes.DataTypes;

using LibsDb;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LibsMVC.Areas.ReaderFace.Views.Liters
{
    public class LitersViewModel: IDisposable
    {

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchQuery { get; set; } = "";

        /// <summary>
        /// Поиск по заголовкам
        /// </summary>
        public bool SearchInTitle { get; set; } = true;

        /// <summary>
        /// Поиск по авторам
        /// </summary>
        public bool SearchInAuthors { get; set; } = true;

        /// <summary>
        /// Поиск по аннотациям
        /// </summary>
        public bool SearchInOverview { get; set; } = true;

        /// <summary>
        /// SelectCount либо DeliveriesCount
        /// </summary>
        public string OrderField { get; set; } = "SelectCount";
        public bool OrderByDesc { get; set; } = true;




        /// <summary>
        /// Признак завершенности оформления заказа
        /// </summary>
        public bool Completed { get; set; } = false;

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Номер текущей страницы
        /// </summary>
        public int CurrentPage { get; set; } = 1;

        /// <summary>
        /// Всего страниц
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Кол-во элементов на странице
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Фильтр по категориям
        /// </summary>
        public ASpbLibs.Views.Categories.CategoryHier Categories { get; set; }

        /// <summary>
        /// Иерархия мест выдачи
        /// </summary>
        public ITreeNode<NamedObject> LibHolderHier { get; set; }

        /// <summary>
        /// Все элементы
        /// </summary>
        public List<LibsDb.LibsEntities.LiterDescription> All { get; set; }

        /// <summary>
        /// Видимые элементы
        /// </summary>
        public List<LibsDb.LibsEntities.LiterDescription> Available { get; set; }

        /// <summary>
        /// Литература в корзине заказа
        /// </summary>
        public List<LibsDb.LibsEntities.Liter> Order { get; set; } = new List<LibsDb.LibsEntities.Liter>();

        /// <summary>
        /// Получение номера предыдущей страницы
        /// </summary>
        /// <returns></returns>
        public int PrevPage()
        {
            return CurrentPage -1;
        }

        /// <summary>
        /// Получение номера следующей страницы
        /// </summary>
        /// <returns></returns>
        public int NextPage()
        {
            return CurrentPage + 1;
        }

        
        /// <summary>
        /// Освобождение обьекта
        /// </summary>
        public void Dispose()
        {
            Writing.ToConsole("Dispose");
            if( !Completed && Order != null)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    foreach (var liter in Order)
                    {
                        db.Liters.Find(liter.ID).Status = 0;
                        db.SaveChanges();
                    }
                }
                    
            }
        }
    }
}
