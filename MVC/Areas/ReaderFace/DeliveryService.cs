﻿using ApplicationCommon.CommonUtils;
using ApplicationDb.Entities;
using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;

namespace ASpbLibs.Areas.ReaderFace
{
    public class DeliveryService
    {
        private readonly ApplicationDbContext _context;

        public DeliveryService( ApplicationDbContext context )
        {
            _context = context;
        }


        /// <summary>
        /// Оформление заказа на выдачу литературы
        /// </summary>
        /// <param name="order"></param>
        public void DeliveryOrderCheckout( User user, List<Liter> liters, DateTime date )
        {            
            DeliveryOrder order = new DeliveryOrder();
            order.UserID = user.ID;
            foreach (Liter liter in liters)
            {
                Liter founded = _context.Liters.Find(liter.ID);
                founded.Status = 2;
                DeliveryFact fact = new DeliveryFact();
                fact.Created = date;
                fact.ReturnBefore = date.AddMonths(1);
                fact.Literature = founded;
                fact.Calendar = (from cal in _context.Calendars where cal.Timestamp == Timing.GetTodayBeginTime() select cal).First();
                fact.Order = order;
                _context.DeliveryFacts.Add(fact);
                order.Deliveries.Add(fact);
            }
            _context.DeliveryOrders.Add(order);
            _context.SaveChanges();
        }
      
    }
}
