﻿using ApplicationDb.Entities;

using ASpbLibs.DAL.LibsEntities;

using LibsDb.LibsEntities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ASpbLibs.Areas.ReaderFace
{
    public class ReturnService
    {
        private readonly ILogger<ReturnService> _logger;
        private readonly ApplicationDbContext _context;

        public ReturnService(ILogger<ReturnService> logger, ApplicationDbContext context )
        {
            _logger = logger;
            _context = context;            
        }


        /// <summary>
        /// Получение списка фактов взятой и не возвращенной литературы читателем
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public List<DeliveryFact> GetNowStoredItems(User user)
        {
            List<DeliveryFact> facts = (from p in _context.DeliveryFacts.Include(f => f.ReturnFact).Include(f => f.Order) where p.Order.UserID == user.ID && p.ReturnFact == null select p).ToList();
            foreach(var fact in facts)
            {
                fact.Literature = _context.Liters.Include(l => l.Description).Where(l => l.ID == fact.LiteratureID).SingleOrDefault();
            }
            return facts;
        }



        /// <summary>
        /// Выполнение операции возврата литературы на пункт выдачи
        /// </summary>
        /// <param name="user"> читатель, осуществляющий возврат </param>
        /// <param name="liters"> список литературы </param>
        public void ReturnOrderCheckout(List<DeliveryFact> facts)
        {
            _logger.LogInformation("Выполнение возврата литературы");
            ReturnOrder order = new ReturnOrder();
            foreach (var fact in facts)
            {
                DeliveryFact factFromDb = _context.DeliveryFacts.Find(fact.ID);
                factFromDb.ReturnFact = new ReturnFact() { 
                    ReturnOrder = order
                };
                _context.Liters.Find(fact.LiteratureID).Status = 0;
                _context.ReturnFacts.Add(factFromDb.ReturnFact);
            }
            _context.ReturnOrders.Add(order);
            _context.SaveChanges();
        }


        /// <summary>
        /// Выполнение операции возврата литературы на пункт выдачи
        /// </summary>
        /// <param name="user"> читатель, осуществляющий возврат </param>
        /// <param name="liters"> список литературы </param>
        public async Task ReturnOrderCheckoutAsync(List<DeliveryFact> facts)
        {
            _logger.LogInformation("Выполнение возврата литературы");
            ReturnOrder order = new ReturnOrder();
            foreach (var fact in facts)
            {
                DeliveryFact factFromDb = _context.DeliveryFacts.Find(fact.ID);
                factFromDb.ReturnFact = new ReturnFact()
                {
                    ReturnOrder = order
                };
                _context.ReturnFacts.Add(factFromDb.ReturnFact);
            }
            _context.ReturnOrders.Add(order);
            await _context.SaveChangesAsync();
        }





        /// <summary>
        /// Выполнение операции возврата литературы на пункт выдачи
        /// </summary>
        /// <param name="user"> читатель, осуществляющий возврат </param>
        /// <param name="liters"> список литературы </param>
        public void ReturnOrderCheckout(User user, List<Liter> liters)
        {
            _logger.LogInformation("Выполнение возврата литературы");
            foreach(var liter in liters)
            {
                _context.Liters.Find(liter.ID).Status = 0;
            }
            _context.SaveChanges();
        }
    }
}
