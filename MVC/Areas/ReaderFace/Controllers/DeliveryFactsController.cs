﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb.LibsEntities;
using CoreApp.AppAPI;
using ASpbLibs.Areas.ReaderFace.Views.DeliveryFacts;
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Core.Services;

namespace ASpbLibs.Areas.ReaderFace.Controllers
{
    [Area("ReaderFace")]
    public class DeliveryFactsController : UserSessionController<ReturnOrderViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly APIAuthorization _authorization;
        private readonly NotificationsService _notifications;

        private readonly ReturnService _returns;

        public DeliveryFactsController(
                ReturnService returns,
          
                NotificationsService notifications,
                ApplicationDbContext context,
                APIAuthorization authorization) : base( authorization )
        {
            _context = context;
            _authorization = authorization;
            _notifications = notifications;
          
            _returns = returns;
        }
        

        public IActionResult NewOrder()
        {
            return View("ReturnOrder",NewModel()); 
        }

        public override void InitModel(ReturnOrderViewModel model)
        {
            model.Available = _returns.GetNowStoredItems(_authorization.Verify());
            model.Selected = new List<DeliveryFact>();
        }


        /// <summary>
        /// Добавление сведений о факте выдачи в заявку на возврат литературы
        /// </summary>
        /// <param name="id">идентификатор факта выдачи</param>
        /// <returns></returns>
        public IActionResult AddToOrder(int? id)
        {
            var model = GetModel();
            DeliveryFact fact = model.Available.Find(f => f.ID == id);
            if (fact != null)
            {
                _notifications.AddNotification(new NotificationMessage()
                {
                    Title = $"{fact.Literature.Description.Title} добавлена в корзину возврата"
                });
                model.Available.Remove(fact);
                model.Selected.Add(fact);
            }
            else
            {
                _notifications.AddNotification(new NotificationMessage()
                {
                    Type = "Error",
                    Title = $"Данная литература не числится за Вами"
                });
            }
            return View("ReturnOrder", model);
        }


        /// <summary>
        /// Исключение сведений о факте выдачи в заявку на возврат литературы
        /// </summary>
        /// <param name="id">идентификатор факта выдачи</param>
        /// <returns></returns>
        public IActionResult RemoveFromOrder(int? id)
        {
            var model = GetModel();
            DeliveryFact fact = model.Selected.Find(f => f.ID == id);
            if (fact != null)
            {
                model.Selected.Remove(fact);
                model.Available.Add(fact);
                _notifications.AddNotification(new NotificationMessage()
                {
                    Title = $"{fact.Literature.Description.Title} исключена из заявки на возврат"
                });
            }
            else
            {
                _notifications.AddNotification(new NotificationMessage()
                {
                    Type = "Error",
                    Title = $"Литературы нету в заявке на возврат"
                });
            }


            return View("ReturnOrder", model);
        }


        /// <summary>
        /// Завершение оформления заявки на возврат литературы
        /// </summary>
        /// <returns></returns>
        public IActionResult Checkout()
        {
            var model = GetModel();
            _returns.ReturnOrderCheckout(model.Selected);
            _notifications.AddNotification(new NotificationMessage()
            {
                Title = $"Оформление возврата литературы завершено"
            });
            return View("ReturnOrder", NewModel());
        }


  

    }
}
