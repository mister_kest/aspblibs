﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;
using LibsMVC.Areas.ReaderFace.Views.Liters;
using CoreApp.AppAPI;
using ASpbLibs.Core.Services;
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Core.Services.Libs;


namespace ASpbLibs.Areas.ReaderFace.Controllers
{
    [Area("ReaderFace")]
    public class LitersController : UserSessionController<LitersViewModel>
    {
      
        private readonly ApplicationDbContext _context;
        private readonly NotificationsService _notifications;
        private readonly CategoriesService _categories;


        public LitersController(
                CategoriesService categories, 
                NotificationsService notifications, 
                         
                ApplicationDbContext context, 
                APIAuthorization authorization) :base(authorization)
        {
          
            _context = context;
            _categories = categories;
            _notifications = notifications;
        }







        public override async Task<IActionResult> Index()
        {
            LitersViewModel model = GetModel();

            List<int> selectedCategoryIds = _categories.GetSelectedCategoryIds();
            model.Categories = _categories.GetCategoryHier();
            await UpdateModel();
            return View(model);
        }




        public IActionResult Checkout()
        {
            return RedirectToAction("Index","DeliveryOrder",new { 
                area="ReaderFace"
            });
        }


        public async Task<IActionResult> SetPage(int page)
        {
            var model = GetModel();
            model.CurrentPage = page;
            model = await UpdateModel();
            return View("Index",  model);
        }


        public async Task<IActionResult> RemoveFromOrder(int? id)
        {
            Liter liter = (from l in GetModel().Order where l.ID == id select l).SingleOrDefault();
            if (liter != null)
            {
                GetModel().Order.Remove(liter);
                (from f in _context.Liters.Include(l => l.Description) where f.ID==liter.ID select f).SingleOrDefault().Status=0;
                await _context.SaveChangesAsync();
            }
            return View("Index",await UpdateModel());
        }

        
        
        public async Task<IActionResult> AddToOrder(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }

            LitersViewModel model = GetModel();
            lock (model)
            {
                var liter = (from l in _context.Liters.Include(l => l.Description) where l.ID == id select l).SingleOrDefault();
                if (liter == null)
                {
                    return NotFound();
                }
                if (liter.Status != 0)
                {
                    _notifications.AddNotification(new NotificationMessage()
                    {
                        Title = liter.Description.Title + " уже занята другим читателем",
                        Type = "Error"
                    });
                }
                else
                {
                    liter.Status = 1;
                    model.Order.Add(liter);
                    _notifications.AddNotification(new NotificationMessage()
                    {
                        Title = liter.Description.Title + " добавлена в корзину заказа",
                        Type = "Success"
                    });
                    _context.SaveChanges();
                }
            }
            
         
            return View("Index", await UpdateModel());
        }


        /// <summary>
        /// Обновление страницы
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Update()
        {
            await UpdateModel();
            return View("Index",GetModel());
        }



        /// <summary>
        /// Выполнение поиска литературы
        /// </summary>
        /// <param name="fromRequest"> строка поиска </param>
        public async Task<IActionResult> Search( [Bind("SearchQuery")] LitersViewModel fromRequest)
        {
            string query = fromRequest.SearchQuery;
            LitersViewModel model = GetModel();
            model.SearchQuery = query;
            await UpdateModel();
            return View("Index", model);
        }


        private void UpdatePagesInModel(LitersViewModel model)
        {
            var dataset = model.All;
            model.TotalPages = (dataset.Count() % model.PageSize == 0) ?
                    (int)Math.Floor((decimal)(dataset.Count() / model.PageSize)) :
                    1 + (int)Math.Floor((decimal)(dataset.Count() / model.PageSize));
            int page = model.CurrentPage;
            int size = model.PageSize;
            model.Available = page == 1 ? dataset.Take(size).ToList() :
                    dataset
                    .Skip((page - 1) * size)
                    .Take(size).ToList();
        }


        private async Task<LitersViewModel> UpdateModel()
        {
            LitersViewModel model = GetModel();
            int size = model.PageSize;
            int page = model.CurrentPage;

            List<int> selectedCategoryIds = _categories.GetSelectedCategoryIds();
            model.All =
                (from p in _context.LiterDescriptions
                 where 
                   (String.IsNullOrEmpty(model.SearchQuery) || String.IsNullOrWhiteSpace(model.SearchQuery) || p.Title.ToLower().IndexOf(model.SearchQuery.ToLower()) != -1) 
                   && selectedCategoryIds.Contains(p.CategoryID)
                   && (p.Liters.Where(l=>l.Status==0).Count()>0)
                 select p).ToList();
            UpdatePagesInModel(model);
            model.All = (from p in _context.LiterDescriptions.Include(d => d.Liters) where (from pp in model.All select pp.ID).Contains(p.ID) select p).ToList();
            return model;
        }

        
        private bool LiterExists(int id)
        {
            return _context.Liters.Any(e => e.ID == id);
        }

        public override void InitModel(LitersViewModel model)
        {
           
        }
    }
}
