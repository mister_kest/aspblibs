﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LibsDb.LibsEntities;
using CoreApp.AppAPI;
using ApplicationCommon.CommonUtils;
using LibsMVC.Areas.ReaderFace.Views.DeliveryOrder;
using LibsMVC.Areas.ReaderFace.Views.Liters;
using ASpbLibs.Application.Abstraction;
using ASpbLibs.Core.Services;

namespace ASpbLibs.Areas.ReaderFace.Controllers
{

    [Area("ReaderFace")]
    public class DeliveryOrderController : UserSessionController<DeliveryOrderViewModel>
    {
        private readonly ApplicationDbContext _context;

        public NotificationsService _notifications { get; }

        public DeliveryOrderController(
                ApplicationDbContext context,
                NotificationsService notifications,
                APIAuthorization authorization): base(authorization)
        {
            _context = context;
            _notifications = notifications;
        }


        /// <summary>
        /// Переход на страницу оформления заказа
        /// </summary>
        /// <returns></returns>
        public async override Task<IActionResult> Index()
        {
            DeliveryOrderViewModel model = GetModel();
            if(model.Liters == null)
            {
                model.Liters = GetAnotherModel<LitersViewModel>(typeof(LitersController).FullName + "::" + typeof(LitersViewModel).FullName);           
            }
            return View(model);
        }


        /// <summary>
        /// Оформление заказа
        /// </summary>
        /// <returns></returns>
        public async Task <IActionResult> Checkout()
        {
            DeliveryOrderViewModel model = GetModel();
            lock (model)
            {
                model.Liters.Completed = true;
                DeliveryOrder order = new DeliveryOrder();
                order.UserID = _authorization.Verify().ID;
                foreach (Liter liter in model.Liters.Order)
                {

                    Liter founded = _context.Liters.Find(liter.ID);
                    founded.Status = 2;
                    DeliveryFact fact = new DeliveryFact();
                    fact.Created = DateTime.Now;
                    fact.ReturnBefore = DateTime.Now.AddMonths(1);
                    fact.Literature = founded;
                    fact.Calendar = (from cal in _context.Calendars where cal.Timestamp == Timing.GetTodayBeginTime() select cal).First();
                    fact.Order = order;
                    _context.DeliveryFacts.Add(fact);
                    order.Deliveries.Add(fact);
                }
               
                _context.DeliveryOrders.Add(order);
                model.Liters.Order.Clear();
                NewModel().Liters = GetAnotherModel<LitersViewModel>(typeof(LitersController).FullName + "::" + typeof(LitersViewModel).FullName);
                _context.SaveChanges();

                _notifications.AddNotification(new NotificationMessage() { 
                    Title = $"Оформление заказа на выдачу завершено, можете забрать литературу и наслаждаться чтением"
                });
            }
            
            return View("Success");
        }


        /// <summary>
        /// Переход на страницу просмотра завершенных заказов
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Completed()
        {
            List<DeliveryOrder> userOrders = await
                                             (from orders 
                                              in _context.DeliveryOrders
                                              where orders.UserID == _authorization.Verify().ID
                                              select orders).ToListAsync();
            foreach(DeliveryOrder order in userOrders)
            {
                order.Deliveries = await (from facts 
                                          in _context.DeliveryFacts.Include(f=>f.Literature)
                                          where facts.OrderID == order.ID 
                                          select facts).ToListAsync();
            }
            return View(userOrders);
        }


        /// <summary>
        /// Инициаллизация модели
        /// </summary>
        /// <param name="model"></param>
        public override void InitModel(DeliveryOrderViewModel model)
        {
            //TODO:
        }
    }
}
