﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.ReaderFace
{

    public static class ReaderExtensions
    {
        public static IServiceCollection AddReaderServices(this IServiceCollection services)
        {
            Writing.ToConsole("Регистрация сервисов читателя");
            services.AddTransient<ReaderService>();
            services.AddTransient<DeliveryService>();
            services.AddTransient<ReturnService>();
            return services;
        }
        public static IApplicationBuilder UseReaderMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
