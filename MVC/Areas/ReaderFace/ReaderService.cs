﻿using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.ReaderFace
{
    public class ReaderService
    {

        private readonly ApplicationDbContext _context;

        public ReaderService(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Проверка возможности продления срока хранения
        /// </summary>
        /// <param name="deliveryFactId"> идентификатор факта выдачи </param>
        /// <returns></returns>
        public bool CanIncreaseStoreTime( int deliveryFactId )
        {
            DeliveryFact fact = _context.DeliveryFacts.Find(deliveryFactId);
            if( fact == null)
            {
                throw new ArgumentException("deliveryFactId");
            }
            return fact.HasBeenIncresed;
        }
    }
}
