﻿using ApplicationCommon.CommonUtils;
using ApplicationDb;
using ApplicationDb.Entities;

using LibsDb.LibsEntities;

using System;
using System.Collections.Generic;
using System.Text;

namespace ASpbLibs.Areas.AdminFace
{
    public class NewsService
    {
        private readonly ApplicationDbContext _context;

        public NewsService( ApplicationDbContext context )
        {
            _context = context;
        }

      /*  public void AddMessageAboutDeliveryComplete(DeliveryOrder order, ApplicationDb.Entities.User user)
        {

        }*/


        /// <summary>
        /// Добавление сообщения с информацией о проведении технических работ
        /// </summary>
        /// <param name="date"> дата проведения </param>
        /// <param name="beginAtHour"> время (час) начала </param>
        /// <param name="longlessInHours"> продолжительность в часах </param>
        public void AddMessageAboutNewLiter(DateTime date, Holder holder, Liter liter)
        {
            _context.News.Add(new News()
            {
                Time = DateTime.Now,
                Title = $"{liter.Description.Year} {liter.Description.Title} поступила на " +holder.Location,
                Description = $"<p> {date.Date} {Timing.GetMonthName(date.Month)} поступила новая литература " +
                    $"на пункт {holder.Location}. <br/> {liter.Description.Overview}"//,
                //Href = liter.GetUrlLocaion()
            });
            _context.SaveChanges();
        }


        /// <summary>
        /// Добавление сообщения с информацией о регистрации пункта выдачи
        /// </summary>
        /// <param name="date"> дата проведения </param>
        /// <param name="beginAtHour"> время (час) начала </param>
        /// <param name="longlessInHours"> продолжительность в часах </param>
        public void AddMessageAboutNewHolder(Lib lib, Holder holder)
        {
            _context.News.Add(new News()
            {
                Time = DateTime.Now,
                Title = $"{lib.NameShort} открыл новый пункт выдачи на "+holder.Location,
                Description = $"{lib.NameShort} открыл новый пункт выдачи на " + holder.Location + " и приглашает читателей на открытие.",
                Href = holder.GetUrl()

            });
            _context.SaveChanges();
        }

        /// <summary>
        /// Добавление сообщения с информацией о проведении технических работ
        /// </summary>
        /// <param name="date"> дата проведения </param>
        /// <param name="beginAtHour"> время (час) начала </param>
        /// <param name="longlessInHours"> продолжительность в часах </param>
        public void AddMessageAboutTechnicalJob(DateTime date, int beginAtHour, int longlessInHours )
        {
            _context.News.Add(new News() { 
                Time = DateTime.Now,
                Title = "Технические работы",
                Description = $"<p>  На {date.Date} {Timing.GetMonthName(date.Month)} запланированы технические "+
                    $"работы в период с {beginAtHour}:00 по {beginAtHour+longlessInHours}:00 ресурс будет недоступен. </p>"
            });
            _context.SaveChanges();
        }
    }
}
