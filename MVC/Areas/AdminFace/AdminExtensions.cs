﻿using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibs.Areas.AdminFace
{

    public static class AdminExtensions 
    {
        public static IServiceCollection AddAdminServices(this IServiceCollection services)
        {
            Writing.ToConsole("Регистрация сервисов администратора");
            services.AddTransient<AdminService>();
            services.AddTransient<NewsService>();
            return services;
        }
        public static IApplicationBuilder UseAdminMiddleware(this IApplicationBuilder app)
        {
            return app;
        }
    }
}
