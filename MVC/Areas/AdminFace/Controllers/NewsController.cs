﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationDb.Entities;
using ASpbLibs.Application.Abstraction.BookView;
using ASpbLibs.Core.Services.CRUD;
using CoreApp.AppAPI;
 
using ASpbLibs.Application.Abstraction.SearchView;
using ASpbLibs.Core.Services;

namespace ASpbLibs.Areas.AdminFace.Controllers
{
    [Area("AdminFace")]
    public class NewsController : SearchViewController<News>
    {
        private readonly ApplicationDbContext _context;
        private readonly PageLayoutService _layout;

        public NewsController(ApplicationDbContext context, 
                CRUDProtocol crud,
                    PageLayoutService layout,
                APIAuthorization authorization) : base(authorization)
        {
            _context = context;
            _layout = layout;


        }


        public override IQueryable<News> GetAll(SearchViewModel<News> model)
        {
            _layout.ShowCategories();
            return _context.News.Where(n => model.SearchQuery == "" || n.Title.ToLower().IndexOf(model.SearchQuery.ToLower()) != -1).OrderByDescending(n => n.Time);
        }

        public override List<string> GetKeywords(string query)
        {
            HashSet<string> keywords = new HashSet<string>();
            List<string> titles =
                (from p in _context.News
                 where p.Title.ToLower().IndexOf(query) != -1
                 select p.Title).Take(10).ToList();
            foreach (string title in titles)
            {
                foreach (string word in new List<string>(title.ToLower().Split(' ', '.', ',', '!')))
                {
                    keywords.Add(word);
                }
            }
            return new List<string>(keywords);
        }

 

        public override string GetViewName()
        {
            return "Index";
        }


        /// <summary>
        /// Переход на страницу просмотра сведений о Новости
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News
                .FirstOrDefaultAsync(m => m.ID == id);
            if (news == null)
            {
                return NotFound();
            }

            return View(news);
        }

    
        /// <summary>
        /// Переход на страницу добавления Новости
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View(new News()
            {
                Time = DateTime.Now
            });
        }


        /// <summary>
        /// Добавления Новости
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Time,ImageID,Href,Description")] News news)
        {
            if (ModelState.IsValid)
            {
                _context.Add(news);
                await _context.SaveChangesAsync();
                NewModel();
                return await SetPage(1);
            }
            return View(news);
        }


        /// <summary>
        /// Переход на страницу редактирования Новости
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News.FindAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            NewModel();
            return await SetPage(1);
        }


        /// <summary>
        /// Редактирование Новости
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Time,Image,Href,Description")] News news)
        {
            if (id != news.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(news);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewsExists(news.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(news);
        }

       
        /// <summary>
        /// Удаление Новости
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News.FirstOrDefaultAsync(m => m.ID == id);
            if (news == null)
            {
                return NotFound();
            }

            return View(news);
        }

        
        /// <summary>
        /// ПОдтверждение удаления Новости
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var news = await _context.News.FindAsync(id);
            _context.News.Remove(news);
            await _context.SaveChangesAsync();
            NewModel();
            return await SetPage(1);
        }


        /// <summary>
        /// Проверка наличия записи с заданным идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool NewsExists(int id)
        {
            return _context.News.Any(e => e.ID == id);
        }

     
    }
}
