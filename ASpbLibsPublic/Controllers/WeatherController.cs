﻿using ASpbLibs.Core.Services.APIS.OpenWeatherAPI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibsPublic.Controllers
{
    [Route("[controller]/[action]")]
    public class WeatherController: ControllerBase
    {
        private readonly OpenWeatherService _weather;

        public WeatherController(OpenWeatherService weather)
        {
            _weather = weather;
        }

        public async Task<string> GetWeather(double lat, double lon, long time)
        {
            return await _weather.GetOneCall(lat, lon, time);
        }
    }
}
