﻿
using ASpbLibs.Core.Services.APIS.TheMovieDatabaseAPI;
using ASpbLibsPublic.API;
using ASpbLibsPublic.Core;
using ASpbLibsPublic.Views.Movies;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ASpbLibsPublic.Controllers
{
    public class MoviesController : Controller
    {
        private readonly TheMovieDatabaseService _movies;
        private readonly PublicSessionService _session;

        public MoviesController(TheMovieDatabaseService movies, PublicSessionService session)
        {
            _movies = movies;
            _session = session;
        }


        /// <summary>
        /// Переход на домашнюю страницу, отображает результаты поиска наиболее популярных кинофильмов
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Home()
        {
          
            //string responseText = await _movies.getMoviesLatest();
          
            string json = await _movies.getMoviesPopular();
            SearchResults results = JsonConvert.DeserializeObject<SearchResults>(json);
            MoviesViewModel model = new MoviesViewModel()
            {
                CurrentPage = 1,
                PageSize = results.results.Count(),
                TotalPages = results.total_pages,
                Title = "Популярное кино",
                Results = new List<object>(),
                //Latest = JsonConvert.DeserializeObject<MovieResult>(responseText),
                IsPopularSearch = true
            };
            foreach (var result in results.results)
            {
                model.Results.Add(result);
            }
            _session.Set(model);
            return View("Movies", model);
        }


        /// <summary>
        /// Переход на страницу просмотра результатов поиска кинофильмов
        /// </summary>
        /// <param name="page">номер страницы</param>
        /// <returns></returns>
        public IActionResult SetPage(int page)
        {
            MoviesViewModel model = null;
            try
            {
                model = _session.Get<MoviesViewModel>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            if (model == null)
            {
                return RedirectToAction("Home");
            }
            if (model.IsPopularSearch)
            {
                SearchResults results = JsonConvert.DeserializeObject<SearchResults>(_movies.getMoviesPopular(model.CurrentPage = page).Result);
                model.Results.Clear();
                foreach (var result in results.results)
                {
                    model.Results.Add(result);
                }
            }
            else
            {
                SearchResults results = _movies.movieSearch(model.SearchQuery, model.CurrentPage = page).Result;
                model.Results.Clear();
                foreach (var result in results.results)
                {
                    model.Results.Add(result);
                }
            }
            _session.Set(model);
            return View("Movies", model);
        }


        /// <summary>
        /// Запрос списка слов автодополнения поискового запроса
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<IActionResult> OnInput(string query)
        {
           
            string responseText = await _movies.keywordSearch(query, 1);
            JArray results = (JArray)((JsonConvert.DeserializeObject<JObject>(responseText))["results"]);
            List<string> keywords = (from p in results select p["name"].Value<string>()).ToList();


            return Json(keywords);
        }

        public async Task<IActionResult> Search(string query)
        {
            
            string responseText = await _movies.getMoviesLatest();
            
            SearchResults results = await _movies.movieSearch(query, 1);
            MoviesViewModel model = new MoviesViewModel()
            {
                CurrentPage = 1,
                PageSize = results.results.Count(),
                TotalPages = results.total_pages,
                Title = "Популярное кино",
                Results = new List<object>(),
                //Latest = JsonConvert.DeserializeObject<MovieResult>(responseText),
                IsPopularSearch = false,
                SearchQuery = query
            };
            foreach (var result in results.results)
            {
                model.Results.Add(result);
            }
            _session.Set(model);
            return View("Movies", model);
        }
    }

}
