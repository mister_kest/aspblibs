function Component(options) {
    console.log('Component');
    return function specification(constructor) {
        $context().components.push({
            options: options,
            constructor: constructor
        });
    };
}
//# sourceMappingURL=component.js.map