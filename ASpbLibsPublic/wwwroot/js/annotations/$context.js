function $context() {
    if (!window['app']) {
        return window['app'] = {
            modules: [],
            components: [],
            directives: [],
            services: []
        };
    }
    else {
        return window['app'];
    }
}
//# sourceMappingURL=$context.js.map