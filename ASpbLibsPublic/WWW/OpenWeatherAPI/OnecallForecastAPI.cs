﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASpbLibs.Core.Services.APIS.OpenWeatherAPI
{
    /// <summary>
    /// Реализует запросы к сервису Onecall
    /// </summary>
    public class OnecallForecastAPI: BaseOpenWeatherAPI
    {  
        private static string BASE_URI = "data/2.5/onecall";


        public OnecallForecastAPI():base(OnecallForecastAPI.BASE_URI)
        {
        }


        /// <summary>
        /// Запрашивает данные о погоде с заданной координатой и временем
        /// </summary>
        public async Task<string> GetOneCall( double lat, double lon, long time )
        {
            Dictionary<string, object> pars = new Dictionary<string, object>();
            pars["lat"] = lat;
            pars["lon"] = lon;
            pars["time"] = time;
            pars["lang"] = "ru";
            pars["units"] = "metric";
            return await this.Request(pars);
        }
    }
}
