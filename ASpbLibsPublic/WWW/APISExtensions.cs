﻿using Microsoft.Extensions.DependencyInjection;
using ASpbLibs.Core.Services.APIS.OpenWeatherAPI;
using ASpbLibs.Core.Services.APIS.TheMovieDatabaseAPI;
using System;

namespace ASpbLibs.Core.Services.APIS
{
    public static class APISExtensions
    {
        public static IServiceCollection AddWebApiServices( this IServiceCollection services )
        {
            Console.WriteLine($"APISExtensions.AddWebApiServices");
           
            services.AddHttpContextAccessor();
            services.AddTransient<OnecallForecastAPI>();
            services.AddTransient<WeatherForecastAPI>();
            services.AddTransient<OpenWeatherService>();
            services.AddTransient<TheMovieDatabaseService>();
            return services;
        }
    }
}
