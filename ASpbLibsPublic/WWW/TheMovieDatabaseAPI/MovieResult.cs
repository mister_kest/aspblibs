﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ASpbLibs.Core.Services.APIS.TheMovieDatabaseAPI
{
    public class MovieResult
    {
        public string popularity { get; set; }
        public string vote_count { get; set; }
        public string video { get; set; }
        public string poster_path { get; set; }
        public string id { get; set; }
        public string adult { get; set; }
        public string backdrop_path { get; set; }
        public string original_language { get; set; }
        public string original_title { get; set; }
        public string title { get; set; }
        public string vote_average { get; set; }
        public string overview { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM.dd.yyyy}")]
        public DateTime? release_date { get; set; }         
        
        public string Year
        {
            get
            {
                return release_date==null? "": ((DateTime)this.release_date).Year.ToString();
            }
        }

        public string GetPosterPath()
        {
            string path = String.IsNullOrEmpty(poster_path) ? backdrop_path : poster_path;
            if (String.IsNullOrEmpty(path))
            {
                return path;
            }
            else
            {
                return "https://image.tmdb.org/t/p/w500" + path + "?api_key=72b56103e43843412a992a8d64bf96e9";
            }
            
        }
    }
}