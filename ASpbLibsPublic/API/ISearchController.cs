﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibsPublic.API
{
    public interface ISearchController
    {

        List<string> OnInput(string query);
    }
}
