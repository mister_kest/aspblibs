﻿
using ApplicationCommon.CommonUtils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibsPublic.Core
{
    public class PublicSessionService
    {
        private IHttpContextAccessor _accessor;

        public PublicSessionService(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public T Get<T>()  
        {
            Writing.ToConsole(typeof(T).FullName);
            string xml = _accessor.HttpContext.Session.GetString(typeof(T).FullName);
            if (xml == null)
            {
                throw new Exception($"Сеанс не содержит обьекта с ключом {typeof(T).FullName}");
            }
            T result = Formating.FromXML<T>(xml);
            return result;
        }

       
        public void Set(object model)
        {            
            _accessor.HttpContext.Session.SetString(model.GetType().FullName, Formating.ToXML(model));
        }
    }
}
