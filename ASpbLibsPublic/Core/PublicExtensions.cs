﻿using ApplicationCommon.CommonUtils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibsPublic.Core
{
    public static class PublicExtensions
    {
        public static IServiceCollection AddPublicSession(this IServiceCollection services, int minutes)
        {
            Writing.ToConsole("Регистрация сеанса анонимного пользователя");
            services.AddHttpContextAccessor();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(minutes*60);
                options.Cookie.HttpOnly = false;
                options.Cookie.IsEssential = true;
            });
            services.AddTransient<PublicSessionService>();
            return services;
        }
    }
}
