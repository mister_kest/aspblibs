﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationDb.Types
{
    /// <summary>
    /// Справочник ( дополнительная информация, не обрабатываеся приложением )
    /// </summary>
    public class DictionaryTable
    {

        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Необходимо указать наменование")]
        public string Name { get; set; }
   
    }
}
