﻿using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ApplicationCommon.CommonUtils
{
    public class Formating
    {
        /// <summary>
        /// Форматирование обьекта в JSON
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string Json(object target)
        {
            if (target is string)
            {
                return (string)target;
            }
            else if (target is JObject)
            {
                return ((JObject)target).ToString();
            }
            else
            {
                return JObject.FromObject(target).ToString();
            }
        }


        /// <summary>
        /// Форматирование обьекта в XML
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string ToXML(object target)
        {            
            XmlSerializer formatter = new XmlSerializer(target.GetType());            
            using (StringWriter writer = new StringWriter())
            {
                formatter.Serialize(writer, target);
                writer.Flush();
                return writer.ToString();
            }                        
        }

        /// <summary>
        /// Форматирование обьекта в XML
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static T FromXML<T>(string xml)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(T));
            using (StringReader reader = new StringReader(xml))
            {
                object item = formatter.Deserialize(reader);                 
                return (T)item;
            }
        }
    }
}
