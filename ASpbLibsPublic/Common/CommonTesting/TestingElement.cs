﻿using System;
using System.Collections.Generic;

namespace ASpbLibs.Core.Services.CoreTesting
{
    /// <summary>
    /// Модуль тестирования наследует данный класс.
    /// </summary>
    public abstract class TestingElement: Dictionary<string, TestingUnit>  
    {
        /// <summary>
        /// Отчёт о проведении тестирования
        /// </summary>
        protected TestingReport report;

        /// <summary>
        /// Список сообщений, полученныйв результате тестирования
        /// 
        /// </summary>
        protected List<string> messages = new List<string>();


        /// <summary>
        /// Реализация метода тестирования
        /// </summary>
        protected abstract void onTest();


        /// <summary>
        /// Добавление метода тестирования
        /// </summary>
        /// <param name="unit"> метод тестирования </param>
        protected void Push(TestingUnit unit)
        {
            this[unit.GetType().Name] = unit;
        }


        /// <summary>
        /// Выполнения теста и составления отчета о тестировании
        /// </summary>
        /// <returns> отчет о тестировании </returns>
        public TestingReport doTest()
        {
            //Console.WriteLine( $"doTest {this.GetType().Name}" );
            this.report = new TestingReport();
            TestingReport report = this.report;
            report.messages = this.messages;
            report.name = this.GetType().Name.Substring("Test".Length);
            try
            {
                report.started = DateTime.Now;
                this.onTest();                
            }
            catch (Exception ex)
            {                
                report.failed = true;
                report.messages.Add( ex.ToString() );
                throw;
            }
            finally
            {
                report.ended = DateTime.Now;

                foreach (var p in this)
                {
                    report.subreports[p.Key] = p.Value.doTest();
                    if( report.subreports[p.Key].failed)
                    {
                        report.failed = true;
                    }
                }    
                
            }
            return report;
        }
    }
}
