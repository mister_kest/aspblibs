﻿using System;
using System.Collections.Generic;

namespace ASpbLibs.Core.Services.CoreTesting
{
    /// <summary>
    /// Модуль тестирования наследует данный класс.
    /// </summary>
    public abstract class TestingUnit: TestingElement
    {
        protected override void onTest()
        {
            
        }
    }
}
