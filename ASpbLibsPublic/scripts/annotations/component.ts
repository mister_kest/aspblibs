﻿function Component(options) {
    console.log('Component');
    return function specification(constructor: Function) {
        $context().components.push({
            options:        options,
            constructor:    constructor
        });
    };
}