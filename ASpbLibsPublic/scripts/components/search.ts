﻿@Component({
    selector: 'search',
    template: `
        <div style="width: 100%; 
                    display: flex; flex-direction: row; flex-wrap: nowrap; 
                    align-items: baseline; j
                    ustify-content: baseline; ">

            <input class="form-control" list="inputSearchOptions" type="text"    
                   ng-model="query"
                    placeholder="Поиск" aria-label="Search" autofocus="true"
                    style="top: 0px; left: 0px; width: 100%;">

            <button class="btn btn-primary my-2 my-sm-0" type="submit" ng-click="onClick()"> поиск </button>
            <datalist id="inputSearchOptions">                
                <option ng-repeat="option in options" value="{{ option }}"> 
                    {{ option }}
                </option>                
            </datalist>
        </div>
    `
})
class SearchComponent
{
    constructor(private $scope, private $element, private $attrs, private $http ) {
        console.log('search', $scope);
        $scope.options = ['a', 'aa'];
    }

    


    $onInit() {
        const ctrl = this;
        this.$scope.query = '';
        this.$scope.search=function() {
          
            document.location.href = ctrl.$attrs.href + '/Search?query=' + ctrl.$scope.query ;
        }
        this.$scope.onClick = function () {
            document.location.href = ctrl.$attrs.href + '/Search?query=' + ctrl.$scope.query;
        }
        this.$element[0].onkeypress = function (evt) {
            console.log(evt);
            if (evt.key == 'Enter') {
                ctrl.$scope.search( ctrl.$scope.query );
            }
        }
        this.$element[0].oninput = function (evt) {            
            ctrl.$http({
                url:    ctrl.$attrs.href + '/OnInput',
                params: {
                    query: evt.target.value
                }
            }).then((resp) => {
                ctrl.$scope.options = resp.data;
            });
        }
        
    }
}