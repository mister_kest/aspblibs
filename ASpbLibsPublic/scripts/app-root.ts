﻿class AppRoot {
    bootstrap() {
        const angular = window['angular'];
        const app = angular.module(
            this.$markupContent(angular), [/*'moduleOfCharts'*/]);
        app.controller(this.$markupRoot(angular), function ($scope) {
            console.log($scope);
        
        });
       
        for (let i = 0; i < window['app'].components.length; i++) {
            const options = window['app'].components[i].options;
            console.log(options.selector);
          
            app.directive(options.selector, function () {
                return {
                    template:   options.template,
                    controller: window['app'].components[i].constructor,
                    link: function ( $scope ) {
                        console.log('frame',$scope);
                    }
                }
            });
        }
    }


    $markupRoot(angular) {
        const selection = angular.element('[ng-app]');
        if (selection.length == 0) {
            throw new Error('firstly add ng-app attribute at root node');
        } else {
            const id = selection[0].getAttribute('ng-app') + 'Ctrl';
            selection[0].setAttribute('ng-controller', id);
            return id;
        }
    }

    $markupContent(angular) {
        const selection = angular.element('[ng-app]');
        if (selection.length > 0) {
            return selection[0].getAttribute('ng-app');
        } else {
            let uri = location.pathname.substring(1);
            const slashIndex = uri.indexOf('/');
            if (slashIndex == -1) {
                throw new Error('run application in child context');
            } else {
                uri = uri.substring(0, slashIndex);
                document.head.parentElement.setAttribute('ng-app', uri);
                return uri;
            }
        }
    }
}