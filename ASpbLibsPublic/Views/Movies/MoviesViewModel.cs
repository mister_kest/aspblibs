﻿
using ASpbLibs.Core.Services.APIS.TheMovieDatabaseAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASpbLibsPublic.Views.Movies
{
    public class MoviesViewModel
    {
        internal bool IsPopularSearch { get; set; } = true;

        /// <summary>
        /// Latest,Populare,Search
        /// </summary>
        public string SearchQuery { get; set; }

        public MovieResult Latest { get; set; }


        public string Title { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int TotalPages { get; set; } = 1;
        public int PageSize { get; set; } = 10;

        public List<object> Results { get; set; }

        public List<T> GetResults<T>()
        {
            List<T> res = new List<T>();
            foreach (object o in Results)
            {
                res.Add((T)o);
            }
            return res;
        }

        public int PrevPage()
        {
            return CurrentPage - 1;
        }


        public int NextPage()
        {
            return CurrentPage + 1;
        }


    }
}
