﻿using ASpbLibs.Application.Abstraction.BookView;
using LibsEsbo.EsboEntities;

using System;
using System.Diagnostics;
using System.Linq;

namespace LibsEsbo
{ 
    class Program
    {
        static void Main(string[] args)
        {

          
            
            using (EsboDbContext db = new EsboDbContext())
            {
                foreach( var rec in db.TRDRs.Take(10).ToList())
                {
                    Console.WriteLine(rec.ID);
                }
            }
        }
    }
}
