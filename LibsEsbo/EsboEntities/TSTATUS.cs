﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TSTATUS
    {
        public TSTATUS()
        {
            T1050s = new HashSet<T1050>();
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public string STATUS { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<T1050> T1050s { get; set; }
        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
