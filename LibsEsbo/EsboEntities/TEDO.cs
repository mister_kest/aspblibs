﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TEDO
    {
        public int R_ID { get; set; }
        public int TIP_ID { get; set; }
        public int ST_ID { get; set; }
        public int NP { get; set; }
        public DateTime? V1052A { get; set; }
        public string V1052B { get; set; }
        public int LIB_ID { get; set; }
        public int? EDOMX_ID { get; set; }
        public bool V1052G { get; set; }
        public string V1052H { get; set; }
        public string V1052I { get; set; }
        public bool V1052J { get; set; }
        public bool V1052K { get; set; }
        public bool V1052L { get; set; }
        public string V1052M { get; set; }
        public string V1052N { get; set; }
        public bool V1052O { get; set; }
        public string V1052P { get; set; }
        public bool V1052Q { get; set; }
        public DateTime? ANNDT { get; set; }
        public string ANNFIO { get; set; }
        public string ANNWHY { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string MD5CACHE { get; set; }

        public virtual TEDOMX EDOMX { get; set; }
        public virtual TLIB LIB { get; set; }
        public virtual TRDR R_IDNavigation { get; set; }
        public virtual TEDO_TIP TIP { get; set; }
    }
}
