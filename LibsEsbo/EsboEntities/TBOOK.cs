﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TBOOK
    {
        public TBOOK()
        {
            TEKZs = new HashSet<TEKZ>();
        }

        public int ID { get; set; }
        public int LIB_ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string V40C { get; set; }
        public string V40G { get; set; }
        public string V903 { get; set; }
        public string V933 { get; set; }
        public int JYEAR { get; set; }
        public string JVOLUME { get; set; }
        public string JNUM { get; set; }
        public string V40RZN { get; set; }
        public string V40HAR { get; set; }
        public string V40VID { get; set; }
        public string V900T { get; set; }
        public string V920 { get; set; }
        public decimal V10D { get; set; }
        public int MFN { get; set; }
        public byte REPSTATUS { get; set; }
        public bool? PRICE_CHECK { get; set; }
        public string BBK { get; set; }
        public string V210D { get; set; }
        public string AZ { get; set; }
        public string V110B { get; set; }
        public byte? V333 { get; set; }
        public int? IVERS { get; set; }

        public virtual ICollection<TEKZ> TEKZs { get; set; }
    }
}
