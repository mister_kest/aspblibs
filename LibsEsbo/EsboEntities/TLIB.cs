﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TLIB
    {
        public TLIB()
        {
            T1050s = new HashSet<T1050>();
            T1051s = new HashSet<T1051>();
            T129s = new HashSet<T129>();
            T33s = new HashSet<T33>();
            T5152s = new HashSet<T5152>();
            TEDOs = new HashSet<TEDO>();
            TKVMNUs = new HashSet<TKVMNU>();
            TMHRMNUs = new HashSet<TMHRMNU>();
            TRDRSUBs = new HashSet<TRDRSUB>();
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string NAME { get; set; }
        public string FULLNAME { get; set; }
        public DateTime D1 { get; set; }
        public DateTime? D2 { get; set; }

        public virtual ICollection<T1050> T1050s { get; set; }
        public virtual ICollection<T1051> T1051s { get; set; }
        public virtual ICollection<T129> T129s { get; set; }
        public virtual ICollection<T33> T33s { get; set; }
        public virtual ICollection<T5152> T5152s { get; set; }
        public virtual ICollection<TEDO> TEDOs { get; set; }
        public virtual ICollection<TKVMNU> TKVMNUs { get; set; }
        public virtual ICollection<TMHRMNU> TMHRMNUs { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
