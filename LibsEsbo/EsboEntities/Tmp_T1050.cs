﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class Tmp_T1050
    {
        public int R_ID { get; set; }
        public int TIP { get; set; }
        public int NP { get; set; }
        public int LIB_ID { get; set; }
        public int ST_ID { get; set; }
        public int? EDOMX_ID { get; set; }
        public DateTime? V1051A { get; set; }
        public string V1051B { get; set; }
        public string UN { get; set; }
        public DateTime TS { get; set; }
    }
}
