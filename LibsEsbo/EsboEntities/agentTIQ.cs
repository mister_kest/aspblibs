﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentTIQ
    {
        public string ID32 { get; set; }
        public int TYPE { get; set; }
        public string V12005 { get; set; }
        public int MFN { get; set; }
        public int ID { get; set; }
        public DateTime TS { get; set; }
        public int? V10001 { get; set; }
        public int? V10002 { get; set; }
        public int? V10003 { get; set; }
        public int? P1 { get; set; }
        public int? P2 { get; set; }

        public virtual agentREC_TYPE TYPENavigation { get; set; }
    }
}
