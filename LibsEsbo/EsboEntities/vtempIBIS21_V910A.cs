﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class vtempIBIS21_V910A
    {
        public string STT { get; set; }
        public int? K { get; set; }
    }
}
