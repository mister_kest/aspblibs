﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TAGENT_WRITE_LOG
    {
        public int LIB_ID { get; set; }
        public string V40G { get; set; }
        public string V903 { get; set; }
        public int MFN { get; set; }
        public int VERS { get; set; }
        public string V10000 { get; set; }
        public bool? S { get; set; }
        public DateTime TS { get; set; }
    }
}
