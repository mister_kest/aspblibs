﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentTEKZ_NEWBOOK
    {
        public string V40G { get; set; }
        public string V903 { get; set; }
        public int MFN { get; set; }
        public DateTime TS { get; set; }
        public int UPD { get; set; }
        public int IVERS { get; set; }
        public int TIP { get; set; }
    }
}
