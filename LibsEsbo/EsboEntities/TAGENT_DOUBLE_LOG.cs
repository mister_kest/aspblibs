﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TAGENT_DOUBLE_LOG
    {
        public int LIB_ID { get; set; }
        public string V40G { get; set; }
        public string V903 { get; set; }
        public int K1 { get; set; }
        public int K2 { get; set; }
        public DateTime TS { get; set; }
    }
}
