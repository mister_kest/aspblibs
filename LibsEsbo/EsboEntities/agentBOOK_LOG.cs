﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentBOOK_LOG
    {
        public int AGENTID { get; set; }
        public string V40G { get; set; }
        public string V903 { get; set; }
        public int? MFN { get; set; }
        public int? VERS { get; set; }
        public string S { get; set; }
        public DateTime TS { get; set; }
    }
}
