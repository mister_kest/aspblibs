﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class RZN_MNU_SIG
    {
        public int LIB_ID { get; set; }
        public string RZN { get; set; }
        public string Razdel { get; set; }
        public bool? ACT { get; set; }
        public string UN { get; set; }
        public DateTime TS { get; set; }
    }
}
