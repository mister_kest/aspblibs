﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentTMP_DB
    {
        public string LIB { get; set; }
        public string DATABASE { get; set; }
        public int K { get; set; }
    }
}
