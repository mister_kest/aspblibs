﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TKVMNU
    {
        public TKVMNU()
        {
            TOPERs = new HashSet<TOPER>();
            TRDRSUBs = new HashSet<TRDRSUB>();
        }

        public int ID { get; set; }
        public int LIB_ID { get; set; }
        public string V40V { get; set; }
        public string DESCR { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public bool? ACT { get; set; }

        public virtual TLIB LIB { get; set; }
        public virtual ICollection<TOPER> TOPERs { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
    }
}
