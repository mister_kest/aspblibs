﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TPROLE
    {
        public TPROLE()
        {
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string PROLE { get; set; }
        public DateTime D1 { get; set; }
        public DateTime? D2 { get; set; }

        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
