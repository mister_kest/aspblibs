﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentREC_TYPE
    {
        public agentREC_TYPE()
        {
            agentTIQs = new HashSet<agentTIQ>();
        }

        public int TYPE { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<agentTIQ> agentTIQs { get; set; }
    }
}
