﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TCLIENTGROUP
    {
        public TCLIENTGROUP()
        {
            TCLIENT_Ms = new HashSet<TCLIENT_M>();
        }

        public int ID { get; set; }
        public int LIB_ID { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TCLIENT_M> TCLIENT_Ms { get; set; }
    }
}
