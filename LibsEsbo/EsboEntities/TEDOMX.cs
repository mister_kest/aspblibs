﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TEDOMX
    {
        public TEDOMX()
        {
            T1050s = new HashSet<T1050>();
            TEDOs = new HashSet<TEDO>();
        }

        public int ID { get; set; }
        public int LIB_ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string V1052D { get; set; }
        public string NAIM { get; set; }
        public string ADDR { get; set; }
        public string PHONE { get; set; }
        public bool? ACT { get; set; }

        public virtual ICollection<T1050> T1050s { get; set; }
        public virtual ICollection<TEDO> TEDOs { get; set; }
    }
}
