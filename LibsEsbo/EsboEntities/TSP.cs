﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TSP
    {
        public TSP()
        {
            TSPERRCODEs = new HashSet<TSPERRCODE>();
        }

        public int SP { get; set; }
        public string NAIM { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TSPERRCODE> TSPERRCODEs { get; set; }
    }
}
