﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentTEKZ
    {
        public int ID0 { get; set; }
        public int BOOK_ID { get; set; }
        public string V910B { get; set; }
        public string V910H { get; set; }
        public string V910D { get; set; }
        public string V910C { get; set; }
        public decimal V910E { get; set; }
        public string V910U { get; set; }
        public string V910F { get; set; }
        public int V910A { get; set; }
        public int ST_ID { get; set; }
        public string V910P { get; set; }
        public string V910I { get; set; }
        public string V910V { get; set; }
        public string V910W { get; set; }
        public string V910R { get; set; }
        public string V910M { get; set; }
        public string V910Y { get; set; }
        public DateTime? V910S { get; set; }
        public byte? EAS { get; set; }
        public byte? REPSTATUS { get; set; }
        public string V910VSKL { get; set; }
        public string BBK { get; set; }
        public string V210D { get; set; }
        public int IDE { get; set; }
    }
}
