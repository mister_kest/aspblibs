﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TVISIT
    {
        public int ID { get; set; }
        public string V40C { get; set; }
    }
}
