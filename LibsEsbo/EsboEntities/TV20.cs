﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TV20
    {
        public TV20()
        {
            TOPERs = new HashSet<TOPER>();
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string V20 { get; set; }
        public string DESCR { get; set; }
        public DateTime D1 { get; set; }
        public DateTime? D2 { get; set; }

        public virtual ICollection<TOPER> TOPERs { get; set; }
        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
