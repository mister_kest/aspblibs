﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class agentBOOK_ERR
    {
        public int IDER { get; set; }
        public int? SIG { get; set; }
        public string V40G { get; set; }
        public string V903 { get; set; }
        public int? MFN { get; set; }
        public string V910 { get; set; }
        public int? SP { get; set; }
        public int? ERRCODE { get; set; }
        public string ERRMESS { get; set; }
        public string UN { get; set; }
        public DateTime TS { get; set; }
    }
}
