﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TCLIENT_M
    {
        public string LOG { get; set; }
        public string PWD { get; set; }
        public int LIB_ID { get; set; }
        public bool ADM { get; set; }
        public int? V40V_ID { get; set; }
        public int? GROUP_ID { get; set; }
        public bool RDRL { get; set; }

        public virtual TCLIENTGROUP GROUP { get; set; }
    }
}
