﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TRDR
    {
        public TRDR()
        {
            T1050s = new HashSet<T1050>();
            T1051s = new HashSet<T1051>();
            T129s = new HashSet<T129>();
            T25s = new HashSet<T25>();
            T30s = new HashSet<T30>();
            T33s = new HashSet<T33>();
            T40s = new HashSet<T40>();
            T5152s = new HashSet<T5152>();
            TEDOs = new HashSet<TEDO>();
            TOPERs = new HashSet<TOPER>();
            TRDRSUBs = new HashSet<TRDRSUB>();
        }

        public int ID { get; set; }
        public string ID32 { get; set; }
        public int RTYPE_ID { get; set; }
        public int V26_ID { get; set; }
        public int V20_ID { get; set; }
        public int V50_ID { get; set; }
        public int? PR_ID { get; set; }
        public int LIB_ID { get; set; }
        public int ST_ID { get; set; }
        public int? FAM_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string V11 { get; set; }
        public string V12 { get; set; }
        public string V32 { get; set; }
        public int? V21 { get; set; }
        public string V17 { get; set; }
        public string V18 { get; set; }
        public string V23 { get; set; }
        public DateTime? V34 { get; set; }
        public string V19A { get; set; }
        public string V16 { get; set; }
        public bool V45 { get; set; }
        public bool V41 { get; set; }
        public string PI { get; set; }
        public string PO { get; set; }
        public string PN { get; set; }
        public string V12814 { get; set; }
        public bool? V12814V { get; set; }
        public string V300 { get; set; }
        public bool v301 { get; set; }
        public DateTime? v12004 { get; set; }
        public int V100 { get; set; }
        public int? V101 { get; set; }
        public int MFN { get; set; }
        public DateTime? V12001 { get; set; }
        public string V12003 { get; set; }
        public int V10001 { get; set; }
        public int V10002 { get; set; }
        public int V10003 { get; set; }
        public DateTime? V31A { get; set; }
        public string V31B { get; set; }
        public bool SQLUPD { get; set; }
        public string V12005 { get; set; }
        public string S1 { get; set; }
        public string S2 { get; set; }
        public string S3 { get; set; }
        public string S4 { get; set; }
        public string S5 { get; set; }
        public string S6 { get; set; }
        public string KSUNIQ { get; set; }

        public virtual TFAM FAM { get; set; }
        public virtual TLIB LIB { get; set; }
        public virtual TPROLE PR { get; set; }
        public virtual TRTYPE RTYPE { get; set; }
        public virtual TSTATUS ST { get; set; }
        public virtual TV101 V101Navigation { get; set; }
        public virtual TV20 V20 { get; set; }
        public virtual TV26 V26 { get; set; }
        public virtual TV50 V50 { get; set; }
        public virtual ICollection<T1050> T1050s { get; set; }
        public virtual ICollection<T1051> T1051s { get; set; }
        public virtual ICollection<T129> T129s { get; set; }
        public virtual ICollection<T25> T25s { get; set; }
        public virtual ICollection<T30> T30s { get; set; }
        public virtual ICollection<T33> T33s { get; set; }
        public virtual ICollection<T40> T40s { get; set; }
        public virtual ICollection<T5152> T5152s { get; set; }
        public virtual ICollection<TEDO> TEDOs { get; set; }
        public virtual ICollection<TOPER> TOPERs { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
    }
}
