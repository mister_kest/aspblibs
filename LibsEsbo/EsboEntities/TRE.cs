﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TRE
    {
        public TRE()
        {
            TRESSUBs = new HashSet<TRESSUB>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }

        public virtual ICollection<TRESSUB> TRESSUBs { get; set; }
    }
}
