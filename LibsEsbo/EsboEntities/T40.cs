﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class T40
    {
        public int ID { get; set; }
        public string V40Z { get; set; }
        public byte[] MD5CACHE { get; set; }
        public bool DELSTATUS { get; set; }
        public string V40G { get; set; }
        public string V40A { get; set; }
        public string V40B { get; set; }
        public string V40H { get; set; }
        public string V40K { get; set; }
        public DateTime? V40D { get; set; }
        public string V40V { get; set; }
        public string V40R { get; set; }
        public DateTime? V40E { get; set; }
        public DateTime? V40F { get; set; }
        public DateTime? V40L { get; set; }
        public string V40C { get; set; }
        public string V40I { get; set; }
        public TimeSpan? V401 { get; set; }
        public TimeSpan? V402 { get; set; }
        public DateTime? TS { get; set; }
        public bool UPD { get; set; }

        public virtual TRDR IDNavigation { get; set; }
    }
}
