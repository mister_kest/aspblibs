﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TOPER
    {
        public TOPER()
        {
            TRDRSUBs = new HashSet<TRDRSUB>();
        }

        public int ID { get; set; }
        public int TYPE_ID { get; set; }
        public int? BOOK_ID { get; set; }
        public int LIB_ID { get; set; }
        public int? R_ID { get; set; }
        public int? EKZ_ID { get; set; }
        public int? V40V_ID { get; set; }
        public int V26_ID { get; set; }
        public int V20_ID { get; set; }
        public int? VIS_ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime V40D { get; set; }
        public int RAGE { get; set; }
        public string BV40G { get; set; }
        public string BV40A { get; set; }
        public string BV40RZN { get; set; }
        public string BV40HAR { get; set; }
        public string BV40VID { get; set; }
        public string BV900T { get; set; }
        public string V40C { get; set; }
        public string EV40B { get; set; }
        public DateTime? EV40E { get; set; }
        public string EV40H { get; set; }
        public string EV40K { get; set; }
        public byte EAS { get; set; }
        public string V40Z { get; set; }
        public string V40I { get; set; }
        public DateTime T0 { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }

        public virtual TEKZ EKZ { get; set; }
        public virtual TRDR R_IDNavigation { get; set; }
        public virtual TVISITSUB TVISITSUB { get; set; }
        public virtual TOPERTYPE TYPE { get; set; }
        public virtual TV20 V20 { get; set; }
        public virtual TV26 V26 { get; set; }
        public virtual TKVMNU V40V { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
    }
}
