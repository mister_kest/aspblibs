﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TSPERRCODE
    {
        public int SP { get; set; }
        public int ERRCODE { get; set; }
        public string DESCR { get; set; }
        public string DESCRFULL { get; set; }

        public virtual TSP SPNavigation { get; set; }
    }
}
