﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TEDO_TIP
    {
        public TEDO_TIP()
        {
            TEDOs = new HashSet<TEDO>();
        }

        public int ID { get; set; }
        public string TIP { get; set; }

        public virtual ICollection<TEDO> TEDOs { get; set; }
    }
}
