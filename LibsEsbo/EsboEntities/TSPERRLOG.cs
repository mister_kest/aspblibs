﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TSPERRLOG
    {
        public int ID { get; set; }
        public string ID32 { get; set; }
        public int ERRCODE { get; set; }
        public string DESCR { get; set; }
        public int R_ID { get; set; }
        public int BOOK_ID { get; set; }
        public int EKZ_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
    }
}
