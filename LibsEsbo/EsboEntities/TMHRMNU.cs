﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TMHRMNU
    {
        public int ID { get; set; }
        public int LIB_ID { get; set; }
        public string V910D { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public bool? ACT { get; set; }

        public virtual TLIB LIB { get; set; }
    }
}
