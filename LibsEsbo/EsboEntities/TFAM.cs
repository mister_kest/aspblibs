﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TFAM
    {
        public TFAM()
        {
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public string NAIM { get; set; }

        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
