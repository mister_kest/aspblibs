﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TRTYPE
    {
        public TRTYPE()
        {
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string RTYPE { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
