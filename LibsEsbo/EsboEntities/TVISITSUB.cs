﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TVISITSUB
    {
        public TVISITSUB()
        {
            TOPERs = new HashSet<TOPER>();
        }

        public int LIB_ID { get; set; }
        public int VIS_ID { get; set; }
        public string V40C { get; set; }

        public virtual ICollection<TOPER> TOPERs { get; set; }
    }
}
