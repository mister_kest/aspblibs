﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TEKZSTATUS
    {
        public TEKZSTATUS()
        {
            TEKZs = new HashSet<TEKZ>();
            TRESSUBs = new HashSet<TRESSUB>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string STT { get; set; }
        public string STATUS { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TEKZ> TEKZs { get; set; }
        public virtual ICollection<TRESSUB> TRESSUBs { get; set; }
    }
}
