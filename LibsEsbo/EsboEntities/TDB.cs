﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TDB
    {
        public string V40G { get; set; }
        public bool ACT { get; set; }
        public bool ACTPORTAL { get; set; }
        public int? LIB_ID { get; set; }
        public int? MASK { get; set; }
        public int? NREC { get; set; }
        public int? ALLREC { get; set; }
        public int? LASTMFN { get; set; }
        public string DESCR { get; set; }
    }
}
