﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class T5152
    {
        public int R_ID { get; set; }
        public int TIP { get; set; }
        public int ST_ID { get; set; }
        public int NP { get; set; }
        public int LIB_ID { get; set; }
        public DateTime A { get; set; }
        public int? C { get; set; }
        public string D { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string MD5CACHE { get; set; }

        public virtual TLIB LIB { get; set; }
        public virtual TRDR R_IDNavigation { get; set; }
    }
}
