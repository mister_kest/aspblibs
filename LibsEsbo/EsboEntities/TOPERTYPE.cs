﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TOPERTYPE
    {
        public TOPERTYPE()
        {
            TOPERs = new HashSet<TOPER>();
            TRDRSUBs = new HashSet<TRDRSUB>();
        }

        public int ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public string OPERNAME { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TOPER> TOPERs { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
    }
}
