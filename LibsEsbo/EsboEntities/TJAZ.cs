﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TJAZ
    {
        public TJAZ()
        {
            T25s = new HashSet<T25>();
        }

        public int ID { get; set; }
        public string V25 { get; set; }
        public string NAIM { get; set; }

        public virtual ICollection<T25> T25s { get; set; }
    }
}
