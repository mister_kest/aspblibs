﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TV101
    {
        public TV101()
        {
            TRDRs = new HashSet<TRDR>();
        }

        public int ID { get; set; }
        public string DOCTYPE { get; set; }
        public string DESCR { get; set; }

        public virtual ICollection<TRDR> TRDRs { get; set; }
    }
}
