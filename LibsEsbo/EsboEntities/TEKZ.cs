﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TEKZ
    {
        public TEKZ()
        {
            TOPERs = new HashSet<TOPER>();
            TRDRSUBs = new HashSet<TRDRSUB>();
        }

        public int ID { get; set; }
        public int BOOK_ID { get; set; }
        public string V910B { get; set; }
        public string V910H { get; set; }
        public string V910D { get; set; }
        public DateTime? V910C { get; set; }
        public decimal V910E { get; set; }
        public string V910U { get; set; }
        public string V910F { get; set; }
        public int ST_ID { get; set; }
        public int V910A { get; set; }
        public string V910P { get; set; }
        public string V910I { get; set; }
        public string V910V { get; set; }
        public string V910W { get; set; }
        public string V910R { get; set; }
        public string V910M { get; set; }
        public string V910Y { get; set; }
        public DateTime? V910S { get; set; }
        public byte EAS { get; set; }
        public byte REPSTATUS { get; set; }
        public byte UPD { get; set; }
        public bool SQLUPD { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public int LIB_ID { get; set; }
        public string V910VSKL { get; set; }
        public int IDA { get; set; }
        public string BBK { get; set; }
        public string AZ { get; set; }
        public byte[] MD5EKZ { get; set; }

        public virtual TBOOK BOOK { get; set; }
        public virtual TEKZSTATUS V910ANavigation { get; set; }
        public virtual ICollection<TOPER> TOPERs { get; set; }
        public virtual ICollection<TRDRSUB> TRDRSUBs { get; set; }
    }
}
