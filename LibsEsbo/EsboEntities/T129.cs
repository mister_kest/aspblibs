﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class T129
    {
        public int R_ID { get; set; }
        public int ST_ID { get; set; }
        public int NP { get; set; }
        public int LIB_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }
        public DateTime V129A { get; set; }
        public string V129B { get; set; }
        public decimal V129C { get; set; }
        public string V129E { get; set; }
        public string V129F { get; set; }
        public string MD5CACHE { get; set; }

        public virtual TLIB LIB { get; set; }
        public virtual TRDR R_IDNavigation { get; set; }
    }
}
