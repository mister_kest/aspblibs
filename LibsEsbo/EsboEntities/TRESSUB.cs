﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LibsEsbo.EsboEntities
{
    public partial class TRESSUB
    {
        public int ID { get; set; }
        public int RES_ID { get; set; }
        public int STATUS_ID { get; set; }
        public int ST_ID { get; set; }
        public DateTime TS { get; set; }
        public string UN { get; set; }

        public virtual TRE RES { get; set; }
        public virtual TEKZSTATUS STATUS { get; set; }
    }
}
