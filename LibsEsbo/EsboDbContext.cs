﻿using System;
using LibsEsbo.EsboEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
 

namespace LibsEsbo
{
    public partial class EsboDbContext : DbContext
    {
        public readonly static string DefaultConnectionString =
            "Server=stat.libs.spb.ru;User ID=SA;Password=Bm_2019_SQL;Database=ESBO02;MultipleActiveResultSets=True";

        public EsboDbContext()
        {
        }

        public EsboDbContext(DbContextOptions<EsboDbContext> options): base(options)
        {
        }

        public virtual DbSet<RZN_MNU_SIG> RZN_MNU_SIGs { get; set; }
        public virtual DbSet<T1050> T1050s { get; set; }
        public virtual DbSet<T1051> T1051s { get; set; }
        public virtual DbSet<T129> T129s { get; set; }
        public virtual DbSet<T25> T25s { get; set; }
        public virtual DbSet<T30> T30s { get; set; }
        public virtual DbSet<T33> T33s { get; set; }
        public virtual DbSet<T40> T40s { get; set; }
        public virtual DbSet<T5152> T5152s { get; set; }
        public virtual DbSet<TAGENT_DOUBLE_LOG> TAGENT_DOUBLE_LOGs { get; set; }
        public virtual DbSet<TAGENT_WRITE_LOG> TAGENT_WRITE_LOGs { get; set; }
        public virtual DbSet<TBOOK> TBOOKs { get; set; }
        public virtual DbSet<TCLIENTGROUP> TCLIENTGROUPs { get; set; }
        public virtual DbSet<TCLIENT_M> TCLIENT_Ms { get; set; }
        public virtual DbSet<TDB> TDBs { get; set; }
        public virtual DbSet<TEDO> TEDOs { get; set; }
        public virtual DbSet<TEDOMX> TEDOMXes { get; set; }
        public virtual DbSet<TEDO_TIP> TEDO_TIPs { get; set; }
        public virtual DbSet<TEKZ> TEKZs { get; set; }
        public virtual DbSet<TEKZSTATUS> TEKZSTATUSes { get; set; }
        public virtual DbSet<TFAM> TFAMs { get; set; }
        public virtual DbSet<TJAZ> TJAZs { get; set; }
        public virtual DbSet<TKVMNU> TKVMNUs { get; set; }
        public virtual DbSet<TLIB> TLIBs { get; set; }
        public virtual DbSet<TMHRMNU> TMHRMNUs { get; set; }
        public virtual DbSet<TOPER> TOPERs { get; set; }
        public virtual DbSet<TOPERTYPE> TOPERTYPEs { get; set; }
        public virtual DbSet<TPROLE> TPROLEs { get; set; }
        public virtual DbSet<TRDR> TRDRs { get; set; }
        public virtual DbSet<TRDRSUB> TRDRSUBs { get; set; }
        public virtual DbSet<TRE> TREs { get; set; }
        public virtual DbSet<TRESSUB> TRESSUBs { get; set; }
        public virtual DbSet<TRTYPE> TRTYPEs { get; set; }
        public virtual DbSet<TSP> TSP { get; set; }
        public virtual DbSet<TSPERRCODE> TSPERRCODEs { get; set; }
        public virtual DbSet<TSPERRLOG> TSPERRLOGs { get; set; }
        public virtual DbSet<TSTATUS> TSTATUSes { get; set; }
        public virtual DbSet<TV101> TV101s { get; set; }
        public virtual DbSet<TV20> TV20s { get; set; }
        public virtual DbSet<TV26> TV26s { get; set; }
        public virtual DbSet<TV50> TV50s { get; set; }
        public virtual DbSet<TVISIT> TVISITs { get; set; }
        public virtual DbSet<TVISITSUB> TVISITSUBs { get; set; }
        public virtual DbSet<Tmp_T1050> Tmp_T1050s { get; set; }
        public virtual DbSet<agentBOOK_ERR> agentBOOK_ERRs { get; set; }
        public virtual DbSet<agentBOOK_LOG> agentBOOK_LOGs { get; set; }
        public virtual DbSet<agentREC_TYPE> agentREC_TYPEs { get; set; }
        public virtual DbSet<agentTEKZ> agentTEKZs { get; set; }
        public virtual DbSet<agentTEKZ_NEWBOOK> agentTEKZ_NEWBOOKs { get; set; }
        public virtual DbSet<agentTIQ> agentTIQs { get; set; }
        public virtual DbSet<agentTMP_DB> agentTMP_DBs { get; set; }
        public virtual DbSet<tmpBOOK> tmpBOOKs { get; set; }
        public virtual DbSet<vtempIBIS21_V910A> vtempIBIS21_V910As { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(DefaultConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RZN_MNU_SIG>(entity =>
            {
                entity.HasKey(e => new { e.LIB_ID, e.RZN })
                    .HasName("PK__RZN_MNU_SIG__0F38F91D");

                entity.ToTable("RZN_MNU_SIG");

                entity.Property(e => e.RZN)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ACT)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("Элемент справочника актуален");

                entity.Property(e => e.Razdel)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())");
            });

            modelBuilder.Entity<T1050>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.TIP, e.ST_ID, e.NP })
                    .HasName("PK_T1050_1");

                entity.ToTable("T1050");

                entity.Property(e => e.TIP)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Тип документа 0 -старый договор, 1 -ЕДО, 2-согласие, 11-аннулир. ЕДО, 12-аннул. согласие, 10 -аннулир. обычный дог.");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.EDOMX_ID).HasComment("Место хранения договора");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V1051A)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Датат договора");

                entity.Property(e => e.V1051B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер договора");

                entity.HasOne(d => d.EDOMX)
                    .WithMany(p => p.T1050s)
                    .HasForeignKey(d => d.EDOMX_ID)
                    .HasConstraintName("FK_T1050_TEDOMX");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.T1050s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T1050_TLIB");

                entity.HasOne(d => d.LIBNavigation)
                    .WithMany(p => p.T1050s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T1050_TRDR");

                entity.HasOne(d => d.ST)
                    .WithMany(p => p.T1050s)
                    .HasForeignKey(d => d.ST_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T1050_TSTATUS");
            });

            modelBuilder.Entity<T1051>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.NP, e.ST_ID })
                    .HasName("PK_T1051_1");

                entity.ToTable("T1051");

                entity.HasIndex(e => e.R_ID, "IX_T1051");

                entity.HasIndex(e => e.LIB_ID, "IX_T1051_1");

                entity.HasIndex(e => e.V1051B, "IX_T1051_2");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.LIB_ID).HasComment("Записан в библиотеку");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.V1051A)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата заключения договора");

                entity.Property(e => e.V1051B)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер договора");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.T1051s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T1051_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T1051s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T1051_TRDR");
            });

            modelBuilder.Entity<T129>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.ST_ID, e.NP })
                    .HasName("PK_T129_1");

                entity.ToTable("T129");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V129A)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, с которой ограничивается право пользования");

                entity.Property(e => e.V129B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("Причина ограничения права пользования");

                entity.Property(e => e.V129C)
                    .HasColumnType("smallmoney")
                    .HasComment("Сумма, количество");

                entity.Property(e => e.V129E)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Ответственный");

                entity.Property(e => e.V129F)
                    .HasMaxLength(255)
                    .HasComment("Примечание");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.T129s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T129_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T129s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T129_TRDR");
            });

            modelBuilder.Entity<T25>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.ST_ID, e.NP })
                    .HasName("PK_T25_1");

                entity.ToTable("T25");

                entity.HasIndex(e => new { e.R_ID, e.ST_ID, e.JAZ_ID }, "IX_T25")
                    .IsUnique();

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.HasOne(d => d.JAZ)
                    .WithMany(p => p.T25s)
                    .HasForeignKey(d => d.JAZ_ID)
                    .HasConstraintName("FK_T25_TJAZ");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T25s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T25_TRDR");
            });

            modelBuilder.Entity<T30>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.TIP, e.ST_ID, e.NP })
                    .HasName("PK__T30");

                entity.ToTable("T30");

                entity.Property(e => e.TIP).HasComment("0-действующий номер, 1- утерянный");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V24)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Короткий номер ЧБ");

                entity.Property(e => e.V30)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Идентификатор читателя");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T30s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T30_TRDR");
            });

            modelBuilder.Entity<T33>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.ST_ID, e.NP });

                entity.ToTable("T33");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V33D)
                    .HasColumnType("date")
                    .HasComment("Дата, время создания");

                entity.Property(e => e.V33E)
                    .HasMaxLength(50)
                    .HasComment("Логин создателя примечания");

                entity.Property(e => e.V33С)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasDefaultValueSql("('')")
                    .HasComment("Текст примечания");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.T33s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T33_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T33s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T33_TRDR");
            });

            modelBuilder.Entity<T40>(entity =>
            {
                entity.HasKey(e => new { e.ID, e.V40Z });

                entity.ToTable("T40");

                entity.Property(e => e.V40Z)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.DELSTATUS).HasComment("Пометка об удалении: 1-удалить, 0-нормальная запись");

                entity.Property(e => e.MD5CACHE)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UPD).HasComment("Отметка об изменении со стороны SQL-сервера");

                entity.Property(e => e.V401).HasComment("Время выдачи/посещения ЧЧ:ММ:СС");

                entity.Property(e => e.V402).HasComment("Время возврата ЧЧ:ММ:СС");

                entity.Property(e => e.V40A)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40B)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40C)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40D).HasColumnType("date");

                entity.Property(e => e.V40E).HasColumnType("date");

                entity.Property(e => e.V40F).HasColumnType("date");

                entity.Property(e => e.V40G)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40H)
                    .HasMaxLength(24)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('''')");

                entity.Property(e => e.V40I)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40K)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40L).HasColumnType("date");

                entity.Property(e => e.V40R)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V40V)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')");

                entity.HasOne(d => d.IDNavigation)
                    .WithMany(p => p.T40s)
                    .HasForeignKey(d => d.ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T40_TRDR");
            });

            modelBuilder.Entity<T5152>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.TIP, e.ST_ID, e.NP })
                    .HasName("PK__T51");

                entity.ToTable("T5152");

                entity.Property(e => e.TIP).HasComment("Тип записи 51- запись в б-ку, 52 -перерегистрация");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.A)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата записи");

                entity.Property(e => e.C).HasComment("Отдел");

                entity.Property(e => e.D)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.T5152s)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T5152_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.T5152s)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T5152_TRDR");
            });

            modelBuilder.Entity<TAGENT_DOUBLE_LOG>(entity =>
            {
                entity.HasKey(e => new { e.LIB_ID, e.V40G, e.V903, e.K1 });

                entity.ToTable("TAGENT_DOUBLE_LOG");

                entity.Property(e => e.V40G)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.V903)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TAGENT_WRITE_LOG>(entity =>
            {
                entity.HasKey(e => new { e.LIB_ID, e.V40G, e.V903, e.MFN, e.VERS });

                entity.ToTable("TAGENT_WRITE_LOG");

                entity.HasIndex(e => new { e.V40G, e.V903 }, "IX_TAGENT_WRITE_LOG");

                entity.HasIndex(e => e.TS, "IX_TAGENT_WRITE_LOG_1");

                entity.HasIndex(e => e.V10000, "IX_TAGENT_WRITE_LOG_2");

                entity.HasIndex(e => e.S, "IX_TAGENT_WRITE_LOG_3");

                entity.Property(e => e.V40G)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.V903)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.S)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("Успешна запись поля 10000");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.V10000)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TBOOK>(entity =>
            {
                entity.ToTable("TBOOK");

                entity.HasIndex(e => e.V920, "IX_TBOOK");

                entity.HasIndex(e => new { e.V40G, e.MFN }, "IX_TBOOK_1");

                entity.HasIndex(e => e.REPSTATUS, "IX_TBOOK_2");

                entity.HasIndex(e => new { e.JYEAR, e.JVOLUME, e.JNUM }, "IX_TBOOK_3");

                entity.HasIndex(e => new { e.V40G, e.V903 }, "IX_TBOOK_4")
                    .IsUnique();

                entity.HasIndex(e => e.AZ, "IX_TBOOK_AZ");

                entity.HasIndex(e => e.BBK, "IX_TBOOK_BBK");

                entity.HasIndex(e => e.V110B, "IX_TBOOK_V110B");

                entity.HasIndex(e => e.V933, "IX_TBOOK_V933");

                entity.Property(e => e.ID).HasComment("Ключ записи, md5 от V40G,V903");

                entity.Property(e => e.AZ)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Авторский знак");

                entity.Property(e => e.BBK)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IVERS)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Версия записи ИРБИС");

                entity.Property(e => e.JNUM)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер");

                entity.Property(e => e.JVOLUME)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Том");

                entity.Property(e => e.JYEAR).HasComment("Год");

                entity.Property(e => e.MFN).HasComment("MFN записи (временно, на период загрузки)");

                entity.Property(e => e.REPSTATUS).HasComment("Статус документа после репликации 0 -ОК, 1-требуются данные с издателя ИРБИС");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V10D)
                    .HasColumnType("money")
                    .HasComment("Цена экземпляра");

                entity.Property(e => e.V110B)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasComment("ВИд периодического издания");

                entity.Property(e => e.V210D)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Год издания");

                entity.Property(e => e.V333)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Возрастной ценз");

                entity.Property(e => e.V40C)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasDefaultValueSql("('')")
                    .HasComment("Краткое описание");

                entity.Property(e => e.V40G)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Имя БД ЭК");

                entity.Property(e => e.V40HAR)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Характер документа");

                entity.Property(e => e.V40RZN)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Раздел знаний");

                entity.Property(e => e.V40VID)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Вид документа");

                entity.Property(e => e.V900T)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true)
                    .HasComment("Тип документа");

                entity.Property(e => e.V903)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Шифр");

                entity.Property(e => e.V920)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V933)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Шифр сводного описания для периодики");
            });

            modelBuilder.Entity<TCLIENTGROUP>(entity =>
            {
                entity.ToTable("TCLIENTGROUP");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<TCLIENT_M>(entity =>
            {
                entity.HasKey(e => e.LOG)
                    .HasName("PK_CLIENT_M");

                entity.ToTable("TCLIENT_M");

                entity.Property(e => e.LOG)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PWD)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GROUP)
                    .WithMany(p => p.TCLIENT_Ms)
                    .HasForeignKey(d => d.GROUP_ID)
                    .HasConstraintName("FK_CLIENT_M_CLIENT_M");
            });

            modelBuilder.Entity<TDB>(entity =>
            {
                entity.HasKey(e => e.V40G);

                entity.ToTable("TDB");

                entity.Property(e => e.V40G)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ACT).HasComment("Флаг актиуальности использования БД");

                entity.Property(e => e.ALLREC).HasDefaultValueSql("((0))");

                entity.Property(e => e.DESCR)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.LASTMFN).HasDefaultValueSql("((0))");

                entity.Property(e => e.MASK)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Фактически это некий условный номер, который позволяет делить БД одной библиотечной системы на группы, для равномерной нагрузки агентов репликации.  Значение 0 соответсвует выбору всех БД.");

                entity.Property(e => e.NREC)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Кол-во измененных записей при последнем запуске агента репликации. Используется для сортировки списка БД так, чтобы базы с более интенсивнам изменением обрабатывались первыми.");
            });

            modelBuilder.Entity<TEDO>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.TIP_ID, e.ST_ID, e.NP });

                entity.ToTable("TEDO");

                entity.HasIndex(e => e.R_ID, "IX_T10521053");

                entity.HasIndex(e => e.LIB_ID, "IX_T10521053_1");

                entity.HasIndex(e => e.EDOMX_ID, "IX_T10521053_2");

                entity.HasIndex(e => e.V1052B, "IX_T10521053_3");

                entity.HasIndex(e => e.V1052A, "IX_T10521053_4");

                entity.HasIndex(e => e.TIP_ID, "IX_TEDO");

                entity.Property(e => e.TIP_ID).HasComment("1-ЕДО, 2 - согласие на обработку ПД, 3- аннулированные ЕДО");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.ANNDT).HasColumnType("datetime");

                entity.Property(e => e.ANNFIO)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ANNWHY)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.EDOMX_ID).HasComment("Место хранения договора");

                entity.Property(e => e.LIB_ID).HasComment("Записан в библиотеку");

                entity.Property(e => e.MD5CACHE)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Равно md5 от строки из всех повторений поля с заданным типом st_id и пишется в запись с NP=0");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V1052A)
                    .HasColumnType("date")
                    .HasComment("Дата заключения ЕДО");

                entity.Property(e => e.V1052B)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер договора");

                entity.Property(e => e.V1052G).HasComment("Нет регистрации в СПб или ЛО");

                entity.Property(e => e.V1052H)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер для СМС");

                entity.Property(e => e.V1052I)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')")
                    .HasComment("e-mail");

                entity.Property(e => e.V1052J).HasComment("СМС- задолженность");

                entity.Property(e => e.V1052K).HasComment("e-mail- задолженность");

                entity.Property(e => e.V1052L).HasComment("Согласие на смс о мероприятиях");

                entity.Property(e => e.V1052M)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер для СМС о мероприятиях");

                entity.Property(e => e.V1052N)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')")
                    .HasComment("e-mail о мероприятиях");

                entity.Property(e => e.V1052O).HasComment("Согласие на e-mail о мероприятиях");

                entity.Property(e => e.V1052P)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер городского телефона");

                entity.Property(e => e.V1052Q).HasComment("Согласие на звонки по номеру городского тлф.");

                entity.HasOne(d => d.EDOMX)
                    .WithMany(p => p.TEDOs)
                    .HasForeignKey(d => d.EDOMX_ID)
                    .HasConstraintName("FK_T10521053_TEDOMX");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.TEDOs)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T10521053_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.TEDOs)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T10521053_TRDR");

                entity.HasOne(d => d.TIP)
                    .WithMany(p => p.TEDOs)
                    .HasForeignKey(d => d.TIP_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TEDO_TEDO_TIP");
            });

            modelBuilder.Entity<TEDOMX>(entity =>
            {
                entity.ToTable("TEDOMX");

                entity.HasIndex(e => new { e.LIB_ID, e.V1052D }, "IX_TEDOMX")
                    .IsUnique();

                entity.Property(e => e.ACT)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ADDR)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NAIM)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PHONE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V1052D)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Наименование места хранения ЕДО");
            });

            modelBuilder.Entity<TEDO_TIP>(entity =>
            {
                entity.ToTable("TEDO_TIP");

                entity.HasIndex(e => e.TIP, "IX_TEDO_TIP")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.TIP)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TEKZ>(entity =>
            {
                entity.ToTable("TEKZ");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910H, e.V910B, e.V910D, e.ST_ID }, "IX_TEKZ");

                entity.HasIndex(e => new { e.V910H, e.LIB_ID, e.ST_ID }, "IX_TEKZ_1");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.IDA }, "IX_TEKZ_10");

                entity.HasIndex(e => new { e.BOOK_ID, e.IDA }, "IX_TEKZ_11");

                entity.HasIndex(e => new { e.V910H, e.ST_ID }, "IX_TEKZ_12");

                entity.HasIndex(e => e.V910B, "IX_TEKZ_2");

                entity.HasIndex(e => e.REPSTATUS, "IX_TEKZ_3");

                entity.HasIndex(e => e.UPD, "IX_TEKZ_4");

                entity.HasIndex(e => e.SQLUPD, "IX_TEKZ_5");

                entity.HasIndex(e => e.LIB_ID, "IX_TEKZ_6");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.V910H, e.IDA }, "IX_TEKZ_7");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.V910D, e.IDA }, "IX_TEKZ_8");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910H, e.IDA }, "IX_TEKZ_9");

                entity.HasIndex(e => e.AZ, "IX_TEKZ_AZ");

                entity.HasIndex(e => e.BBK, "IX_TEKZ_BBK");

                entity.HasIndex(e => new { e.BOOK_ID, e.IDA, e.MD5EKZ }, "IX_TEKZ_MD5");

                entity.Property(e => e.AZ).HasMaxLength(20);

                entity.Property(e => e.BBK).HasMaxLength(50);

                entity.Property(e => e.EAS)
                    .HasDefaultValueSql("((2))")
                    .HasComment("Отметка о противокражной подписи (для книговыдачи) 1- установлена (\"звенит\" в  воротах), -1 -снята, 0 - не установлена, 2- состояние не известно.");

                entity.Property(e => e.IDA).HasDefaultValueSql("((1))");

                entity.Property(e => e.MD5EKZ).HasMaxLength(16);

                entity.Property(e => e.REPSTATUS).HasComment("Статус документа после репликации 0 -ОК, 1-требуются данные с издателя ИРБИС");

                entity.Property(e => e.SQLUPD).HasComment("Отметка об обновлении экземпляра в MS SQL.\r\nПо ней формируется UPD.");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна. Пока реально используем только 0 и 1.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.UPD).HasComment("Ключ для агента репликации с MS SQL (он издатель).\r\nФормируется триггером.");

                entity.Property(e => e.V910A).HasComment("Статус экземпляра");

                entity.Property(e => e.V910B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Инвентарный номер");

                entity.Property(e => e.V910C)
                    .HasColumnType("date")
                    .HasComment("Дата ввода");

                entity.Property(e => e.V910D)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Место хранения");

                entity.Property(e => e.V910E)
                    .HasColumnType("money")
                    .HasComment("Цена экземпляра");

                entity.Property(e => e.V910F)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Канал поступления");

                entity.Property(e => e.V910H)
                    .IsRequired()
                    .HasMaxLength(24)
                    .HasDefaultValueSql("('')")
                    .HasComment("Метка/штрих-код");

                entity.Property(e => e.V910I)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Инв. номер \"подшивки\"");

                entity.Property(e => e.V910M)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("МХР-новое");

                entity.Property(e => e.V910P)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Шифр \"подшивки\"");

                entity.Property(e => e.V910R)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Расстановочный шифр");

                entity.Property(e => e.V910S)
                    .HasColumnType("date")
                    .HasComment("Дата проверки фонда");

                entity.Property(e => e.V910U)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер записи КСУ поступления");

                entity.Property(e => e.V910V)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Выбытие-N КСУ");

                entity.Property(e => e.V910VSKL)
                    .HasMaxLength(50)
                    .HasComment("v910^!");

                entity.Property(e => e.V910W)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Передача-N акта");

                entity.Property(e => e.V910Y)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер акта индивидуального учета");

                entity.HasOne(d => d.BOOK)
                    .WithMany(p => p.TEKZs)
                    .HasForeignKey(d => d.BOOK_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TEKZ_TBOOK");

                entity.HasOne(d => d.V910ANavigation)
                    .WithMany(p => p.TEKZs)
                    .HasForeignKey(d => d.V910A)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TEKZ_TEKZSTATUS");
            });

            modelBuilder.Entity<TEKZSTATUS>(entity =>
            {
                entity.ToTable("TEKZSTATUS");

                entity.HasIndex(e => e.DESCR, "IX_TEKZSTATUS")
                    .IsUnique();

                entity.HasIndex(e => e.STT, "IX_TEKZSTATUS_1")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Значение для получения кода по краткому наименованию");

                entity.Property(e => e.STATUS)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.STT)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true)
                    .HasComment("Статус в ИРБИС");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TFAM>(entity =>
            {
                entity.ToTable("TFAM");

                entity.HasIndex(e => e.NAIM, "IX_TFAM")
                    .IsUnique();

                entity.Property(e => e.NAIM)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TJAZ>(entity =>
            {
                entity.ToTable("TJAZ");

                entity.HasIndex(e => e.V25, "IX_TJAZ")
                    .IsUnique();

                entity.HasIndex(e => e.NAIM, "IX_TJAZ_1")
                    .IsUnique();

                entity.Property(e => e.NAIM)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V25)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TKVMNU>(entity =>
            {
                entity.ToTable("TKVMNU");

                entity.HasIndex(e => new { e.LIB_ID, e.V40V }, "IX_TKVMNU")
                    .IsUnique();

                entity.Property(e => e.ACT)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V40V)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.TKVMNUs)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TKVMNU_TLIB");
            });

            modelBuilder.Entity<TLIB>(entity =>
            {
                entity.ToTable("TLIB");

                entity.HasIndex(e => e.NAME, "IX_TLIB")
                    .IsUnique();

                entity.HasIndex(e => e.FULLNAME, "IX_TLIB_1")
                    .IsUnique();

                entity.Property(e => e.ID)
                    .ValueGeneratedNever()
                    .HasComment("Код библиотечной системы от 1 до 21");

                entity.Property(e => e.D1)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата начала действия категории");

                entity.Property(e => e.D2)
                    .HasColumnType("date")
                    .HasComment("Датат окончания действия категории. NULL - для текущих значений.");

                entity.Property(e => e.FULLNAME)
                    .IsRequired()
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasComment("Полное название библиотечной системы");

                entity.Property(e => e.NAME)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Сокращенное название библиотечной системы");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TMHRMNU>(entity =>
            {
                entity.ToTable("TMHRMNU");

                entity.HasIndex(e => new { e.LIB_ID, e.V910D }, "IX_TMHRMNU")
                    .IsUnique();

                entity.Property(e => e.ACT)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("Элемент справочника актуален");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V910D)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.TMHRMNUs)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TMHRMNU_TLIB");
            });

            modelBuilder.Entity<TOPER>(entity =>
            {
                entity.ToTable("TOPERS");

                entity.HasIndex(e => e.BOOK_ID, "IX_TOPERS");

                entity.HasIndex(e => e.EKZ_ID, "IX_TOPERS_1");

                entity.HasIndex(e => new { e.R_ID, e.EKZ_ID }, "IX_TOPERS_10");

                entity.HasIndex(e => e.LIB_ID, "IX_TOPERS_2");

                entity.HasIndex(e => e.R_ID, "IX_TOPERS_3");

                entity.HasIndex(e => e.ST_ID, "IX_TOPERS_4");

                entity.HasIndex(e => e.TYPE_ID, "IX_TOPERS_5");

                entity.HasIndex(e => e.V20_ID, "IX_TOPERS_6");

                entity.HasIndex(e => e.V26_ID, "IX_TOPERS_7");

                entity.HasIndex(e => e.V40V_ID, "IX_TOPERS_8");

                entity.Property(e => e.ID).HasComment("Идентификатор операции (уникальный).");

                entity.Property(e => e.BOOK_ID).HasComment("ID издания");

                entity.Property(e => e.BV40A)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Шифр для экземпляра");

                entity.Property(e => e.BV40G)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Имая БД для экземпляра");

                entity.Property(e => e.BV40HAR)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Характер документа");

                entity.Property(e => e.BV40RZN)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Раздел знаний");

                entity.Property(e => e.BV40VID)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Вид документа");

                entity.Property(e => e.BV900T)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true)
                    .HasComment("Тип документа");

                entity.Property(e => e.EAS).HasComment("Отметка о противокражной подписи (для книговыдачи) 1- установлена (\"звенит\" в  воротах), -1 -снята, 0l - не изменилась либо состояние не известно.");

                entity.Property(e => e.EKZ_ID)
                    .HasDefaultValueSql("((0))")
                    .HasComment("ID экземпляда из таблицы TEKZ");

                entity.Property(e => e.EV40B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Инвентарный номер для экземпляра");

                entity.Property(e => e.EV40E)
                    .HasColumnType("date")
                    .HasComment("Дата предполагаемого возврата для экземпляра");

                entity.Property(e => e.EV40H)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Метка/штрих-код");

                entity.Property(e => e.EV40K)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Место хранения");

                entity.Property(e => e.LIB_ID).HasComment("Код библиотечной системы");

                entity.Property(e => e.RAGE).HasComment("Возраст на момент совершения операции (условная дата рождения 01.07.ГГГГ)");

                entity.Property(e => e.R_ID)
                    .HasDefaultValueSql("((0))")
                    .HasComment("ID пользователя");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна. Всегда совпадает с ST_ID из TRDR.");

                entity.Property(e => e.T0)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время создания записи");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.TYPE_ID).HasComment("ID типа операции");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V20_ID).HasComment("ID социальной группы");

                entity.Property(e => e.V26_ID).HasComment("ID места учебы");

                entity.Property(e => e.V40C)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasDefaultValueSql("('')")
                    .HasComment("Краткое описание (денормализовано)");

                entity.Property(e => e.V40D)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(CONVERT([date],getdate()))")
                    .HasComment("Дата операции");

                entity.Property(e => e.V40I)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Ответственное лицо");

                entity.Property(e => e.V40V_ID)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Место выдачи");

                entity.Property(e => e.V40Z)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("\"Внешний ключ\" для связи поля 40 RDR и текущей БД MS SQL");

                entity.Property(e => e.VIS_ID).HasComment("ID вида посещения");

                entity.HasOne(d => d.EKZ)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.EKZ_ID)
                    .HasConstraintName("FK_TOPERS_TEKZ");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.R_ID)
                    .HasConstraintName("FK_TOPERS_TRDR");

                entity.HasOne(d => d.TYPE)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.TYPE_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TOPERS_TOPERTYPE");

                entity.HasOne(d => d.V20)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.V20_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TOPERS_TV20");

                entity.HasOne(d => d.V26)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.V26_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TOPERS_TV26");

                entity.HasOne(d => d.V40V)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => d.V40V_ID)
                    .HasConstraintName("FK_TOPERS_TKVMNU");

                entity.HasOne(d => d.TVISITSUB)
                    .WithMany(p => p.TOPERs)
                    .HasForeignKey(d => new { d.LIB_ID, d.VIS_ID })
                    .HasConstraintName("FK_TOPERS_TVISITSUB");
            });

            modelBuilder.Entity<TOPERTYPE>(entity =>
            {
                entity.ToTable("TOPERTYPE");

                entity.HasIndex(e => e.OPERNAME, "IX_TOPERTYPE")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.DESCR)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasComment("Пояснение");

                entity.Property(e => e.OPERNAME)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Тип операции");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TPROLE>(entity =>
            {
                entity.ToTable("TPROLE");

                entity.HasIndex(e => e.PROLE, "IX_TPROLE")
                    .IsUnique();

                entity.Property(e => e.D1)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата начала действия категории");

                entity.Property(e => e.D2)
                    .HasColumnType("date")
                    .HasComment("Датат окончания действия категории. NULL - для текущих значений.");

                entity.Property(e => e.PROLE)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Роль поручителя");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TRDR>(entity =>
            {
                entity.ToTable("TRDR");

                entity.HasIndex(e => e.ID32, "IX_TRDR")
                    .IsUnique();

                entity.HasIndex(e => e.LIB_ID, "IX_TRDR_1");

                entity.HasIndex(e => e.PR_ID, "IX_TRDR_2");

                entity.HasIndex(e => e.RTYPE_ID, "IX_TRDR_3");

                entity.HasIndex(e => e.ST_ID, "IX_TRDR_4");

                entity.HasIndex(e => e.V20_ID, "IX_TRDR_5");

                entity.HasIndex(e => e.V26_ID, "IX_TRDR_6");

                entity.HasIndex(e => e.KSUNIQ, "IX_TRDR_7")
                    .IsUnique();

                entity.HasIndex(e => e.SQLUPD, "IX_TRDR_8");

                entity.HasIndex(e => e.FAM_ID, "IX_TRDR_9");

                entity.HasIndex(e => new { e.S1, e.ST_ID }, "IX_TRDR_S1");

                entity.HasIndex(e => new { e.S2, e.ST_ID }, "IX_TRDR_S2");

                entity.HasIndex(e => new { e.S3, e.ST_ID }, "IX_TRDR_S3");

                entity.HasIndex(e => new { e.S4, e.ST_ID }, "IX_TRDR_S4");

                entity.HasIndex(e => new { e.S5, e.ST_ID }, "IX_TRDR_S5");

                entity.HasIndex(e => new { e.S6, e.ST_ID }, "IX_TRDR_S6");

                entity.HasIndex(e => e.V300, "Поиск читателя");

                entity.Property(e => e.ID).HasComment("ID читателя/пользователя");

                entity.Property(e => e.ID32)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true)
                    .HasComment("Старый ID читателя (для совместимости) - поле 12000 ИРБИС");

                entity.Property(e => e.KSUNIQ)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(left(([V12814]+[PN])+CONVERT([varchar],[ST_ID]*[ID]),(50)))", true)
                    .HasComment("Служит для контроля уникальности V12814+PN ТОЛЬКО для записей со ST_ID=0");

                entity.Property(e => e.LIB_ID).HasComment("Библиотечная система, создавгая запись.");

                entity.Property(e => e.PI)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Имя поручителя");

                entity.Property(e => e.PN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер у поручителя");

                entity.Property(e => e.PO)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Отчество поручителя");

                entity.Property(e => e.PR_ID).HasComment("Роль поручителя");

                entity.Property(e => e.RTYPE_ID)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Тип читателя");

                entity.Property(e => e.S1)
                    .HasMaxLength(33)
                    .IsUnicode(false);

                entity.Property(e => e.S2)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.S3)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.S4)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.S5)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.S6)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.SQLUPD).HasComment("Признак обновления на стороне SQL");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V100)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Способ ввода КС");

                entity.Property(e => e.V10001).HasComment("Версия записи ИРБИС64");

                entity.Property(e => e.V10002).HasComment("Версия записи MS SQL");

                entity.Property(e => e.V10003).HasComment("Версия записи RUSLAN");

                entity.Property(e => e.V101).HasComment("Тип документа, по которому строится КС");

                entity.Property(e => e.V11)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Имя");

                entity.Property(e => e.V12)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Отчество");

                entity.Property(e => e.V12001)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата/время последнего изменения записи");

                entity.Property(e => e.V12003)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.V12005)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true);

                entity.Property(e => e.V12814)
                    .IsRequired()
                    .HasMaxLength(193)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("КС, длина 33 для совместимости с суммами, начин. с ? или у");

                entity.Property(e => e.V12814V)
                    .IsRequired()
                    .HasDefaultValueSql("((1))")
                    .HasComment("1-поле V12814 валидное, 0 - с отклонениями");

                entity.Property(e => e.V16)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Специальность/должность");

                entity.Property(e => e.V17)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Телефон");

                entity.Property(e => e.V18)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Телефон");

                entity.Property(e => e.V19A)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Место учебы");

                entity.Property(e => e.V20_ID)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Образование");

                entity.Property(e => e.V21).HasComment("Год рождения");

                entity.Property(e => e.V23)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Ж')")
                    .IsFixedLength(true)
                    .HasComment("Пол");

                entity.Property(e => e.V26_ID)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Социальная группа");

                entity.Property(e => e.V300)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Номер ЕЧБ");

                entity.Property(e => e.V31A)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.V31B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V32)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("e-mail");

                entity.Property(e => e.V34)
                    .HasColumnType("date")
                    .HasComment("Прописан до");

                entity.Property(e => e.V41).HasComment("Владеет системой Брайля");

                entity.Property(e => e.V45).HasComment("Пользователь с ограниченными возможностями");

                entity.Property(e => e.V50_ID)
                    .HasDefaultValueSql("((1))")
                    .HasComment("Образование");

                entity.Property(e => e.v12004)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата/время создания записи");

                entity.Property(e => e.v301).HasComment("ЕЧБ в библиотеке");

                entity.HasOne(d => d.FAM)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.FAM_ID)
                    .HasConstraintName("FK_TRDR_TFAM");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TLIB");

                entity.HasOne(d => d.PR)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.PR_ID)
                    .HasConstraintName("FK_TRDR_TPROLE");

                entity.HasOne(d => d.RTYPE)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.RTYPE_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TRTYPE");

                entity.HasOne(d => d.ST)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.ST_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TSTATUS");

                entity.HasOne(d => d.V101Navigation)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.V101)
                    .HasConstraintName("FK_TRDR_TV101");

                entity.HasOne(d => d.V20)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.V20_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TV20");

                entity.HasOne(d => d.V26)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.V26_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TV26");

                entity.HasOne(d => d.V50)
                    .WithMany(p => p.TRDRs)
                    .HasForeignKey(d => d.V50_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDR_TV50");
            });

            modelBuilder.Entity<TRDRSUB>(entity =>
            {
                entity.HasKey(e => new { e.R_ID, e.EKZ_ID });

                entity.ToTable("TRDRSUB");

                entity.HasIndex(e => new { e.R_ID, e.EKZ_ID }, "IX_TRDRSUB")
                    .IsUnique();

                entity.HasIndex(e => e.EKZ_ID, "IX_TRDRSUB_1");

                entity.HasIndex(e => e.LAST_OP_ID, "IX_TRDRSUB_10");

                entity.HasIndex(e => e.LAST_OP_TYPE, "IX_TRDRSUB_11");

                entity.HasIndex(e => e.LIB_ID, "IX_TRDRSUB_2");

                entity.HasIndex(e => e.ST_ID, "IX_TRDRSUB_3");

                entity.HasIndex(e => e.V20_ID, "IX_TRDRSUB_4");

                entity.HasIndex(e => e.V26_ID, "IX_TRDRSUB_5");

                entity.HasIndex(e => e.V40V_ID, "IX_TRDRSUB_6");

                entity.HasIndex(e => e.EAS, "IX_TRDRSUB_7");

                entity.HasIndex(e => new { e.R_ID, e.V40Z }, "IX_TRDRSUB_8")
                    .IsUnique();

                entity.Property(e => e.R_ID).HasComment("ID читателя из TRDR");

                entity.Property(e => e.BV40A)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Шифр для экземпляра");

                entity.Property(e => e.BV40G)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Имая БД для экземпляра");

                entity.Property(e => e.BV40HAR)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Характер документа");

                entity.Property(e => e.BV40RZN)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Раздел знаний");

                entity.Property(e => e.BV40VID)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('*')")
                    .HasComment("Вид документа");

                entity.Property(e => e.BV900T)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .IsFixedLength(true)
                    .HasComment("Тип документа");

                entity.Property(e => e.EV40B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Инвентарный номер для экземпляра");

                entity.Property(e => e.EV40E)
                    .HasColumnType("date")
                    .HasComment("Дата предполагаемого возврата для экземпляра");

                entity.Property(e => e.EV40H)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Метка/штрих-код");

                entity.Property(e => e.EV40K)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Место хранения");

                entity.Property(e => e.LAST_OP_ID).HasComment("ID последней операции с ресурсом");

                entity.Property(e => e.LAST_OP_TYPE).HasComment("Тип последней операции с ресурсом");

                entity.Property(e => e.RAGE).HasComment("Возраст на момент совершения операции (условная дата рождения 01.07.ГГГГ)");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна. Всегда совпадает с ST_ID из TRDR.");

                entity.Property(e => e.T0)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время создания записи");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V20_ID).HasComment("ID социальной группы");

                entity.Property(e => e.V26_ID).HasComment("ID места учебы");

                entity.Property(e => e.V40C)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasDefaultValueSql("('')")
                    .HasComment("Краткое описание (денормализовано)");

                entity.Property(e => e.V40D).HasColumnType("date");

                entity.Property(e => e.V40I)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Ответственное лицо");

                entity.Property(e => e.V40Z)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EKZ)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.EKZ_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TEKZ");

                entity.HasOne(d => d.LAST_OP)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.LAST_OP_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TOPERS");

                entity.HasOne(d => d.LAST_OP_TYPENavigation)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.LAST_OP_TYPE)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TOPERTYPE");

                entity.HasOne(d => d.LIB)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.LIB_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TLIB");

                entity.HasOne(d => d.R_IDNavigation)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.R_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TRDR");

                entity.HasOne(d => d.V40V)
                    .WithMany(p => p.TRDRSUBs)
                    .HasForeignKey(d => d.V40V_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRDRSUB_TKVMNU");
            });

            modelBuilder.Entity<TRE>(entity =>
            {
                entity.ToTable("TRES");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TRESSUB>(entity =>
            {
                entity.ToTable("TRESSUB");

                entity.Property(e => e.STATUS_ID).HasComment("Статус конкретной записи ресурса по аналогии с экземплярами в TEKZ");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.HasOne(d => d.RES)
                    .WithMany(p => p.TRESSUBs)
                    .HasForeignKey(d => d.RES_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRESSUB_TRES");

                entity.HasOne(d => d.STATUS)
                    .WithMany(p => p.TRESSUBs)
                    .HasForeignKey(d => d.STATUS_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TRESSUB_EKZSTATUS");
            });

            modelBuilder.Entity<TRTYPE>(entity =>
            {
                entity.ToTable("TRTYPE");

                entity.HasIndex(e => e.RTYPE, "IX_TRTYPE")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.DESCR)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasComment("Пояснение");

                entity.Property(e => e.RTYPE)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Тип читателя");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");
            });

            modelBuilder.Entity<TSP>(entity =>
            {
                entity.HasKey(e => e.SP)
                    .HasName("PK_TStoredProcedures");

                entity.HasIndex(e => e.NAIM, "IX_TStoredProcedures")
                    .IsUnique();

                entity.Property(e => e.SP)
                    .ValueGeneratedNever()
                    .HasComment("Уникальный код ХП");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(1024)
                    .IsUnicode(false)
                    .HasComment("Описание ХП");

                entity.Property(e => e.NAIM)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Имя ХП");
            });

            modelBuilder.Entity<TSPERRCODE>(entity =>
            {
                entity.HasKey(e => new { e.SP, e.ERRCODE })
                    .HasName("PK_TSP_ErrorCodes");

                entity.ToTable("TSPERRCODES");

                entity.HasIndex(e => new { e.SP, e.ERRCODE }, "IX_TSP_ErrorCodes")
                    .IsUnique();

                entity.Property(e => e.SP).HasComment("Уникальный код ХП");

                entity.Property(e => e.ERRCODE).HasComment("Код ошибки");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("Краткое описаие ошибки");

                entity.Property(e => e.DESCRFULL)
                    .HasMaxLength(2048)
                    .IsUnicode(false)
                    .HasComment("Подробное описание ошибки");

                entity.HasOne(d => d.SPNavigation)
                    .WithMany(p => p.TSPERRCODEs)
                    .HasForeignKey(d => d.SP)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TSPERRCODES_TSP");
            });

            modelBuilder.Entity<TSPERRLOG>(entity =>
            {
                entity.ToTable("TSPERRLOG");

                entity.HasIndex(e => e.R_ID, "IX_TSPERRLOG_1");

                entity.HasIndex(e => e.BOOK_ID, "IX_TSPERRLOG_2");

                entity.HasIndex(e => e.EKZ_ID, "IX_TSPERRLOG_3");

                entity.HasIndex(e => e.ID32, "IX_TSPERRLOG_4");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(1024);

                entity.Property(e => e.ID32)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.R_ID).HasComment("ID читателя из TRDR");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())");
            });

            modelBuilder.Entity<TSTATUS>(entity =>
            {
                entity.ToTable("TSTATUS");

                entity.HasIndex(e => e.STATUS, "IX_TSTATUS")
                    .IsUnique();

                entity.HasIndex(e => e.DESCR, "IX_TSTATUS_1")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Краткое наименование, для получения по нему кода");

                entity.Property(e => e.STATUS)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TV101>(entity =>
            {
                entity.ToTable("TV101");

                entity.HasIndex(e => e.DOCTYPE, "IX_T101")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DOCTYPE)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TV20>(entity =>
            {
                entity.ToTable("TV20");

                entity.HasIndex(e => new { e.V20, e.D1 }, "IX_TV20")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.D1)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата начала действия категории");

                entity.Property(e => e.D2)
                    .HasColumnType("date")
                    .HasComment("Датат окончания действия категории. NULL - для текущих значений.");

                entity.Property(e => e.DESCR)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V20)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Образование");
            });

            modelBuilder.Entity<TV26>(entity =>
            {
                entity.ToTable("TV26");

                entity.HasIndex(e => new { e.V26, e.D1 }, "IX_TV26")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.D1)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата начала действия категории");

                entity.Property(e => e.D2)
                    .HasColumnType("date")
                    .HasComment("Датат окончания действия категории. NULL - для текущих значений.");

                entity.Property(e => e.DESCR)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V26)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Социальная группа");
            });

            modelBuilder.Entity<TV50>(entity =>
            {
                entity.ToTable("TV50");

                entity.HasIndex(e => new { e.V50, e.D1 }, "IX_TV50")
                    .IsUnique();

                entity.HasIndex(e => new { e.D1, e.L1 }, "IX_TV50_1")
                    .IsUnique();

                entity.HasIndex(e => new { e.D1, e.L2 }, "IX_TV50_2")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.D1)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата начала действия категории");

                entity.Property(e => e.D2)
                    .HasColumnType("date")
                    .HasComment("Датат окончания действия категории. NULL - для текущих значений.");

                entity.Property(e => e.DESCR)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.L1).HasComment("Нижняя граница возраста");

                entity.Property(e => e.L2).HasComment("Верхняя граница возвраста");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())")
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V50)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')")
                    .HasComment("Возрастная группа");
            });

            modelBuilder.Entity<TVISIT>(entity =>
            {
                entity.ToTable("TVISIT");

                entity.HasIndex(e => e.V40C, "IX_VISIT_MNU")
                    .IsUnique();

                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.V40C)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TVISITSUB>(entity =>
            {
                entity.HasKey(e => new { e.LIB_ID, e.VIS_ID });

                entity.ToTable("TVISITSUB");

                entity.Property(e => e.LIB_ID).HasDefaultValueSql("([dbo].[fSIG]())");

                entity.Property(e => e.V40C)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tmp_T1050>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Tmp_T1050");

                entity.Property(e => e.EDOMX_ID).HasComment("Место хранения договора");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.TIP).HasComment("Тип документа 0 -старый договор, 1 -ЕДО, 2-согласие, 11-аннулир. ЕДО, 12-аннул. согласие, 10 -аннулир. обычный дог.");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasComment("Дата, время последнего изменения записи");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("Последний изменивший запись");

                entity.Property(e => e.V1051A)
                    .HasColumnType("date")
                    .HasComment("Датат договора");

                entity.Property(e => e.V1051B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("Номер договора");
            });

            modelBuilder.Entity<agentBOOK_ERR>(entity =>
            {
                entity.HasKey(e => e.IDER);

                entity.ToTable("agentBOOK_ERR");

                entity.HasIndex(e => e.SIG, "IX_agentBOOK_ERR");

                entity.HasIndex(e => new { e.V40G, e.V903 }, "IX_agentBOOK_ERR_1");

                entity.HasIndex(e => e.MFN, "IX_agentBOOK_ERR_2");

                entity.Property(e => e.ERRMESS)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.MFN).HasDefaultValueSql("((0))");

                entity.Property(e => e.SIG).HasDefaultValueSql("((0))");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UN)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.V40G)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V903)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910).IsUnicode(false);
            });

            modelBuilder.Entity<agentBOOK_LOG>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("agentBOOK_LOG");

                entity.Property(e => e.S)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("Успешна запись поля 10000");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.V40G)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.V903)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VERS).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<agentREC_TYPE>(entity =>
            {
                entity.HasKey(e => e.TYPE);

                entity.ToTable("agentREC_TYPES");

                entity.HasIndex(e => e.DESCR, "IX_agentREC_TYPES")
                    .IsUnique();

                entity.Property(e => e.TYPE)
                    .ValueGeneratedNever()
                    .HasComment("Тип издателя: 1-ИРБИС, 2- MS SQL, 3-RUSLAN");

                entity.Property(e => e.DESCR)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<agentTEKZ>(entity =>
            {
                entity.HasKey(e => e.ID0)
                    .HasName("PK_agentTEKZ_1");

                entity.ToTable("agentTEKZ");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.V910H, e.V910D, e.V910A }, "IX_agentTEKZ");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.V910H, e.V910A }, "IX_agentTEKZ_1");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910B, e.V910D }, "IX_agentTEKZ_2");

                entity.HasIndex(e => new { e.BOOK_ID, e.V910H, e.V910D }, "IX_agentTEKZ_3");

                entity.HasIndex(e => e.IDE, "IX_agentTEKZ_4");

                entity.HasIndex(e => new { e.BOOK_ID, e.IDE }, "IX_agentTEKZ_5");

                entity.Property(e => e.ID0).HasComment("Искусственный ключ");

                entity.Property(e => e.BBK)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IDE).HasComment("ID записи из TEKZ, заполняемый при репликации");

                entity.Property(e => e.ST_ID).HasComment("Статус записи: 0 - норм.; 1 - к удалению; 2 - удалена; 3 - не активна.");

                entity.Property(e => e.V210D)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Год издания");

                entity.Property(e => e.V910B)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V910C)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasComment("Дата ввода в формате ГГГГММДД");

                entity.Property(e => e.V910D)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V910E).HasColumnType("money");

                entity.Property(e => e.V910F)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910H)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.V910I)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910M)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910P)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910R)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910S).HasColumnType("date");

                entity.Property(e => e.V910U)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910V)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910VSKL)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910W)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V910Y)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<agentTEKZ_NEWBOOK>(entity =>
            {
                entity.HasKey(e => new { e.V40G, e.V903 });

                entity.ToTable("agentTEKZ_NEWBOOK");

                entity.Property(e => e.V40G)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.V903)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IVERS).HasComment("Версия записи ЭК ИРБИС");

                entity.Property(e => e.TIP).HasComment("0 - очередь публ. ИРБИС, 1 - конфликт версий при публик ИРБИС");

                entity.Property(e => e.TS)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UPD).HasComment("Отметка об обновлении. Если 0 - обновления еще не было.");
            });

            modelBuilder.Entity<agentTIQ>(entity =>
            {
                entity.HasKey(e => new { e.ID32, e.TYPE });

                entity.ToTable("agentTIQ");

                entity.Property(e => e.ID32)
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.TYPE).HasComment("Тип записи из таблицы [agentREC_TYPES]");

                entity.Property(e => e.ID).HasComment("ID из TRDR");

                entity.Property(e => e.MFN).HasComment("Для ИРБИС mfn, для MS SQL - код (счетчик)");

                entity.Property(e => e.P1)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Код подписчика 1");

                entity.Property(e => e.P2)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Код подписчика 2");

                entity.Property(e => e.TS).HasColumnType("datetime");

                entity.Property(e => e.V10001)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Версия записи ИРБИС");

                entity.Property(e => e.V10002)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Версия записи MS SQL");

                entity.Property(e => e.V10003)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Версия записи RUSLAN");

                entity.Property(e => e.V12005)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength(true)
                    .HasComment("Первые два символа поля (в широком смысле) 12003");

                entity.HasOne(d => d.TYPENavigation)
                    .WithMany(p => p.agentTIQs)
                    .HasForeignKey(d => d.TYPE)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_agentTIQ_agentREC_TYPES");
            });

            modelBuilder.Entity<agentTMP_DB>(entity =>
            {
                entity.HasKey(e => new { e.LIB, e.DATABASE });

                entity.ToTable("agentTMP_DB");

                entity.Property(e => e.LIB)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DATABASE)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<tmpBOOK>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tmpBOOK");
            });

            modelBuilder.Entity<vtempIBIS21_V910A>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vtempIBIS21_V910A");

                entity.Property(e => e.STT)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
