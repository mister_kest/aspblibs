﻿using LibsEsbo.EsboEntities;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibsEsbo
{
    public static class EsboExtensions
    {

        public static IServiceCollection AddEsbo( this IServiceCollection services )
        {
            services.AddDbContext<EsboDbContext>(options=> {
                options.UseSqlServer(EsboDbContext.DefaultConnectionString);
            });
            return services;
        }
    }
}
